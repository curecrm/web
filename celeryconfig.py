# -*- coding: utf-8 -*-
import os

import celery
from celery.app.base import Celery


CELERY_DEFAULT_QUEUE = 'celery_worker'

REDIS_HOST = os.environ.get('CELERY_REDIS_HOST', 'localhost')
REDIS_PORT = os.environ.get('CELERY_REDIS_PORT', '6379')
REDIS_DB = os.environ.get('CELERY_REDIS_DB', '0')
REDIS_PASSWORD = os.environ.get('CELERY_REDIS_PASSWORD', '')


def get_broker_url():
    return 'redis://:{password}@{host}:{port}/{db}'.format(
        password=REDIS_PASSWORD,
        host=REDIS_HOST,
        port=REDIS_PORT,
        db=REDIS_DB)


def get_celery():
    print('CELERY VERSION ---------------------->', celery.__version__)
    return Celery('celery_worker',
                  backend=get_broker_url(),
                  broker=get_broker_url())
