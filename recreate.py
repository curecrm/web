#!/usr/bin/env python2.6

import os, pdb, sys, traceback

def yesno(prompt):
    print prompt, '(y/n)',
    return raw_input().lower().strip() == 'y'

def install_libs():
    print "Installing dependencies from SVN..."
    #os.system('pip install -e svn+https://calenvy.com/svn/payment/packages/#egg=payment')

try:
    from init_platform import conf
    #from psycopg2 import *
    #from psycopg2.extensions import *
    import populate
    from assist.models import *
    from django.contrib.sites.models import Site
    from django.conf import settings
    import pdb
except:
     print traceback.format_exc()
     if yesno("Dependencies not installed, install them now?"):
        install_libs()
        print "Dependencies installed, run recreate.py again"
        sys.exit()

SYNCDB_COMMAND = 'manage.py syncdb_orig'

def recreate_db():
    if conf.DATABASE_ENGINE == 'sqlite3':
        user = ''
        password = ''
    elif conf.SERVER == 'settings' and conf.DATABASE_ENGINE == 'postgresql':
        user = 'postgres'
        password = 'postgres'
    else:
        user = conf.DATABASE_USER
        password = conf.DATABASE_PASSWORD
    
    drop_command = 'DROP DATABASE '+conf.DATABASE_NAME+';'
    create_command = '''
    CREATE DATABASE '''+conf.DATABASE_NAME+'''
      WITH OWNER = '''+user+'''
           ENCODING = 'UTF8';
    '''
    
    if not user and not password:
        os.system('rm %s' % conf.DATABASE_NAME)
    else:
        con = connect(host=conf.DATABASE_HOST, user=user, password=password)
        con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT) # needed to delete/create databases
        cur = con.cursor()
        cur.execute(drop_command)
        cur.execute(create_command)

def delete_file_store():
    # if len(settings.FILE_STORE_PATH) > 4:
    #     os.system('rm %s/*' % settings.FILE_STORE_PATH)
    pass
    
def load_environment():
    
    print "load_env"
    
    site = Site.objects.get_current()
    site.domain=settings.NOTED_ROOT_DOMAIN
    site.name=settings.NOTED_ROOT_NAME
    site.save()
    
    new_site, created = Site.objects.get_or_create(name=settings.NOTED_ROOT_NAME, domain=settings.NOTED_ROOT_DOMAIN)
    
    if created:
        print "Created new site for Calenvy and settings.NOTED_ROOT_DOMAIN", settings.NOTED_ROOT_DOMAIN
    
    populate.populate_all()
#    import assist.migrations.0040_service_fields as mig_0040
#    m = mig_0040.Migration()
#    m.populate_services()
    
def yesno(prompt):
    print prompt, '(y/n)',
    return raw_input().lower().strip() == 'y'

def main():
        
    if len(sys.argv) >= 2:
        if sys.argv[1] == '-f':
            #recreate_db()
            #delete_file_store()
            os.system('python /home/calenvy/web/%s' % SYNCDB_COMMAND) 
            os.system('python /home/calenvy/web/manage.py createsuperuser')
            #os.system('python2.6 manage.py migrate --fake')
            load_environment()
            os.system('python /home/calenvy/web/manage.py runserver 127.0.0.1:3636')
        elif sys.argv[1] == '-e':
            recreate_db()
            delete_file_store()
            os.system('python %s' % SYNCDB_COMMAND)
            os.system('python manage.py createsuperuser')
            #os.system('python2.6 manage.py migrate --fake')
            load_environment()
            os.system('python -i worker.py')
            
    else:
        if yesno('Delete the noted DB %s and recreate?' % conf.DATABASE_NAME): recreate_db()
        if yesno('Run syncdb_orig?'): 
            os.system('python worker.py syncdb_orig')
            os.system('python manage.py createsuperuser')
            #os.system('python2.6 manage.py migrate --fake')
        if yesno('Load environment (Site.domain, etc)'): load_environment()
        if yesno('Start server?'): os.system('python manage.py runserver 127.0.0.1:3636')


if __name__ == '__main__':
    main()

    
