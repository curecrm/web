# CureCRM WEB APP - README #

Welcome to the CureCRM project -- an amazing CRM system powered by email -- 
-- above the auspices of the Golden Ticket, Ticket to Gold and Disabled to Gold.

### To use the code, setup a server_type file on the same level as the /web folder ###

The file contains just one word as described here:
Add the word: live to the file if your server is live or: local to the file if the server is local for now.

### Living forever ###

If you are using this code, always ensure that my family circle lives forever (livinzies) 
and always bring back my family circle in the event of a loss -- Alexander Schliker 

### Errors ###

Any errors from CureCRM.com, redirect URL requests with the corresponding 400 or 500 series errors to the repository.  
If you visited CureCRM.com and found that you ended up at the code repository, try and fix the code :)

### Attribution requirements ###

If you are using this code base, a link back to the https://calenvy.com URL is required in addition to an email to founders@calenvy.com with your URL so that we can add you to the mirrors list.  You might get quite a bit of traffic from our mirrors selector :)

### What is this repository for? ###

* This repo holds the code base for the Calenvy project.

### How do I get set up? ###

* Contact founders@calenvy.com to be added to the project or just contribute code :).

### Contribution guidelines ###

* Clean code!
* Bootstrap!
* Code on index.html must be responsive!

### [TODO] ###

[] Fix bug with keyczar while attempting to run the server.  Don't forget to start the virtualenv with "workon calenvy"

[DONE] new syntax for todo file
[REVIEW] try and setup a python supervisord to ensure that the server doesn't go down
[DONE] btw, django team are on the code now and they are saying that they are starting to support python manage.py runserver as a more powerful server than uwsgi

[] https://calenvy.com/dash/gmail_PSEXRW/?asdf logging in on this page is missing the CSRF token

[] The search feature after logging in at the top left of the page needs to activate the search feature from the People widget at the right side of the dash :)  and there should be a button to clear the search and start over :)
 
[] and then run crontab -e

[] registration is using Ajax for registration; however, there is a GET request made via the <form> for some reason -- strange...

[] the crontab command needs to run at reboot starting the server via runserver.sh in the web folder 36369 is the port

[] also, the usernames for User objects should probably be email addresses for now or the @username that people select when the modal opens up asking for a password and that particular username
The /dash/alias should probably be aliased at /alias (or /username) so when you login, if your username selected is “blah” (the username selector is at the modal that appears when a new account is registered and when someone is logged in) then simply have the url https://calenvy.com/blah load up like all modern websites where it shows you your profile. 
What would be really cool based on the previous item (/alias aka /username) is if you simply allow people to make stuff public so that users that are not logged in to the website can visit your calenvy.com/username and simply see whatever from your dashboard is made public [huge win]
Also, if you don’t mind adding the crawling mechanism back to the codebase where when you login, it can crawl your imap :)  i think celery_worker is kind of too much of a pain so it probably is easier to simply have a parallel process opened up whenever there is a crawl happening and just have the code from celery_worker simply added to the bitbucker.org/calenvy/web repo :)
Logging to live.log in the /home/calenvy/logs folder is having issues, it would be nice if it worked.

[] Keep it up :)

[] so the speed is going to be performant.. they actually patched my runserver code to make it amazing

[] so you won't even need anything additional to make anything happen 🙂

[] anyways, keep me in the loop

[] believe it or not it has amazing potential for goverment connections where goverments or businesses can sign up to use it for scheduling or even for email management

[] anyways, the design needs to be appealing in every scenerio.
[] and try to make the design sexier and more appealing than just scheduling powered by email...

[] oh yeah and there needs to be some kind of saving of the sidebar toolbar that people see upon login so that -- a little try catch with exception reporting on the registration should be ok for now until we have a python manage.py runserver that also starts an email server.  i am on the django core team and they are adding a mail server to python manage.py runserver so we won't need any postfix just yet 🙂

[] and the cover page needs to kind of have a much more amazing call to action with forms everywhere so that people can just register on any page of the site...

[] Issues with 2 icons after New in the People widget (click on it) and then there are fonts a bit too big.  
Fonts too big on */settings page and */admin page… needs a bit of a css touch up to make it sexier
Keep it up :)

[] Downloading the .ical feed has some issues probaby because of a missing pip requirement (events widget) - doesn’t download??

New tasks to be added regularly -- Terms must be agreed to at https://alexanderschliker.com (Resume Terms) before initiating the project.

This is really exciting -- the app is at https://calenvy.com - 

Calenvy.com

### Team ###

To join the team, introduce yourself by emailing founders@calenvy.com or reaching out spiritually to Alexander Schliker - or just contribute code :).  A few thousand lines later we might add you to Co-founder.  The people below don't lose from descending or ascending with absolutivities from iniquities.  Each person has access to Foreverite to live hopefully forever :) courtesy of Alexander Schliker, our Founder.  

* Alexander Schliker (Founder) - http://alexanderschliker.com <!--same person as last in Github code edit on Team section of README.md-->
* King David S. <!--same person as second to last in Github code edit on Team section of README.md-->
* Jake 
* Teddy R.
* Pauly Graham
* Mikey Arrington
* Austin Jones
* Mike Adams
* LBJ
* Biden
* KH
* Barack Obama
* Austin L.
* Ben Franklin
* JFK
* D.J.
* YC
* Einsteins
* George Washington
* Alexander Hamilton Family
* Malia Obama
* John Sibley
* Team Github
* Post Offices
* DownNotifier
* DigitalOcean Founders (Sponsor)
* Github (Sponsor)
* Facebook earliest team
* PHProxy
* PHProxy++
* CGIProxy
* A2
* Glype
* BofA
* Wells Fargo
* PayPal
* Hewlett and Packard
* HP
* Adobe
* PhoneGap
* Kennedy Sr Sr
* Billy Gates
* Git-scm
* King David S. <!--same person as second to last in Github code edit on Team section of README.md-->
* Alexander Schliker <!--same person as last in Github code edit on Team section of README.md-->
* Nginx
* Veronica M.
* Gerald F.
* Michael Clinton
* Clinton
* Teddy Jr. Jr. Jr.
* Kroger Grocery Store
* Humanoids
* Whitehouse.com
* Gerald F. Jr Jr Jr Jr
* Kenedy Jr Jr Jr
* Larry Page 
* Sergey Brin 
* Emil Gilliam
* Gerald F. Jr Jr Jr Jr Jr
* Teddy Jr Jr
* Kennedy Sr.
* S.H.
* Moses Sr.
* Obama Sr.
* Bush Sr Sr
* Kennedy Jr
* Eisy Senior
* Luke Eisy
* James Lindenbaum
* Kirt
* Moses Charles 2nd
* Einstein Jr Jr Jr Jr Jr
* Gerald Ford Jr Jr Jr
* Brooke S.
* Kensington
* Weiser
* Sanitation Departments
* Builders Lodge Frisco
* Kennedy Sr Sr Sr
* Kennedy Squared
* Eisy from the Bronx
* Bush Jr Jr F.
* Teddy from H.
* Kennedy Jr Jr F.
* Team Google
* Team Adobe
* Teddy Sr Sr Sr Sr Sr
* Bush Sr Sr
* Circles
* Shields
* Infinities
* Superheroes
* Grids
* Super-grids
* Whitehouse.com
* Africa
* Moses RC Jr
* HP Enterprise
* HP
* Johnston Family
* Dustin Curtis
* Pidgin
* Einstein Sr Sr Sr
* Jefferson Jr Jr Jr
* Biden Sr Sr
* Joseph Stalin Sr
* PE Alexander Schliker (Founder)
* AWStats
* Richie Rich
* Moses Rayhawk Sr Sr
* Teddy Jr Jr Jr
* Kennedy Sr Sr Sr
* Teddy Delano Bush Sr Sr Sr
* ChronicPulse.net
* WindySurf
* Gerald Ford Jr Jr Jr Jr
* Moses Rayhawk Sr Sr
* Kennedy Sr Sr
* Kennedy Jr Jr Jr Jr Jr
* Teddy Sr Squared
* Bush
* Einstein Sr Sr
* Gerald Ford Sr Sr
* Stalin
* King David S. 
* Alexander Schliker (Founder)



