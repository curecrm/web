from django.conf.urls.defaults import *
from piston.resource import Resource

from auth import LegacyAuthentication, AliasHttpBasicAuthentication
# TODO: OAuth
from api.handlers_legacy import *
from api.handlers_plugins import *
from api.handlers_auth import *
from api.handlers_message import *
from api.handlers_followup import *
from api.handlers_payment import *
from api.handlers_register import *
from assist.models import Alias

# Legacy APIs for (old) Salesforce and Firefox (maybe Outlook too?)
# TODO: Kill these completely
legacy_auth = LegacyAuthentication()
legacy_ad = { 'authentication': legacy_auth }
get_apitoken_resource = Resource(handler=GetAPITokenHandler)         # no auth on this one
login_resource = Resource(handler=LoginHandler, **legacy_ad)

# New APIs
test_noauth_resource = Resource(handler=TestNoauthHandler)          # Test API that just returns success
register_resource = Resource(handler=RegisterHandler)               # No auth needed for this one
services_resource = Resource(handler=ServicesHandler)               # No auth needed for this one
alias_password_auth = AliasHttpBasicAuthentication(use_token=False)
alias_token_auth = AliasHttpBasicAuthentication(use_token=True)
get_credential_resource = Resource(handler=GetCredentialHandler, authentication=alias_password_auth)
login_api_resource = Resource(handler=LoginAPIHandler, authentication=alias_token_auth)
messages_resource = Resource(handler=MessagesHandler, authentication=alias_token_auth)
list_plugins_resource = Resource(handler=ListPluginsHandler)        # No auth needed for this one
followup_resource = Resource(handler=FollowupHandler, authentication=alias_token_auth)
payment_notify_resource = Resource(handler=PaymentNotifyHandler)

# Internal APIs, used for the callbacks
internal_messages_resource = Resource(handler=InternalMessagesHandler)  # No auth on this one, yet

urlpatterns = patterns('',
    # Legacy resources (Salesforce / Firefox plugins)
    url(r'^get_apitoken/$', get_apitoken_resource),
    url(r'^login/$', login_resource),                               # Requires auth

    # New API resources
    url(r'^test_noauth/$', test_noauth_resource),                   # Just "ping" the server to see if it's alive
    url(r'^register/$', register_resource, { 'emitter_format': 'json' }),
    url(r'^services/$', services_resource),                         # allow json or xml format
    url(r'^get_credential/$', get_credential_resource, { 'emitter_format': 'json' }),
    url(r'^login_api/$', login_api_resource, { 'emitter_format': 'json' }),
    url(r'^messages/$', messages_resource, { 'emitter_format': 'xml_cdata' }),
    url(r'^list_plugins/$', list_plugins_resource, { 'emitter_format': 'json' }),
    url(r'^followup/$', followup_resource, { 'emitter_format': 'xml_cdata' }),
    url(r'^payment_notify/$', payment_notify_resource, { 'emitter_format': 'json' }),
    
    url(r'^internal/messages/$', internal_messages_resource, { 'emitter_format': 'json' })
    
   # url(r'^messages/$', message_resource)
    
    
    #url(r'^events/(?P<event_id>[^/]+)/$', event_resource),
#    url(r'^test/$', test_resource),
    #url(r'^other/(?P<username>[^/]+)/(?P<data>.+)/$', arbitrary_resource), 
)
