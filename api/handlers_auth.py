import re, pdb, traceback
from piston.handler import BaseHandler, AnonymousBaseHandler
from piston.utils import *
from remote.models import *
from assist.models import *
from plugins.models import *
from django.utils.translation import ugettext as _
from piston.emitters import Emitter
from django.utils.xmlutils import SimplerXMLGenerator


class TestNoauthHandler(AnonymousBaseHandler):
    
    allowed_methods = ('GET', )

    def read(self, request):
        return {'success': True}
    

class GetCredentialHandler(BaseHandler):
    """
    Exchange username/password credentials for a token -- requested over BasicAuth
    TODO: Return OAuth access tokens
    """
    
    allowed_methods = ('GET', )
    
    def read(self, request):
        try:                                    
            log("GetCredentialHandler: start")            
            remote_usertype = request.GET['remote_usertype']
            remote_userid = request.GET['remote_userid']
            remote_username = request.GET.get('remote_username', '')
            remote_organizationid = request.GET.get('remote_organizationid', '')
            remote_organizationname = request.GET.get('remote_organizationname', '')
            cred_type = request.GET['cred_type']
            
            alias = request.session['alias']
            if not alias:
                raise Exception()
            
            if not remote_userid:
                return {'success': False, 'msg': 'Must supply a remote_userid'}
            
            if remote_usertype not in RemoteUser.TYPES:
                return {'success': False, 'msg': 'Unknown remote_usertype'}

            if cred_type not in RemoteCredential.TYPES:
                return {'success': False, 'msg': 'Unknown cred_type'}
            
            if (remote_usertype == RemoteUser.TYPE_SF) and not alias.account.service.allow_sf:
                return {'success': False, 'msg': 'Sorry, the %s package does not support Salesforce integration - please upgrade to a new package with SF support at http://calenvy.com/@/register/Free/' % alias.account.service.name}
                             
            remote_user, created = RemoteUser.objects.get_or_create(type=remote_usertype, alias=alias, account=alias.account, remote_userid=remote_userid)
            if created:
                # remote_user newly created -- 'ping' the cc account
                from network.ajax_admin import admin_ping
                category = RemoteUser.TYPE_CATEGORIES[remote_usertype]
                content = 'RemoteUser created: %s\nType: %s' % (alias.email, category)
                admin_ping(alias.contact, content=content, categories=[category])
                
                # This is a good remote user, no matter what its previous state
                remote_user.enabled = True
                remote_user.valid = True
            if remote_organizationid:
                remote_user.remote_organizationid = remote_organizationid
            remote_user.save()
           
            token, created = RemoteCredential.objects.get_or_create_for_remote_user(alias, remote_user, cred_type)
            
            log("GetCredentialHandler: success -- returning token starting with ", token.token[:6])
            
            result = {'success': True, 'cred_type': cred_type, 'token': token.token}
            return result
                        
        except:
            return rc.FORBIDDEN
        
     
class LoginAPIHandler(BaseHandler):
    """
    Use a permanent token to "log in" to the API, which means:
        * updating the date_last_accessed and version fields in the remote_user
        * getting a one-time token that can be used to log into the website
    This isn't strictly necessary to access most API calls.
    """
    
    allowed_methods = ('GET', )
    
    def read(self, request):
        # If we even get here, then the permanent token is successfully authenticated.
                
        alias = request.session['alias']
        account = alias.account
        version = request.GET.get('version', None)
        start_crawl = bool(int(request.GET.get('start_crawl', '0')))
        agent = get_agent(request)
        
        log("LoginAPIHandler: alias=", alias, " version=", version, " start_crawl=", start_crawl)

        request.session['remote_user'].date_last_accessed = now()
        if version:
            request.session['remote_user'].version = version
        request.session['remote_user'].save()
        
        cred_type = RemoteCredential.TYPE_WEB_ONETIME_TOKEN
        token, created = RemoteCredential.objects.get_or_create_for_remote_user(\
            alias, request.session['remote_user'], cred_type)

        result = {'success': True, 'cred_type': cred_type, 'token': token.token, 'msg': '', 'base_url': alias.account.base_url}
        
        if start_crawl:
            if alias.password_hash:
                # Alias has already done their first-time setup
                
                log("LoginAPIHandler: starting a crawl for ", alias)
                import crawl.crawler
                
                if alias.server_enabled and alias.server_valid:
                    m, noted_meta = crawl.crawler.start_crawl_alias(alias, join_existing=True)
                    log("LoginAPIHandler: started crawl with pk = ", noted_meta.pk)
                    result['crawl_task_success'] = True
                    result['crawl_task_id'] = str(noted_meta.id)
                else:
                    # User needs to set up their email settings
                    result['crawl_task_success'] = False
                    result['crawl_task_icon'] = 'error';
                    result['crawl_task_msg'] = _('Cannot connect to your inbox - please check your email settings.')
                    result['crawl_task_redirect_to'] = '/dash/%s/settings/?agent=%s&wizard_section=email' % (account.base_url, agent)
            else:
                # User hasn't set up yet
                result['crawl_task_success'] = False
                result['crawl_task_icon'] = 'info';
                result['crawl_task_msg'] = _('Please set up Calenvy and configure your email settings.')
                result['crawl_task_redirect_to'] = '/dash/%s/settings/?agent=%s' % (account.base_url, agent)
                  
        return result
    
    

        