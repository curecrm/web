from datetime import *
import os, sys, string, pytz, re
ROOTDIR = os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir))
DATADIR = os.path.join(ROOTDIR, 'data')
from django.utils.translation import ugettext as _
from keyczar import keyczar
from django.conf import settings

SERVER_TIMEZONE = pytz.timezone('US/Pacific')

ASSISTANT_GENERIC_NAMES = [
    'note',
    'noted'
]

class ContactField():
    def __init__(self, field_name, name, special=False):
        self.field_name, self.name, self.special = field_name, name, special
        
    def as_json(self):
        return {\
            'field_name':   self.field_name,
            'name':         self.name,
            'special':      self.special
        }
    
CONTACT_DEFAULT_FIELDS = [
    ContactField('first_name',      _('First name')),
    ContactField('last_name',       _('Last name')),
    ContactField('email',           _('Email')),
    ContactField('website',         _('Website')),
    ContactField('company',         _('Company')),
    ContactField('title',           _('Title')),
    ContactField('phone',           _('Phone')),
    ContactField('description',     _('Description')),
]

TAG_SHORTCUTS = {
    '@n':           '@note',
    '@noted':       '@note',
    '@s':           '@status',
    '@d':           '@digest',
    '@p':           '@private',
    '@r':           '@reminders',
    '@reminder':    '@reminders'
}

# If this is found anywhere in the email or file name, skip it.
RE_CONFIDENTIAL = re.compile(r'(?i)confidential')

EMAIL_IGNORE_PATTERNS = [
    r'(?i)activation (link|e-?mail)',
    #r'(?i)love,',
    r'(?i)love you',
    r'(?i)miss you',
    #r'(?i)babe',
    #r'Baby'
]

# Time, in seconds, to keep stuff in memcached
CACHE_TIMEOUTS = {                                                     
    None:           60*30                               # For now, use this value for all agents
}
                 
MOBILE_AGENTS = ['iphone', 'windowmob']                 # Don't allow these unless they have that level of service

AUTO_BUCKET_NAMES = ['friends']
SF_BUCKET_NAME = 'salesforce'                           # Contacts in this category will get synced to Salesforce
            
EZ_NAME_ALLOWED_CHARS = string.ascii_letters + string.digits + '._'

SUMMARY_LENGTH_MAX = 400                                # Store this much in the DB; we can truncate at display time
SUMMARY_TAGONLY_ELLIPSIZE_MIN = 200
SUMMARY_TAGONLY_ELLIPSIZE_MAX = 230

ONE_MEG = 2**20
ONE_GIG = 2**30

MIN_PASSWORD_LENGTH = 6

MAX_EMAIL_ADDRESS_LENGTH = 74

EMAIL_FREQUENCY_NONE = '---'
EMAIL_FREQUENCY_ONLY_DIGESTS = 'D--'
EMAIL_FREQUENCY_DIGEST_AND_NOTIFICATIONS = 'D+R'
EMAIL_FREQUENCY_ONLY_NOTIFICATIONS = '--R'

ACCOUNT_STATUS_ACTIVE = 'AACT'
ACCOUNT_STATUS_INACTIVE_MAX_USERS_EXCEEDED = 'AMXU'     # Too many users for this service/payment level -- but
                                                        #  we'll restore service as soon as they fix this
ACCOUNT_STATUS_INACTIVE = 'AINA'
ACCOUNT_STATUS_DELETE_PENDING = 'Adel'                  # Account is marked for deletion within 24 hours
ACCOUNT_STATUS_DELETE_IN_PROGRESS = 'Axxx'              # Account is being deleted right now

ALIAS_STATUS_ACTIVE = 'aACT'
ALIAS_STATUS_INACTIVE = 'aINA'
ALIAS_STATUS_AWAITING_CONFIRMATION = 'aACO'

EMAIL_X_MAILER_NOTED = 'Noted'                          # Use this for the 'X-Mailer' header

ADDR_SPACE_RFC822 = 'rfc822'                            # The message address space of email addresses
ADDR_SPACE_TWITTER = 'twitter'                          # The message address space of Twitter screen names
 
EMAIL_PRIORITY_HIGH = 1                                 # for the 'X-Priority' header
EMAIL_PRIORITY_MEDIUM = 3
EMAIL_PRIORITY_LOW = 5

EMAIL_TYPE_USER     =   'EUSR'                          # Emails cc:ed to us by the user
EMAIL_TYPE_OTHER    =   'EOTH'                          # Replies by other people
EMAIL_TYPE_NOTED    =   'ENOT'                          # Non-debug mails we sent
EMAIL_TYPE_DEBUG    =   'EDEB'                          # Debug output we sent
EMAIL_TYPE_INCOMPLETE = 'E---'                          # Not finished yet (perodically delete these)

EMAIL_SUBTYPE_USER_REGISTER =     'uREG'                # Registration email
EMAIL_SUBTYPE_USER_CREATE_EVENT = 'uEVT'                # User creating a new event
EMAIL_SUBTYPE_USER_CREATE_REMINDER = 'uREM'             # User creating a simple reminder
EMAIL_SUBTYPE_USER_PARSE_FAILED  = 'uPFD'               # Cannot translate the email
EMAIL_SUBTYPE_USER_TAGONLY = 'uTAG'                     # Default email type
EMAIL_SUBTYPE_USER_FILE = 'uFIL'                        # Email with attached file (if no event or reminder)
EMAIL_SUBTYPE_USER_FORMMAIL_CONFIRM = 'uFRC'            # Placeholder in the newsfeed -- a form mail was sent out
EMAIL_SUBTYPE_USER_FOLLOWUP_REMINDER = 'uFWR'

EMAIL_SUBTYPE_NOTED_FORWARD = 'nFWD'
EMAIL_SUBTYPE_NOTED_REGISTER    =   'nREG'
EMAIL_SUBTYPE_NOTED_REMINDER    =   'nREM'
EMAIL_SUBTYPE_NOTED_UNUSED_REMINDER =   'nUNU'
EMAIL_SUBTYPE_NOTED_EXPIRATION_SOON_REMINDER = 'nEXS'
EMAIL_SUBTYPE_NOTED_DIGEST      =   'nDIG'
EMAIL_SUBTYPE_NOTED_CONTACT_REPLY = 'nCOR'
EMAIL_SUBTYPE_NOTED_FORMMAIL_SUCCESS = 'nFM+'
EMAIL_SUBTYPE_NOTED_FORMMAIL_FAIL = 'nFM-'
EMAIL_SUBTYPE_NOTED_IMPORT_SUCCESS = 'nIM+'
EMAIL_SUBTYPE_NOTED_IMPORT_FAIL = 'nIM-'
EMAIL_SUBTYPE_NOTED_REGISTER_FAIL = 'nFAI'
EMAIL_SUBTYPE_NOTED_CONFIRMATION = 'nCON'
EMAIL_SUBTYPE_NOTED_TAG_LOOKUP =    'nTLK'
EMAIL_SUBTYPE_NOTED_CONFIRM_USER =  'nCOU'
EMAIL_SUBTYPE_NOTED_PENDING_USER = 'nPEN'
EMAIL_SUBTYPE_NOTED_ADMIN_NOTIFY = 'nADN'
EMAIL_SUBTYPE_NOTED_ASSISTANT_CHANGED = 'nACH'
EMAIL_SUBTYPE_NOTED_REQUEST_FILE_PERMS = 'nRFP'
EMAIL_SUBTYPE_NOTED_GRANT_FILE_PERMS = 'nGFP'
EMAIL_SUBTYPE_NOTED_ASK_STATUS = 'nAST'
EMAIL_SUBTYPE_NOTED_INVITE = 'nINV'
EMAIL_SUBTYPE_NOTED_PAYMENT_RECEIPT = 'nPAY'
EMAIL_SUBTYPE_NOTED_NOTIFY_TRACKER = 'nNOT'
EMAIL_SUBTYPE_RESET_PASSWORD_LINK = 'nRPW'
EMAIL_SUBTYPE_NOTED_ASSIGN_THREAD = 'nASN'
EMAIL_SUBTYPE_NOTED_WARN_MAX_USERS_EXCEEDED = 'nMXU'

EVENT_STATUS_TENTATIVE =    'evtT'
EVENT_STATUS_SCHEDULED =    'evtS'
EVENT_STATUS_CANCELED =     'evtX'
EVENT_STATUS_COMPLETED =    'evtC'
EVENT_STATUS_FOLLOWUP_REMINDER = 'evtR'
EVENT_STATUS_TAGONLY =      'evt-'                  # Most events are this type, just holds a snippet
EVENT_STATUS_FADING =       'evfa'                  # TODO: Get rid of this
EVENT_STATUS_SCHEDULED_FADING = 'evfs'
EVENT_STATUS_TENTATIVE_FADING = 'evft'

EVENT_PERMS_PRIVATE          =   'ePRI'
EVENT_PERMS_PROTECTED        =   'ePRO'
EVENT_PERMS_PUBLIC           =   'ePUB'

EVENT_RESPONSE_TYPE_YES =       'ert+'          # Can do the event (for tentative events only)
EVENT_RESPONSE_TYPE_TENTATIVE = 'ert?'          # Tentatively accept
EVENT_RESPONSE_TYPE_NO =        'ert-'          # Completely reject the event
EVENT_RESPONSE_TYPE_UNKNOWN =   'ert.'          # Can't parse the response

EVENT_RESPONSE_STATUS_AWAITING =        'erpA'
EVENT_RESPONSE_STATUS_NONE =            'erp-'
EVENT_RESPONSE_STATUS_RECEIVED =        'erpR'
EVENT_RESPONSE_STATUS_DISABLED =        'erp.'
    
REPEAT_TYPE_WEEKLY =        'WEE'
REPEAT_TYPE_WEEKLY_2 =      'WE2'
REPEAT_TYPE_MONTHLY =       'MON'
REPEAT_TYPE_MONTHLY_2 =     'MO2'
REPEAT_TYPE_QUARTERLY =     'MO3'
REPEAT_TYPE_MONTHLY_6 =     'MO6'
REPEAT_TYPE_YEARLY =        'YEA'

TAG_TYPE_OBJECT =           'tOBJ'
TAG_TYPE_PERSON =           'tPER'
TAG_TYPE_GROUP =            'tGRP'              # not used yet...
TAG_TYPE_BUCKET =           'tBUC'

# Modes for retrieving text content from an email
CONTENT_MODE_ALL = 'all'            # Get message text including footers and quotes
CONTENT_MODE_MIN = 'min'            # Get message text, no footers, no quotes
MAX_CONTENT_BODY_LENGTH = 10000     # Maximum message body length we store, in bytes
MAX_PROCMAIL_REMINDER_LENGTH = 5000 

MAILSERVER_POP = 'sPOP'
MAILSERVER_IMAP = 'sIMP'
MAILSERVER_OWA = 'sOWA'
MAILSERVER_YAHOO = 'sYHO'

CHRONIC_PEAK_TAGGED =   3
CHRONIC_PEAK_REGULAR =  2
CHRONIC_PEAK_OTHER  =   1
CHRONIC_PEAK_NONE   =   0

ICS_DAY_NAMES = ['MO','TU','WE','TH','FR','SA','SU']    # Starts on Monday because that's Python's numbering

DAY_PORTIONS = [
    {
        'key':          'am',
        'hours':        (0, 12),
        'text':         _('am'),
        'suffix_text':  _('in the a.m.'),
        'modify_today': False,
    },
    {
        'key':          'pm',
        'hours':        (12, 24),
        'text':         _('pm'),
        'suffix_text':  _('in the p.m.'),
        'modify_today': False,
    },    
    {
        'key':          'morning',
        'hours':        (6, 12),
        'text':         _('morning'),
        'suffix_text':  _('morning'),
        'modify_today': True,
    },       
    {
        'key':          'afternoon',
        'hours':        (12, 17),
        'text':         _('afternoon'),
        'suffix_text':  _('afternoon'),
        'modify_today': True,
    },     
    {
        'key':          'evening',
        'hours':        (17, 24),
        'text':         _('evening'),
        'suffix_text':  _('evening'),
        'modify_today': True,
    },  
    {
        'key':          'night',
        'hours':        (17, 24),
        'text':         _('night'),
        'suffix_text':  _('night'),
        'modify_today': True,
    }
]


DATETIME_LOW = pytz.utc.localize(datetime(2, 1, 1, 0, 0))
HOWLONG_SHOW_ACCOUNT_EXPIRATION = timedelta(days=3)     # How long in advance we show the # of days remaining in the account above the newsfeed
HOWLONG_INACTIVATE_ACCOUNT_MAX_USERS_EXCEEDED = timedelta(days=14)  # How many days we give admins to upgrade accounts that have too many users

HOWLONG_REMINDER_STALE_EVENT = timedelta(days=7)
HOWLONG_REMINDER = timedelta(days=1)                    # TODO: Smarter handling of this
HOWLONG_REMINDER_REPEATING = timedelta(hours=4)         # TODO: Smarter handling of this
HOWLONG_REMINDER_LAST_MINUTE = timedelta(minutes=65)    # How long in advance to send "last minute" reminders for events
                                                        #  I made this slightly longer than the "real" interval of 60
                                                        #  minutes, because the cron job is run every 30 minutes and meetings
                                                        #  tend to be scheduled for :00 or :30 of the hour -- to avoid
                                                        #  roundoff / clock inaccuracy problems
HOWLONG_REMINDER_WAIT = timedelta(minutes=70)           # Don't send a reminder, if one has been sent within this period of time
                                                        #  (The original confirmation email counts as a reminder)
                                                        
HOWLONG_REMINDER_UNUSED = timedelta(days=10)            # If login hasn't been used for this long, send them a new blog post...

HOWLONG_MEETING_DEFAULT = timedelta(minutes=30)         # If we get a zero-width span from Chronic and it's a scheduled event,
                                                        #  use this
HOWLONG_MOSSO_FILES = timedelta(days=30)
                                      
HOWLONG_REFRESH_DISABLED = timedelta(minutes=1)         # When they click Refresh in the newsfeed, how long before they can click it again
 
HOWLONG_IMAP_CRAWL_DEFAULT = timedelta(days=7)          # How many days back to crawl, if not set in the service level

if settings.SERVER == 'local':                          
    HOWMANY_IMAP_CRAWL_USER = 20                        # How many messages back to crawl, initially
    HOWMANY_IMAP_CRAWL_CRON = 30                        # How many messages back to crawl, initially
else:
    HOWMANY_IMAP_CRAWL_USER = 50                        # How many messages back to crawl, initially
    HOWMANY_IMAP_CRAWL_CRON = 150

HOWMANY_IMAP_FOLDERS_MAX = 25                           # Max # of IMAP folders we crawl

HOWMANY_CRAWL_FAILURES_TO_DISABLE = 2
HOWLONG_CRAWL_FAILURES_TO_DISABLE = timedelta(days=1)

HOWLONG_KILL_STUCK_JOB = timedelta(minutes=5) 
#HOWLONG_IMAP_TIMEOUT = 10                               # in seconds (must be a bare number)
HOWLONG_SMTP_TIMEOUT = 10                               # in seconds (must be a bare number)
HOWLONG_FILE_PERMISSION = timedelta(hours=24)
HOWLONG_ASK_STATUS_UPDATE = timedelta(hours=24)
HOWLONG_ASK_EVENT_RESPONSE = timedelta(hours=24)        # How often to ask for event responses (for all kinds of events)
HOWLONG_EXPIRE_TAGONLY_RESPONSE = timedelta(days=3)     # How long after the fact do we give up asking for tag-only responses
HOWLONG_PAYMENT_GRACE = timedelta(days=2)                # Grace period after account 'expires' that we don't shut it off, just in case
HOWLONG_DELETE_EXPIRED_ACCOUNTS = timedelta(days=90)    # How old inactive accounts can get before we automatically delete them completely
HOWLONG_AGO_ICAL_FEED = timedelta(days=14)              # How far back the .ics HTTP feed goes                                                       

HOWLONG_REPLY_GUESS_THREADING = timedelta(days=14)      # How far back we look for the parent of an email with missing reply id
HOWLONG_REPLY_GUESS_THREADING_PARANOID = timedelta(days=7)    # How far back we look for the parent of an email with missing reply id
HOWLONG_REPLY_SLACK = timedelta(minutes=2)              # How far forward we look for the parent of an email with missing reply id
                                                        #  (since clocks may not be perfectly synced across mail servers)
                                                        #  0 for now, since this 'feature' was causing threading problems
#HOWLONG_COMING_UP = timedelta(days=14)                  # How far ahead we show events in the Coming Up box

HOWMANY_ASK_EVENT_RESPONSE = 1

HOWLONG_PARSE_QUICK = timedelta(seconds=10)
HOWLONG_PARSE_QUICK_BACKOFF = timedelta(seconds=60)
MAX_PARSE_QUICK = 10

# Threading settings
MIN_SUBJECT_LEN_THREADING_GUESS = 1
MIN_DIFFSCORE_GUESSING = 0.7                            # How similar email addresses must be to consider them the same, for threading guessing

# Formmail settings
HOWMANY_FORMMAIL_CONTACTS_REQUIRE_UNSUBSCRIBE_LINK = 10

# In seconds, how long between consecutive retries of sending mail
FORMMAIL_RETRY_INTERVALS = [3600, 3600*4, 3600*25]  

# These are currently not used. (eg 7/22)
HOWMANY_FORMMAIL_EMAILS_AT_ONCE = 5
HOWLONG_FORMMAIL_DELAY_BETWEEN_BATCHES = 20     # after every 5 emails, pause for this many seconds

HOWLONG_FORMMAIL_COMPOSE_CONFIRM = timedelta(hours=48)  # If form-mailing a person again before this much time has elapsed,
                                                        #  confirm with user
                                                            
HOWFAR_COMBINE_SPANS = 8                                # How many words apart (at most) can the peaks of 
                                                        # consecutive date/time spans be before we combine them
                                                        # e.g. "Meeting Friday with Bob at 4:30pm"
HOWMANY_TOKENS_GUESS_UNTAGGED_DATETIMES = 40
# HOWMANY_TOKENS_PARSE_UNTAGGED_DATETIMES = 0             # For now, just disable all date/time parsing that doesn't have @

NUM_FOOTER_LINES_SEARCH = 40

#HOWMANY_COMING_UP = 6                                   # Show this many items in the Coming Up box

NEWSFEED_MAX_ITEMS = {                                  # How many items to show in the Conversations widget
    'firefox': 4,                                       # Firefox/Chrome plugin
    'default': 15                                       # Any other agent
}
                      
HOWMANY_CONTACTS_COMPACT = 25
HOWMANY_CONTACTS_FULL = 100
MAX_CONTACTS_DISPLAY_SCHEDULED = 8                       # Per newsfeed item
MAX_CONTACTS_DISPLAY_FILES = 8
#HOWMANY_CONTACTS_DISPLAY_RECENT = 15

MAX_LENGTH_EMAIL = 75                                   # Max length of an email address in the DB

#MAX_REMINDER_UNUSED_BLOG_POSTS = 5                      # How many blog posts to display in the "unused account" mails

MAILSERVICE_GMAIL = 'mGMA'
MAILSERVICE_GOOGLE_APPS = 'mGAP'
MAILSERVICE_YAHOO = 'mYHO'
MAILSERVICE_HOTMAIL = 'mHOT'
MAILSERVICE_OTHER_IMAP = 'm--I'
MAILSERVICE_OTHER_OWA = 'm--O'
MAILSERVICE_OTHER_POP = 'm--P'

# Values:
#    Undefined: don't display them
#    Specific value:
#        if "", display them as blank
#        if "somevalue", fill them in but don't display them
#        if "<blah>", a macro -- fill them in, and display them
    
MAIL_SERVICES = [        
    {
        'service':  MAILSERVICE_GMAIL,
        'name':     'IMAP - Gmail (secure)',
        'type':     MAILSERVER_IMAP,
        'imap_server':  'imap.gmail.com',
        'imap_port':    993,
        'imap_use_ssl': 'true',
        'imap_username': '<email_username>',         # this tells the Javascript to fill in the part of alias.email before the @
        'smtp_server': 'smtp.gmail.com',
        'smtp_port':    587,
        'smtp_use_ssl': 'true',
        'smtp_username': '<email_username>',
        'smtp_tracks_imap': 'true',
        'msg': _("Enable IMAP in Gmail settings.")
    },
    {
        'service':  MAILSERVICE_GOOGLE_APPS,
        'name':     'IMAP - Google Apps (secure)',
        'type':     MAILSERVER_IMAP,
        'imap_server':  'imap.gmail.com',
        'imap_port':    993,
        'imap_use_ssl': 'true',
        'imap_username': '<email>',          # this tells the Javascript to fill in alias.email
        'smtp_server': 'smtp.gmail.com',
        'smtp_port':    587,
        'smtp_use_ssl': 'true',
        'smtp_username': '<email>',
        'smtp_tracks_imap': 'true',
        'msg': _("Enable IMAP in Google Apps settings.")
    },     
    {  
        'service':  MAILSERVICE_OTHER_IMAP,
        'name':     _('IMAP/Exchange - Other'),
        'type':     MAILSERVER_IMAP,
        'imap_server':      '',
        'imap_server_prompt':   _('Hostname or IP:'),
        'imap_port':        '',
        'imap_use_ssl':     '',
        'imap_username':    '',
        'smtp_server':      '',
        'smtp_port':        '',
        'smtp_username':    '',
        'smtp_password':    '',
        'smtp_use_ssl':     ''
    },
#    {
#        'service':  MAILSERVICE_YAHOO,
#        'name':     _('Yahoo Mail Plus'),
#        'type':     MAILSERVER_YAHOO,
#        'smtp_tracks_imap': 'true',
#    },
    
#    {  
#        'service':  MAILSERVICE_YAHOO,
#        'name':     _('POP - Yahoo Mail Plus'),
#        'type':     MAILSERVER_POP,
#        'imap_server': 'plus.pop.mail.yahoo.com',
#        'imap_username': '<email>',          # this tells the Javascript to fill in alias.email
#        'imap_use_ssl': 'true',
#        'imap_port':    995,
#        'smtp_server': 'plus.smtp.mail.yahoo.com',
#        'smtp_port':    465,
#        'smtp_use_ssl': 'true',
#        'smtp_username': '<email>',
#        'smtp_tracks_imap': 'true',
#        'msg': _("POP is not recommended as it marks all mail read.")
#    },
#    {  
#        'service':  MAILSERVICE_HOTMAIL,
#        'name':     _('POP - Hotmail'),
#        'type':     MAILSERVER_POP,
#        'imap_server': 'pop3.live.com',
#        'imap_use_ssl': 'true',
#        'imap_username': '<email>',      
#        'imap_port':    995,
#        'smtp_server': 'smtp.live.com',
#        'smtp_port':    25,
#        'smtp_username': '<email>',
#        'smtp_use_ssl': 'true',
#        'smtp_tracks_imap': 'true',
#        'msg': _("POP is not recommended as it marks all mail read.")
#    },    
#    {  
#        'service':  MAILSERVICE_OTHER_POP,
#        'name':     _('POP - Other'),
#        'type':     MAILSERVER_POP,
#        'imap_server':      '',
#        'imap_port':        '',
#        'imap_use_ssl':     '',
#        'imap_username':    '',
#        'smtp_server':      '',
#        'smtp_port':        '',
#        'smtp_username':    '',
#        'smtp_password':    '',
#        'smtp_use_ssl':     '',
#        'msg': _("POP is not recommended as it marks all mail read.")
#    },    
    {  
        'service':      MAILSERVICE_OTHER_OWA,
        'name':         _('Web - Outlook Web Access'),
        'type':         MAILSERVER_OWA,
        'imap_server_prompt':   'URL:',
        'imap_server':          '<prefix>',
        'imap_username':    '<email_username>',
        'smtp_server':      '',
        'smtp_port':        '',
        'smtp_username':    '',
        'smtp_password':    '',
        'smtp_use_ssl':     '',
        'msg': _("Enter the URL to Exchange OWA.")
    },    
]

ICS_MIMETYPES = ['text/calendar', 'application/ics']

PASSWORD_BULLET_CHAR = '~'

HTML_TEXT_OUTLOOK_CONVERSION_STRING = "Converted from text/plain format"

WORDLISTS = {
    'names_male':       ['male.txt',                None],
    'names_female':     ['female.txt',              None],
    'words_2of4':       ['2of4brif.txt',            None],
    'surnames':         ['surnames.txt',            None]
}      

def get_wordlist(key):
    
    def read_wordlist(filename):
        filename = os.path.join(DATADIR, filename)
        f = open(filename, 'rb')
        names = [s.strip() for s in f.readlines()]
        f.close()
        return names   
    
    if WORDLISTS[key][1] is None:
        WORDLISTS[key][1] = read_wordlist(WORDLISTS[key][0])
    return WORDLISTS[key][1]


# TODO: Convert the following code to use the above

def read_word_freq_table(filename, startline=None, endline=None):
    result = []
    filename = os.path.join(DATADIR, filename)
    f = open(filename, 'rb')
    for line in f.readlines()[startline:endline]:
        name = line.split()[0]
        if name:
            if name[-1] == '*':
                name = name[:-1]
            result.append(name)
    f.close()
    return result
    
def read_words(filename, startline=None, endline=None):
    result = []
    filename = os.path.join(DATADIR, filename)
    f = open(filename, 'rb')
    for line in f.readlines()[startline:endline]:
        result.extend(line.split())
    f.close()
    return result

BANNED_PASSWORDS = set(read_words('banned_passwords.txt'))

# List of second level domains around the world, like .co.uk
SECOND_LEVEL_DOMAINS = set(read_words('second_level_domains.txt'))

# TODO: Read these from a file
COMMON_TIMEZONES = [t for t in pytz.common_timezones if t.startswith('US')] +\
    [t for t in pytz.common_timezones if not t.startswith('US')]
    
# For these domains, don't automatically assume that two email addresses belonging to this
#  domain must be the same company (e.g., gmail, hotmail)
#  List is formatted one domain (or domain prefix) per line.
#  Use the function is_common_domain in utils.py to test membership.
COMMON_DOMAINS = set(read_words('common_email_domains.txt'))

# For these domains, the bucketizer should pay attention to the part before the @
COMMON_LISTSERV_DOMAINS = [\
    'googlegroups'
]

EXCLUDE_ICS_DOMAINS= [\
    'aol', 
#    'zmail'    # for debugging
]

# In addition to the ignorable addresses, ignore senders that match any of these
SENDER_NO_SECONDARY_EMAIL = ['calendar-notification@google.com']

BUCKET_VERSION              =   4           # 5/30/09
BUCKET_SUBVERSION           =   0

# Types of file tokens (specifies what kind of permission the person has for this file)

# These are tokens that never expire
FILETOKEN_TYPE_FROM         =   'ftFR'              # This person actually sent the file
FILETOKEN_TYPE_CRAWLED      =   'ftCR'              # File was crawled from this person's email box
FILETOKEN_TYPE_TOCC         =   'ftTC'              # This person received the file in an email
FILETOKEN_TYPE_OTHER        =   'ftOT'              # Anyone else who can permanently see this file

# This kind of token expires
FILETOKEN_TYPE_TEMP         =   'ft--'              # A regular token, good for a limited time only
    

# Types of file permissions (these are global to an entire file)

FILE_PERMS_PRIVATE          =   'fPRI'
FILE_PERMS_PROTECTED        =   'fPRO'
FILE_PERMS_PUBLIC           =   'fPUB'

CRYPTER = keyczar.Crypter.Read(settings.KEYS_DIR)
CRYPTER_TRACKER = keyczar.Crypter.Read(settings.KEYS_TRACKER_DIR)
        
CLEAR_AGENTS_AT_ROOT = ['salesforce', 'outlook']

SPECIAL_SESSION_DOMAINS = [                         # If the domain matches one of these, set a session flag
    (r'(www\.)?localhost?$',         'show_sales'),
    (r'(www\.)?clerk.ccl?$',            'show_assistant'),
    (r'(www\.)?salesforce\..*$',        'show_salesforce'),
    (r'college\..*$',           'show_college'),
    (r'(www\.)?secret\..*$',            'show_secret')
]
    
SEARCH_TYPE_CONTACT = 'wCON'
SEARCH_TYPE_TEXT = 'wTXT'
SEARCH_TYPE_COMPANY = 'wCOM'
SEARCH_TYPE_NEWCONTACT = 'wnco'             # "search" that puts the contacts widget in the "create a new contact"
                                            #  mode, and puts the other widgets into the empty set
SEARCH_TYPE_NORESULTS = 'w---'              # search that always returns the empty set


RE_SEPARATOR = re.compile(r"^[\s]*((---*)|(===*)|(\*\*\**))[\s]*$")
RE_GOOGLE_CALENDAR_FOOTER = re.compile(r"You are receiving this email at the account")
#RE_EMAIL = re.compile(r"\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}\b")
RE_EMAIL_SEARCH = re.compile(r"\b[A-Za-z0-9._%=+-]+@([A-Za-z0-9-]+\.)*([A-Za-z-]{2,})\b")   # for finding an email address in a string
RE_EMAIL = re.compile(r"^[A-Za-z0-9._%=+-]+@([A-Za-z0-9-]+\.)+([A-Za-z-]{2,})$")            # for verifying that a string is an email address
RE_DOMAIN = re.compile(r"\b([A-Za-z0-9-]+\.)*([A-Za-z-]{2,})\b")
RE_TWITTER = re.compile(r"@?[A-Za-z0-9_]+$")
RE_URL = re.compile(r"(?i)(http://)?([A-Za-z0-9\-]+\.)+([A-Za-z]{2,6})([/?][A-Za-z0-9\-/?%=&.,]*)?")
# US phone number
RE_PHONE = re.compile(r"(?i)\b(\(?(c(ell)?|m(obile)?|h(ome)?|w(ork)?|business|f(ax)?)\)?:? *)?\+?(1[-.]?)?\(?(?P<area>\d\d\d)[)-.]? ?(?P<prefix>\d\d\d)( ?[-.] ?| )(?P<number>\d\d\d\d)(\s*(x|ext\.?|extension)(?P<extension>\d+))?\b")
# Int'l phone number, + is required
RE_PHONE2 = re.compile(r"\(?\+(?P<country_code>\d{1,3})\)?\s{0,2}(?P<number>[()\d -.]{6,}\d)") 
RE_DEVICE = re.compile(r"(?i)\s*sent (from|via) (my )?(?P<device>.*)$")    
RE_COMPANY1 = re.compile(r"([\w+\-]+\s+)*([\w+\-]+\s*),?\s*((C|c)orp\.?|Corporation|(I|i)nc\.?|Incorporated|(Ll)td\.?|Limited|(LLC|llc))\b")
institution_re = r"\b(University|Univ\.|School|College|Laboratory|Labs|Office|Laboratory|Laboratories|Institutes?|Foundation|Chamber|Group|Communications|Enterprises|Ventures|" + \
    r"Services|Sciences?|Software|Networks?)\b"
RE_COMPANY2 = re.compile(r"([\w+\-]+\s+)+" + institution_re + r"(\s+[\w+\-]+)*")    # 'Princeton University', etc.
RE_COMPANY3 = re.compile(r"([\w+\-]+\s+)*" + institution_re + r"(\s+[\w+\-]+)+")    # 'University of Iowa', etc.
title_re = r"\b(Dept\.|Department|Division|Professor|Accountant|CPA|Director|(Vice[ -])?President|Principal|Principle|" + \
    r"Executive|Manager|Chair|Chairperson|Chairman|Associate|Editor|CTO|CEO|CFO|CXO|COO|VP|Founder|Finance|Sales|Engineering|Development)"
RE_TITLE1 = re.compile(r"([A-Za-z+\-]+\s+)?([&A-Za-z+\-]+\s+)?" + title_re + r"\s*,?(\s+[&A-Za-z+\-]+)+")   # President, Blah Division
RE_TITLE2 = re.compile(r"([A-Za-z+\-]+\s+)?([&A-Za-z+\-]+\s+)?" + title_re)
RE_ADDRESS = re.compile(r"[^|,;]* (P\.?O\.? ?(B|b)ox|Suite|Ste\.|Unit|Road|(R|r)d\.|Street|(S|s)t\.|Avenue|(A|a)ve\.|Boulevard|(B|b)lvd\.?|Circle|(C|c)ir\.|Cr\.|Lane|Ln\.|Highway|Hwy\.?|Parkway|Pkwy\.?)")
RE_ZIP = re.compile(r"\b\d{5}(-\d{4})?\b")
RE_GREETING = re.compile(r"(?i)(hello|hey|greeting|\bhi\b|welcome|howdy|Dear\b|to whom)")
RE_CLOSING_LOW = re.compile(r"^\w+(\s\w+)?,\s*$")
RE_CLOSING_HI = re.compile(r"(?i)^(thanks?|sincerely|all the best|best\b|cordially)(\s\w+)?([,.!]\s*)$")
RE_NAME1 = re.compile(r"([-=]|\s)*([A-Z][a-z]+)\s+([A-Z]([a-z]+|\.)\s+)?([A-Z]['\-a-z]+)\s*$")
RE_NAME2 = re.compile(r"[A-Z][a-z]+\s\s?[A-Z][\.]?\s\s?[A-Z][a-z]+")
RE_DISCLAIMER = re.compile(r"(?i)(notice|disclaimer|confidential|proprietary)")

RE_REMOVEWS = re.compile(r"[\t\n]{2,}")
# SF Leads must have a company name; if we don't have one, use this

RE_BANG = re.compile(r'(^|\s)@!($|\W)')
RE_QUES = re.compile(r'((^|(^|[^./A-Za-z0-9=])[/A-Za-z0-9!]+|\s)\?+($|[^A-Za-z0-9=?]))')  # count ?'s, but not ones in URLs
 
RE_TOKENIZE = re.compile(r'(@?\w+(-\w+)*(\.(-\w+)+)*|[^\w\s]+)')

RE_REFERER_SALESFORCE = re.compile(r'https://.*\.(sales)?force\.com')

SF_COMPANY_PLACEHOLDER = '*'

MAX_CSV_COLUMNS = 50

DEFAULT_SOCIAL_IMAGE_SIZE = 80
