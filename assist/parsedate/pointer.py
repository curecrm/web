import re
from tokens import *

class Pointer(Tag):
    # TODO: Where's the best place to lower-case the words?
    @staticmethod
    def scan(tokens):
        scanner = {
            r"^(ago|before|past)$":             PD_PAST,
            # r"^(hence|after|from|future)$":     PD_FUTURE,
            r"^in$":                            PD_FUTURE
        }
        for token in tokens:
            for pattern in scanner:
                if re.match(pattern, token.word):
                    token.tag(Pointer(scanner[pattern]))
                    break
        return tokens

    def __repr__(self):
        return 'P:%s' % self.data

