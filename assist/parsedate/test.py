# Unit testing for Chronic (actually, for the whole reminder.py thingy)

from django.db import models
from django.db import connection
from django.db.models import *
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib import admin
from django.template import Context, loader
from django.utils.translation import ugettext as _
import hashlib, random, email, calendar, uuid
import traceback
import json
import copy, pdb
from utils import *
from datetime import *
from assist.constants import *
from assist.models import *
import pytz
from assist import reminder
from assist import parsedate
from tokens import Span

now_d = None

def p(s):
    options = {
        'now': now_d if now_d else now().astimezone(pytz.timezone('US/Central'))
    }
    pd = parsedate.ParseDate(options)
    return pd.parse(s)

def next_weekday(dt, day):
    while dt.weekday() != day:
        dt = dt + ONE_DAY
    return dt

def today(hour=0, minute=0, second=0, days_from=0):
    if now_d:
        today = now_d
    else:
        today = now().astimezone(pytz.timezone('US/Central'))
    today = today.replace(hour=hour, minute=minute, second=second, microsecond=0)
    dir = 1 if days_from > 0 else -1  
    while days_from:
        today = today + ONE_DAY * dir
        days_from -= dir
    return reinterpret_by_tz(today)

def test_chronic():
    user_tz = pytz.timezone('US/Central')
    f = open(settings.PROJECT_DIR + '/docs/chronic_tests.txt', "r")
    results = []
    for line in f:
        line = line.strip()
        if (not line) or line[0] == '#':
            continue
        try:
            tup = eval(line)
        except:
            log("test_chronic: bad syntax in chronic_tests.txt:", line)
            continue
        if type(tup) is not tuple or len(tup) < 2:
            log("test_chronic: bad syntax in chronic_tests.txt:", line)
            continue            
        text = tup[0]
        start_ref = tup[1]
        end_ref = tup[2] if len(tup) >= 3 else None
        
        log ("^^^^^^^^^^^ testing: ", text)
        mode = {
            'source':   Email.SOURCE_DIRECT,
            'is_guest': False
        }
        global now_d
        now_d = datetime(2009, 3, 14, 11, tzinfo=user_tz)
        items = reminder.extract_items_from_text(text, now_d, mode)
        try:
            span = items[1][0]['span']
        except:
            span = None
        
        if type(start_ref) is str:
            span_start = p(start_ref)[0]
        else:
            span_start = Span(start_ref, start_ref)
        
        if not span_start:
            log("Couldn't parse span_start!")
            continue
        
        if not items:
            results.append((text, False, span))
            continue
        
        if not span:
            results.append((text, False, span))
        elif end_ref:
            if type(end_ref) is str:
                span_end = p(end_ref)[0]
            else:
                span_end = Span(end_ref, end_ref)
            
            if (span.start == span_start.start) and (span.end == span_end.start):
                results.append((text, True, span))
            else:
                #pdb.set_trace()
                results.append((text, False, span))
        else:
            if (span.start == span_start.start) and (span.end == span_start.end):
                results.append((text, True, span))
            else:
                #pdb.set_trace()
                results.append((text, False, span))
                
    log ("==============================")
    log ("test_chronic test results:")
    good_results, total_results = 0, 0
    for line, result, span in results:    
        if result:
            result_text = "PASSED"
            good_results += 1
        else:
            result_text = "FAILED"
        total_results += 1
        log('%s "%s":' % (result_text, line), span)
    log('%d passed out of %d' % (good_results, total_results))
        
