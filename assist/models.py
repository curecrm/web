from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db import connection
from django.db.models import *
from django.contrib.sessions.models import Session
from django.contrib.sessions.backends.db import SessionStore
from django.contrib.auth.models import User
from django.contrib import admin
from django.db import transaction
import hashlib
import re, string, os, hashlib, string
import json
from utils import *
# from datetime import *
import datetime
from constants import *
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager
from django.utils.translation import ungettext
from django.utils.translation import ugettext as _
from schedule.models import *
import pdb, traceback
from email import Charset, Encoders
from paypal.standard.models import *


''' This models.py file must be as Object Oriented as possible.  Each class below must have 
    static and instance methods that generate lists, perform critical actions, etc.
    
    Reference material:
    
        For admin model information: http://www.djangoproject.com/documentation/admin/ 
        (great docs on customizing the admin site)

        Possible Class field usage:
            + CommaSeparatedIntegerField
            + UTCDateTimeField for all timestamps
            
            
        New object methods: 
            # Update all the headlines with pub_date in 2007.
            Entry.objects.filter(pub_date__year=2007).update(headline='Everything is the same')

    '''


# Additional fields (possibly of same type as in the main model) for contacts and aliases
        
class ExtraInfo(Model):
    
    TYPE_EMAIL          =   'xEML'                                  # secondary email    
    TYPE_PHONE          =   'xPHN'                                  # secondary phone number
    # etc.
    owner_contact     =   models.ForeignKey('Contact', blank=True, null=True)           # every ExtraInfo belongs to a Contact
    data              =   TruncCharField(max_length=64, blank=True, null=True)
    type              =   models.CharField(max_length=4, blank=True, null=True)
    

class Tag(Model):
    TAG_TYPE = (
        (TAG_TYPE_OBJECT, 'Object'),
        (TAG_TYPE_PERSON, 'Person'),
        (TAG_TYPE_GROUP, 'Group'),
        (TAG_TYPE_BUCKET, 'Bucket')
    )
    
    text = TruncCharField(max_length=64)
    type = models.CharField(max_length=4, choices=TAG_TYPE, default=TAG_TYPE_OBJECT)
        
    def __unicode__(self):
        return u'%s (%s)' % (self.text, self.type)
    def __str__(self):
        return '%s (%s)' % (self.text, self.type)
    
    class Meta:
        unique_together = ("text", "type")


class Location(Model):
    address = models.CharField(max_length=255)
    address_sub = models.CharField(max_length=255, blank=True, null=True)
    zip = models.CharField(max_length=255, blank=True, null=True)
    state = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    country = models.CharField(max_length=255, blank=True, null=True)
    
    def __unicode__(self):
        return u'%s' % (self.address)
    def __str__(self):
        return self.address


class ServiceManager(Manager):

    def get_available_services(self, agent=None):    
        """
        Return a list of the (non-hidden) services that a user with this particular agent
        can choose from
        List free ones first, if applicable
        """
        services = Service.objects.filter(hidden=False)                 
        if agent == 'salesforce':
            services = services.filter(allow_sf=True)                   # If coming in through Salesforce, only show services that support it...
        elif agent == 'firefox':
            services = services.filter(allow_firefox=True)              # ditto
        elif agent == 'outlook':
            services = services.filter(allow_outlook=True)
        elif agent:
            services = services.filter(allow_mobile=True)
        
        services = list(services.order_by('name'))
        services_free = [s for s in services if s.is_free()]
        services_free_trial = [s for s in services if (s not in services_free) and s.trial_num_dmy]
        services_nonfree = [s for s in services if (s not in services_free) and (s not in services_free_trial)]
        services = services_free + services_free_trial + services_nonfree
        
        return services
        
class Service(Model): #model for different service levels, individual level, corporate service, etc.
    ''' The Service model defines what level of Service an Account gets.  
        How many free_request? Price_month? etc.
        '''
    
    slug = models.SlugField(unique=True)
    extra_slugs = models.CharField(max_length=256,null=True,blank=True,help_text='Any other slugs that we accept as belonging to this service') # 
                                                                        #  (for Paypal validation) - useful when one service is deprecated in favor of another
                                                                    
    name = models.CharField(max_length=256,null=True,help_text="example: 'Small-business (single account)', 'Small-business (entire company)'") # 
    
    # Free trial period
    trial_num_dmy = models.IntegerField(null=True, blank=True, help_text='If None, no trial period')      # 
    trial_dmy = models.CharField(max_length=1,null=True, blank=True, help_text='M, D, Y') #     
    
    price_per_num_dmy = models.FloatField(null=True, help_text="Price per num_dmy dmy's, per user")                    # 
    num_dmy = models.IntegerField(null=True)
    dmy = models.CharField(max_length=1,null=True, blank=True, help_text="M, D, Y") # 
    
    recurring = models.NullBooleanField(default=True, help_text='If service payments are recurring vs one-time.')                   # 
    
    max_users_flat_rate = models.IntegerField(null=True, blank=True, help_text='Max # of users that can use this service for the base price')    # 
    price_per_extra_user = models.FloatField(null=True, blank=True, help_text='For each user above max_users, charge this much per dmy period')     # 
    price_per_synced_email = models.FloatField(null=True, blank=True, help_text='Price for each email synced to Salesforce')   #  
    
    percent_paypal_fee = models.IntegerField(default=0, help_text='How much we raise the price for Paypal users  ')                 #                
    setup_fee = models.FloatField(null=True, blank=True, editable=False)                # NOT USED at the moment
    
    hidden = models.NullBooleanField(default=False, help_text='Do we hide this service from default package/service options in places like select boxes?')
    
    # Capabilities
    allow_crawl = models.NullBooleanField(default=True)
    allow_twitter = models.NullBooleanField(default=True)
    allow_sf = models.NullBooleanField(default=False)                   # Salesforce
    allow_sf_incoming = models.NullBooleanField(default=True)           # Do we sync Salesforce incoming mails?
    allow_sf_outgoing = models.NullBooleanField(default=True)           # Do we sync Salesforce outgoing mails?
    allow_firefox = models.NullBooleanField(default=True)
    allow_formmail = models.NullBooleanField(default=True)
    allow_outlook = models.NullBooleanField(default=True)
    allow_mobile = models.NullBooleanField(default=True)
    allow_social_lookups = models.NullBooleanField(default=True)
    
    max_queries_per_num_dmy = models.IntegerField(null=True, blank=True, editable=False) #not really using this
    max_storage_gb = models.FloatField(null=True, blank=True, help_text='Not really implemented', default=1.0, editable=False) #not fully implemented
    crawl_email_howlong = models.IntegerField(null=True, blank=True, help_text='Crawl email this many days back - if null, crawl back to: %s days' % HOWLONG_IMAP_CRAWL_DEFAULT)        # 
    max_store_email_howlong = models.IntegerField(null=True, blank=True, help_text='Measured in days - None means forever!')   # measured in days; None means forever
    hours_training = models.FloatField(null=True, blank=True, help_text='Set this to 0 to clearly communicate "limited support"') #not really using this.
    
    objects = ServiceManager()
    
    # Comparisons between services
    
    def __ge__(self, other):
        if  (self.allow_crawl >= other.allow_crawl) and\
            (self.allow_twitter >= other.allow_twitter) and\
            (self.allow_sf >= other.allow_sf) and\
            (self.allow_sf_incoming >= other.allow_sf_incoming) and\
            (self.allow_sf_outgoing >= other.allow_sf_outgoing) and\
            (self.allow_firefox >= other.allow_firefox) and\
            (self.allow_formmail >= other.allow_formmail) and\
            (self.allow_outlook >= other.allow_outlook) and\
            (self.allow_mobile >= other.allow_mobile) and \
            (self.allow_social_lookups >= other.allow_social_lookups):
            # Don't count max # of users here -- we're going strictly by features
            #((self.max_users_flat_rate is None) or ((other.max_users_flat_rate is not None) and self.max_users_flat_rate > other.max_users_flat_rate)):
            #(self.crawl_email_howlong >= other.crawl_email_howlong):
            # Don't count max # of users here! We don't want to say that SalesforceIntro >= other services just because it's 
            return True
        return False
    
    def __gt__(self, other):
        if self.__ge__(other):
            if  (self.allow_crawl > other.allow_crawl) or\
                (self.allow_twitter > other.allow_twitter) or\
                (self.allow_sf > other.allow_sf) or\
                (self.allow_sf and other.allow_sf and (self.allow_sf_incoming > other.allow_sf_incoming)) or\
                (self.allow_sf and other.allow_sf and (self.allow_sf_outgoing > other.allow_sf_outgoing)) or\
                (self.allow_firefox > other.allow_firefox) or\
                (self.allow_formmail > other.allow_formmail) or\
                (self.allow_outlook > other.allow_outlook) or\
                (self.allow_mobile > other.allow_mobile) or \
                (self.allow_social_lookups > other.allow_social_lookups) or \
                ((other.max_users_flat_rate is not None) and ((self.max_users_flat_rate is None) or (self.max_users_flat_rate > other.max_users_flat_rate))): # Do count max # of users here
                #(self.crawl_email_howlong > other.crawl_email_howlong):
                return True
        return False
        
    def __le__(self, other):
        return other.__ge__(self)
    
    def __lt__(self, other):
        return other.__gt__(self)
    
    def get_price_difference_string(self, other_service, account=None):
        "Return a string expressing how much more expensive this service is compared to some other service"
                
        promo = account and account.promo
        num_users = account.num_active_aliases() if account else 1
                
        this_price = self.calc_discounted_price(account=account)
        other_price = other_service.calc_discounted_price(account=account)

        this_price_per_synced_email = self.calc_discounted_price_per_synced_email(promo=promo)
        other_price_per_synced_email = other_service.calc_discounted_price_per_synced_email(promo=promo)

        if ((self.num_dmy == other_service.num_dmy) and (self.dmy == other_service.dmy) and (this_price > other_price) or (other_price == 0)):
            # If the num_dmy's are the same, then we can calculate a price difference
            result = '+ $%.2f/%s' % (this_price - other_price, format_payment_period(self.num_dmy, self.dmy, use_abbrevs=True))
            # For price_per_synced_email, don't show a price difference (that doesn't seem intuitive)
            if this_price_per_synced_email != other_price_per_synced_email:
                result += ' + $%.2f/%s' % (this_price_per_synced_email, _('email-synced to Salesforce'))
        else:
            result = '$%.2f/%s' % (this_price, format_payment_period(self.num_dmy, self.dmy, use_abbrevs=True))
            # For price_per_synced_email, don't show a price difference (that doesn't seem intuitive)
            if this_price_per_synced_email:
                result += ' + $%.2f/%s' % (this_price_per_synced_email, _('email-synced to Salesforce'))
        return result
    
    def calc_discounted_price(self, promo=None, account=None, num_users=None):
        """Return the fixed (per-payment-period) price for this service, 
        taking into account a promo and the # of users.
        
        If account is specified, get the promo and the # of users from account
        promo and num_users can also be specified separately
        """
        
        # Base price
        result = self.price_per_num_dmy
        
        # Number of users
        if (num_users or account) and (self.max_users_flat_rate is not None):
            result += self.price_per_extra_user * max(0, (num_users or account.num_active_aliases()) - self.max_users_flat_rate)
        
        # Promo
        promo = promo or (account and account.promo)

        if promo and promo.service and (promo.service != self):     # If promo restricted to one service, make sure it's this one
            promo = None
        if promo:
            if promo.dollar_discount is not None:
                result = max(result - promo.dollar_discount, 0)
            elif promo.percent_discount is not None:
                result = max(((100 - promo.percent_discount) / 100.0) * result, 0)
        
        return result

    def calc_discounted_price_per_synced_email(self, promo=None):
        """Return the per-usage price for this service, taking into account a promo"""
        result = self.price_per_synced_email or 0
        
        if promo and promo.service and (promo.service != self):     # If promo restricted to one service, make sure it's this one
            promo = None

        if promo and promo.percent_discount_per_synced_email:
            result = result * ((100 - promo.percent_discount_per_synced_email) / 100.0)
        return result

    def get_per_usage_string(self, promo=None):
        """Return a simple string giving the per-usage part of the pricing for this service."""
        amount = self.calc_discounted_price_per_synced_email(promo=promo)
        result = '$%.2f/%s' % (amount, _('email-synced to Salesforce'))
        return result
    
        
    def get_simple_terms_string(self, promo=None):
        """Return a simple string giving the total price (per dmy), assuming only 1 user.
        Doesn't count the per-usage part. 
        Used for the /@/register/Free page."""
        price = self.calc_discounted_price(promo=promo)
        result = '$%.2f/%s' % (price, format_payment_period(self.num_dmy, self.dmy, use_abbrevs=False))
        return result
    
    def get_medium_terms_string(self, account):
        """Fancier terms string -
        takes into account # of users and the price per synced email"""
        this_price = self.calc_discounted_price(account=account)
        
        result = ''
        
        if this_price:
            result += '$%.2f/%s' % (this_price, format_payment_period(self.num_dmy, self.dmy, use_abbrevs=True))
            num_active_aliases = account.num_active_aliases()
            
            if self.max_users_flat_rate is not None:
                display_num = max(self.max_users_flat_rate, num_active_aliases)
                result += _(' (%s%d user%s)') % (
                    _('up to ') if num_active_aliases <= self.max_users_flat_rate else '',
                    display_num,
                    's' if display_num > 1 else ''
                )
                
        price_per_synced_email = self.calc_discounted_price_per_synced_email(promo=account and account.promo)
        if price_per_synced_email:
            if result:
                result += ' + '
            result += '$%.2f/' % price_per_synced_email + _('email synced')
            
        if self.is_free(promo=account.promo):
            result = _('free')
            
        if account.promo and not (account.promo.service and (account.promo.service != self)):   # Don't count a promo if it doesn't match this service
            result += _(' (promo %d)') % account.promo.pk
            
        return result
    
    def get_alternate_terms_string(self):
        """Yet another terms string - 
        this is the preferred formatting for the dropdown inside the Salesforce plugin
        """
        
        if self.is_free():
            return _('Free (limited)')
        
        terms_parts = []
        
        if self.max_users_flat_rate is not None:
            users = _('%d user%s') % (self.max_users_flat_rate, 's' if self.max_users_flat_rate > 1 else '')
        else:
            users = _('Unlimited users')
        terms_parts.append(users)
        
        price = ''
        this_price = self.calc_discounted_price()
        if this_price:
            price += '$%.2f/%s' % (this_price, format_payment_period(self.num_dmy, self.dmy, use_abbrevs=True))
                
        price_per_synced_email = self.calc_discounted_price_per_synced_email()
        if price_per_synced_email:
            if price:
                price += ' + '
            price += '$%.2f/' % price_per_synced_email + _('email synced to Salesforce')
        terms_parts.append(price)
        
        result = ' - '.join(terms_parts)
        return result
    
        
    def _price_string(self, base_price, account):
        """Return a HTML string giving the total price (per dmy), accounting for the number of users.
        This calculation should match what calc_discounted_price above does!
        Given: base_price (the price per num_dmy after promo, possibly scaled by some factor)"""
        num_aliases = account.num_active_aliases() if account else 1
        if base_price == 0:
            return _('Free')
        elif (self.max_users_flat_rate is None) or num_aliases <= self.max_users_flat_rate: 
            return '$%.2f USD' % base_price
        else:
            extra_users = num_aliases - self.max_users_flat_rate
            extra_price_per_user = (self.price_per_extra_user / self.price_per_num_dmy) * base_price
            return '$%.2f + $%.2f x <a target="_blank" href="/dash/%s/admin/">%d %s</a> = $%.2f USD' % (\
                base_price,                     # flat-rate price per num_dmy after promo has been accounted for
                extra_price_per_user,           # price per extra user after promo has been accounted for
                account.base_url, 
                extra_users,            # number of additional users
                _('extra users') if extra_users > 1 else _('extra user'), 
                base_price + extra_price_per_user * extra_users
            )
          
    def get_fancy_terms_string(self, promo=None, disable_trial=False, account=None):
        """
        Return an HTML terms string with line breaks
        Used for the yellow box on the order and billing pages
        """
        
        def then(s):    return _('Then') + ' ' if s else ''
        
        if promo and promo.service and (promo.service != self):     # If promo restricted to one service, make sure it's this one
            promo = None

        # This is for the flat rate, not counting extra users
        price_per_num_dmy = self.calc_discounted_price(promo=promo)
            
        # The calculations for price based on # of users are all done inside of _price_string, not here.
        
        if self.is_free(promo=promo):
            return _('Completely Free')
        
        if promo and promo.disable_service_trial:
            disable_trial = True
        
        if promo and promo.trial_dmy:
            trial_num_dmy, trial_dmy = promo.trial_num_dmy, promo.trial_dmy
        else:
            trial_num_dmy, trial_dmy = self.trial_num_dmy, self.trial_dmy
        
        result = ''
        
        if price_per_num_dmy:
            if trial_dmy and not disable_trial:
                result += _('Free for the first %s<br/>') % format_payment_period(trial_num_dmy, trial_dmy) 
                
            if (promo is not None) and (promo.num_dmy is not None):
                term_ratio = promo.get_term_length() / self.get_term_length()
                result += then(result) + _('%s for %s') % (self._price_string(price_per_num_dmy * term_ratio, account), format_payment_period(promo.num_dmy, promo.dmy))
                result += _(' (you save %s)') % self._price_string((self.price_per_num_dmy-price_per_num_dmy) * term_ratio, account)
                result += '<br/>'
                
            result += then(result) + _('%s every %s<br/>') % (self._price_string(price_per_num_dmy, account), format_payment_period(self.num_dmy, self.dmy))
            
        price_per_synced_email = self.calc_discounted_price_per_synced_email(promo=promo)    
        if (price_per_synced_email):
            if result:
                result += ' + '
            result += _('$%.2f per email synced to Salesforce' % price_per_synced_email)
            
        return result
    
    def get_term_length(self):
        dmy_dict = {'D': 1, 'M': 30, 'Y': 365}
        return float(dmy_dict.get(self.dmy.upper(), 0) * (self.num_dmy or 0))
        
    def is_free(self, promo=None):
        "Is this service completely free (or, optionally, free with the given promo)?"    
        return (self.calc_discounted_price(promo=promo) == 0) and (self.calc_discounted_price_per_synced_email(promo=promo) == 0)
    
    def is_payment_required(self, upgrade=False, promo=None):
        "Does this service/promo combo require us to send them to the payment gateway immediately?"
                
        if self.is_free(promo=promo):
            return False
        if upgrade:                                             # if upgrading from another package, they must pay immediately
            return True
        if promo and promo.disable_service_trial:               # For now, we don't support promos that can be applied after the original signup [eg 12/20/09]
            return True
        if self.trial_num_dmy is None:                          # no trial period, they must pay immediately
            return True
        return False                                            # there's a trial period ... they can go w/o Paypal for a while
    
    def get_features_for_display(self, other=None, for_email=False):
        """Return a JSON list of features for this package
        If other is specified, only show the features where this one is superior to other"""
        
        site = Site.objects.get_current()
        has_ssl = (site.domain.lower() in settings.SSL_DOMAINS)
        
        if for_email:
            target = ''
            base_url = 'http%s://%s' % ('s' if has_ssl else '', site.domain)
        else:
            target = 'target="_blank"'
            base_url = ''
        
        result = []
        
        if (other is None):
            result.append({
                'type':     'checkbox',
                'name':     _('Service level: <b>%s</b>') % (self.name),
                'value':    True
            })
        
#        if other and self.crawl_email_howlong and other.crawl_email_howlong and (self.crawl_email_howlong > other.crawl_email_howlong):
#            result.append({     
#                'type':     'checkbox',
#                'name':     _('<b>%d day</b> email limit') % self.crawl_email_howlong,
#                'value':    True
#            })
            
        #if (other is None) or (self.max_users_flat_rate and other.max_users_flat_rate and (self.max_users_flat_rate > other.max_users_flat_rate)):
        if self.max_users_flat_rate is None:
            result.append({
                'type':    'checkbox',
                'name':     _('Unlimited users'),
                'value':    True
            })
        else:
            name = (_('Up to <b>%d</b> users') % self.max_users_flat_rate) if (self.max_users_flat_rate > 1) else _('Single user only')
            if self.price_per_extra_user > 0:
                name += "&nbsp;"
                name += '<a href="#" onclick="$(\'#extra_users_price_%s\').toggle(); $(this).hide(); return false;">?</a>' % str(self.id)
                name += '<span id="extra_users_price_'+ str(self.id) +'" style="font-size: .9em; color: #666; display:none;">' + (_('(extra: $%.2f/user/%s%s%s)') % (self.price_per_extra_user, '' if self.num_dmy == 1 else self.num_dmy + ' ', _('month') if self.dmy.upper() == 'M' else _('year'), 's' if self.num_dmy > 1 else '')) + "</span>"
            result.append({
                'type':     'checkbox',
                'name':     name,
                'value':    True
            })
            
        if (other is None) or self.allow_crawl > other.allow_crawl:
            result.append({     
                'type':     'checkbox',
                'name':     _('Email connector - IMAP/Gmail/Exchange'),
                'value':    self.allow_crawl
            })
            
        if (other is None) or (self.allow_sf > other.allow_sf) or \
            (self.allow_sf and other.allow_sf and ((self.allow_sf_incoming > other.allow_sf_incoming) or (self.allow_sf_outgoing > other.allow_sf_outgoing))):
            if self.allow_sf_incoming and self.allow_sf_outgoing:
                subfeatures = _('<b>in & outbound</b> email sync')
            elif self.allow_sf_incoming and not self.allow_sf_outgoing:
                subfeatures = _('<b>inbound</b> email sync only')
            elif not self.allow_sf_incoming and self.allow_sf_outgoing:
                subfeatures = _('<b>outbound</b> email sync only')
            else:
                subfeatures = ''
            result.append({
                'type':     'checkbox',
                'name':     _('Salesforce: %s <a %s href="%s/products/salesforce-automation/">&rarr;</a>') % (subfeatures, target, base_url),
                'value':    self.allow_sf
            })
        
        if (other is None) or (self.crawl_email_howlong > other.crawl_email_howlong):
            if self.crawl_email_howlong > 60:
                result.append({
                    'type':     'checkbox',
                    'name':     (_('<span style="background:#FCF8BC;">Historic email <b>archiving</b></span>: <b>%s days</b>') % self.crawl_email_howlong),
                    'value':    (self.crawl_email_howlong > 0)
                })
            else:
                pass
        
        if (other is None) or self.allow_outlook > other.allow_outlook:
            result.append({
                'type':     'checkbox',
                'name':     _('Outlook 2007 & 2010 connector <a %s href="%s/info/features/outlook/">&rarr;</a>') % (target, base_url),
                'value':    self.allow_outlook
            })
         
        if (other is None):
            result.append({
                'type':     'checkbox',
                'name':     _('Personal scheduling assistant <a %s href="%s/info/features/assistant/">&rarr;</a>') % (target, base_url),
                'value':    True
            })
            
        
        if (other is None) or self.allow_formmail > other.allow_formmail:
            result.append({     
                'type':     'checkbox',
                'name':     _('Personalized contact e-mailer <a %s href="%s/info/features/mailer/">&rarr;</a>') % (target, base_url),
                'value':    self.allow_formmail
            })
            
        if (other is None) or self.allow_mobile > other.allow_mobile:
            result.append({             
                'type':     'checkbox',
                'name':     _('Mobile site - iPhone, Android, etc. <a %s href="%s/info/features/mobile/">&rarr;</a>') % (target, base_url),
                'value':    self.allow_mobile
            })            

        if (other is None) or self.allow_firefox > other.allow_firefox:
            result.append({      
                'type':     'checkbox',
                'name':     _('Google mail add-on - Firefox / Chrome <a %s href="%s/install/">&rarr;</a>') % (target, base_url),
                'value':    self.allow_firefox
            })
        
            
        if (other is None):
            result.append({
                'type':     'checkbox',
                'name':     (_('Support - %s') % ('' + _('personal account rep.')) if (self.hours_training is not None and self.hours_training != 0.0) else _('Limited support')), #str(self.hours_training)
                'value':    True
            })
            
        if (other is None) or self.allow_social_lookups > other.allow_social_lookups:
            result.append({     
                'type':     'checkbox',
                'name':     _('<img style="max-height: 10px;" src="/static/images/social_logos/linkedin.ico"/> + age, gender, location'),
                'value':    self.allow_social_lookups
            })
            '''
            <img style="max-height: 10px;" src="/static/images/social_logos/myspace.ico"/> \
            <img style="max-height: 10px;" src="/static/images/social_logos/bebo.ico"/> \
            <img style="max-height: 10px;" src="/static/images/social_logos/plaxo.ico"/> \
            <img style="max-height: 10px;" src="/static/images/social_logos/hi5.ico"/> \
            <img style="max-height: 10px;" src="/static/images/social_logos/friendster.ico"/> \
            <img style="max-height: 10px;" src="/static/images/social_logos/flickr.ico"/> \
            <img style="max-height: 10px;" src="/static/images/social_logos/wordpress.ico"/> \
            <img style="max-height: 10px;" src="/static/images/social_logos/myyearbook.ico"/> \
            <img style="max-height: 10px;" src="/static/images/social_logos/twitter.ico"/>
            '''
            
        return result
            
            
    def allow_agent(self, agent):
        if agent=='sf' and not self.allow_sf:
            return False
        if agent=='firefox' and not self.allow_firefox:
            return False
        if agent=='outlook' and not self.allow_outlook:
            return False
        return True
    
    
    def __unicode__(self):
        return u'%s' % (self.name)
    

class Promo(Model):
    ''' TODO: eventually, this model will list all of the promo messages with their expirations, etc. 
        Promo messages live in the footer of emails for customers using the Free trial service.
        
        OR, Promos are keywords which let users signup at a discount or free.  To be defined soon.
        '''
    
    service = models.ForeignKey(Service, blank=True, null=True)         # the promo may be restricted to one service level.
    
    date_created = UTCDateTimeField(default=now)                        # Date the promo was created
    expiration = UTCDateTimeField(default=now, blank=True, null=True)   # Date after which new users can't sign up for this promo
                                                                        #  (NOT the date that the promo terms expire for folks already on the promo)
    percent_discount = models.IntegerField(null=True, blank=True)       # Only one of these should be non-Null
    dollar_discount = models.IntegerField(null=True, blank=True)
    
    percent_discount_per_synced_email = models.IntegerField(null=True, blank=True) # Percent discount on the per-email discount
    # How long a user is under the promo terms once they've signed up (None=forever)
    # Note: Paypal only supports a trial price for one billing cycle, so we give one
    # initial billing cycle with the price times num_dmy.
    
    disable_service_trial = models.NullBooleanField(default=True)           # when using this promo, don't allow the service's normal trial period
    num_dmy = models.IntegerField(null=True, blank=True)                # How long a user is under the promo terms once they're signed up
                                                                        #  (None=forever)
    dmy = models.CharField(max_length=1,null=True, blank=True)          # M, D, Y

    trial_num_dmy = models.IntegerField(null=True, blank=True)          # when using this promo, use this long of a trial period
    trial_dmy = models.CharField(max_length=1,null=True, blank=True) 
    
    name = models.CharField(max_length=256,null=True, blank=True)
    code = models.CharField(max_length=64, blank=True, null=True)                 # What you enter on the website to get this promo
    
    #require_payment_activation = models.BooleanField(default=True)      # If using this promo, do we direct them to the payment gateway immediately?
                    
    def __unicode__(self):
        if self.percent_discount is not None:
            disc = '%% off: %d' % self.percent_discount
        elif self.dollar_discount is not None:
            disc = '$ off: %d' % self.dollar_discount
        else:
            disc = '???'
        return u'%s %s (%s) [expires: %s]' % (self.code, self.name, disc, str(self.expiration))

    def get_promo_string(self):
        if self.percent_discount == 100:
            if self.num_dmy is None:
                return _("free service")
            else:
                return format_payment_period(self.num_dmy, self.dmy) + _(" of free service")
        else:
            if self.percent_discount:
                s = "%d%% off" % self.percent_discount
            elif self.dollar_discount:
                s = "$%d off" % self.dollar_discount
            else:
                s = ''
            if self.num_dmy is not None:
                s += _(" for ") + format_payment_period(self.num_dmy, self.dmy, include_one=True)
            return s

    def get_term_length(self):
        dmy_dict = {'D': 1, 'M': 30, 'Y': 365}
        return float(dmy_dict.get(self.dmy.upper(), 0) * (self.num_dmy or 0))
    
    
class AssistantManager(Manager):

    def create_assistant(self, assistant_email, account):
        assistant_email = assistant_email.lower()
        assistant_first_name, assistant_last_name = get_first_last_name_from_email(assistant_email, use_namelist=False)
        assistant = Assistant(email=assistant_email,\
            first_name=assistant_first_name, last_name=assistant_last_name, owner_account=account)
        assistant.save()    
        return assistant

class Assistant(Model):
    email = models.EmailField(blank=True)       # eventually, make this unique
    first_name = TruncCharField(max_length=64, blank=True)
    last_name = TruncCharField(max_length=64, blank=True)
    owner_account = models.ForeignKey('Account', related_name='assistant_set')

    objects = AssistantManager()

    def get_full_name(self):
        return ' '.join([self.first_name, self.last_name]).strip()
    def get_domain(self):
        return self.email.split('@')[-1]
    def __unicode__(self):
        return u'%s' % (self.email)
    def __str__(self):
        return self.email
    # def get_user(self):
    #     return UserProfile.objects.get(assistant=self).user

class AccountManager(Manager):
    
    def active(self):
        return self.filter(status=ACCOUNT_STATUS_ACTIVE)
    
    def inactivate_expired(self):
        log('inactivate_expired...')
        for account in Account.objects.filter(status=ACCOUNT_STATUS_ACTIVE, date_expires__lt=now()-HOWLONG_PAYMENT_GRACE).exclude(date_expires=None):
            # Allow some customers "extra days" beyond the official expiration of their account
            if account.date_expires and (account.date_expires < now()-HOWLONG_PAYMENT_GRACE-timedelta(days=account.extra_days)):
                log('Inactivating account %s' % str(account))
                account.status = ACCOUNT_STATUS_INACTIVE
                account.date_modified = now()
                account.save()
        log('... done.')
        
    def delete_pending(self):
        """
        Delete all accounts that are marked ACCOUNT_STATUS_DELETE_PENDING
        Run by the nightly cron job
        """
        
        log("AccountManager.delete_pending start...")
        
        for account in Account.objects.filter(status=ACCOUNT_STATUS_DELETE_PENDING):

            log(" - AccountManager.delete_pending: deleting account ", account.pk, account.base_url)
            
            account.status = ACCOUNT_STATUS_DELETE_IN_PROGRESS
            account.save()
            
            # Delete emails one at a time, because the queryset of all emails may be too big to fit in memory
            email_ids = Email.objects.filter(owner_account=account).values_list('id', flat=True)
                        
            for id in email_ids:
                try:
                    log("        deleting email: ", id)
                    Email.objects.only('id').get(id=id).delete()
                except:
                    pass        # email has already been deleted (child of a previously deleted email)
                
            log(" -  actually deleting account ", account.pk, account.base_url)
            account.delete()
            
        log("AccountManager.delete_pending done...")
            
    
    
#    def delete_old_inactivated(self):
#        '''Delete inactivated accounts that have been expired for more than 90 days.
#        The files belonging to these accounts will be auto-deleted by delete_old_files_from_cloud below.'''
#        log('delete_old_inactivated...')
#        for account in Account.objects.filter(status=ACCOUNT_STATUS_INACTIVE, date_expires__lt=now()-HOWLONG_DELETE_EXPIRED_ACCOUNTS).exclude(date_expires=None):
#            log('Deleting old expired account %s' % str(account))
#            account.delete()
            
        
    def delete_old_files_from_cloud(self, do_it=True):
        from lib.cloud import CloudFilesManager
        conn = CloudFilesManager().get_connection()
        cc = conn.list_containers()
        cc = [c for c in cc if c.startswith('live_')]
        for c in cc:
            slug = c.replace('live_', '')
            cont = conn.get_container(c)
            if Account.objects.active().filter(slug=slug).count() > 0:
                log('Deleting old files from ', c)
                objs = cont.list_objects_info()
                for o in objs:
                    dt = o['last_modified'].split('T')[0]
                    dt = pytz.UTC.localize(datetime.strptime(dt, '%Y-%m-%d'))
                    name = o['name']
                    if dt < now() - HOWLONG_MOSSO_FILES:
                        log (' -- deleting ', name, dt)
                        if do_it:
                            cont.delete_object(name)
                        ff = File.objects.filter(file_path__contains=name)
                        for f in ff:
                            log ('  -- removing from file path', name)
                            if do_it:
                                f.file_path = None
                                f.save()
                            
            else:
                log ('Deleting ', c, ' -- no active account')
                objs = cont.list_objects()
                for o in objs:
                    log (' -- deleting ', o)
                    if do_it:
                        cont.delete_object(o)
                        
                if do_it:
                    conn.delete_container(c)
        
        
class Account(models.Model):
    ''' An Account is the central controlling point for a company to manage
        multiple Aliases here.
    '''

    ACCOUNT_STATUS = (
        (ACCOUNT_STATUS_ACTIVE, 'Account active'),
        (ACCOUNT_STATUS_INACTIVE, 'Account inactive'),
        (ACCOUNT_STATUS_INACTIVE_MAX_USERS_EXCEEDED, 'Account inactive because max # users exceeded'),
        (ACCOUNT_STATUS_DELETE_PENDING, 'Account will be deleted next evening (daily cron job)'),
        (ACCOUNT_STATUS_DELETE_IN_PROGRESS, 'Account is actually being deleted')
    )
    
    ACCOUNT_EMAIL_STORAGE_CHOICES = (
        (0,  _('Never (paranoid mode)')),
        (7,  _('7 days')),
        (14, _('14 days')),
        (21, _('21 days')),
        (30, _('30 days')),
        (60, _('60 days')),
        (90, _('90 days')),
        (180, _('1/2 year')),
        (365, _('1 year')),
        (730, _('2 years')),
        (-1, _('Forever'))
    )
    
    status = models.CharField(max_length=4, null=True, blank=True, default=ACCOUNT_STATUS_INACTIVE, choices=ACCOUNT_STATUS, help_text='You can delete the account here via a nightly "delete account" job.')
    name = TruncCharField(max_length=255, null=True, blank=True, help_text='The name of the account')      # Name of the account (usually company name)
    service = models.ForeignKey(Service, null=True, blank=True, related_name='account_service', help_text='Which service level is the account on?') # The service level for an account.
    promo = models.ForeignKey(Promo, null=True, blank=True, related_name='account_promo') # Promo they signed up with, if any

    base_url = TruncCharField(max_length=255, null=True, help_text='This is the base url for the account.  Example: https://base_url.calenvy.com/ use "base_url"')
    
    date_created = UTCDateTimeField(default=now) #added new creation field.
    date_expires = UTCDateTimeField(blank=True, null=True, help_text='when does the account expire (normally)?') # 
    trial_period = models.NullBooleanField(null=True, default=False)            # Are we still in the free trial period for this account?
    extra_days = models.IntegerField(default=0, help_text='For certain customers, give them this many extra days after official expiration') # 

    
    date_modified = UTCDateTimeField(blank=True, null=True, help_text='When did we change the expiration date, extra trial days, etc? <b>Update this for most admin changes!</b>') # 

   
    max_users = models.IntegerField(null=True, blank=True, help_text="Max # of users this account has paid for (this may be greater than the service's max_users_flat_rate field, if they've paid for extra users)")

    archive_mode = models.BooleanField(default=False, help_text="Archive mode - disables deleting old emails and causes Salesforce to sync emails all the way back to an Alias's crawl back date. <br/><br/>If someone pays us for archiving, be sure to enable this!  Archiving is $250/year for each Alias.")
    store_email_howlong = models.IntegerField(default=30, choices=ACCOUNT_EMAIL_STORAGE_CHOICES, null=True, help_text='Retention setting - how long do we hold on to emails') # how long we store emails/files, None means forever

    transparent = models.NullBooleanField(default=False, null=True, help_text='Are contacts and conversations completely transparent on the team?')             # Is everything open within the team? (contacts, emails)
    transparent_sync = models.BooleanField(default=False, help_text='When one Alias does a sync on Salesforce, this field syncs emails for the entire team!  One Alias syncs all emails from all Aliases.')                       # Do salesforce syncs get messages for everyone in the team?
    
    
    logo_url = models.CharField(max_length=255, null=True, blank=True, help_text='Paste the full URL to a logo (136 x 60)')
    logo_weight = models.FloatField(default=1, help_text='How high on the company success page should the logo show?  3 means very high.  1 means average.')
    
    # Email sending settings
    assistant_emails_anytime = models.NullBooleanField(null=True, default=True, help_text='Completely disable the assistant emails here (reminder emails, calenvy news, etc)')
    assistant_emails_others = models.NullBooleanField(null=True, default=True, help_text='Can the assistant email anyone outside of your team?  Should non-Alias people such as bill.gates@microsoft.com receive emails?')    
    allow_mass_formmail_alias_sender = models.BooleanField(default=False, help_text='Can this account send formmails using SMTP and the Alias.email address with >= 10 contacts? (otherwise, we use the Account.assistant.email as the sender).')       # 

    block_confidential_mails = models.BooleanField(default=True, help_text='Search for confidential in the subject and exclude.')
    
    twitter_username = TruncCharField(max_length=255, null=True, blank=True) # the super cool company twitter account.
    site = models.ForeignKey(Site, null=True, blank=True, related_name='account_site') # Noted.cc, Clerk.cc, etc.

    date_inactivate_max_users_exceeded = UTCDateTimeField(blank=True, null=True) # when do we inactivate the account because they haven't paid for this many users?
    remote_gateway = models.CharField(max_length=16, null=True, blank=True)     # the payment gateway (on the payment server) where the subscription is held
    remote_customer_profile_json = models.TextField(null=True, blank=True)      # the profile ID for this account's payment info on the payment gateway
    
    # At most, only ONE of the following fields should be non-null at any given time (you can't have two subscriptions):
    remote_subscription_id = models.IntegerField(null=True,blank=True)              # id of this account's active subscription on the billing server 
    paypal_invoice = models.CharField(max_length=32, null=True, blank=True)         # id of this account's active subscription on Paypal... actually, their IPN invoice number 
        
    # Usage
    anon_storage_used_mb_msgs = models.FloatField(default=0, null=True, blank=True)
    anon_storage_used_mb_files = models.FloatField(default=0, null=True, blank=True)

    #delete_all_pending = models.NullBooleanField(default=False)     # If admin selects "Delete All" in the team settings page
    


    public_domains = models.TextField(default=None, null=True, blank=True)
    
    extra_json = models.TextField(null=True, blank=True)                        # Any other misc. stuff we need to store about this account
                                                                              # For now, only one key: 'sync_exclude_domains'
    slug = models.SlugField(null=True, unique=True)

    objects = AccountManager()
    
    def is_subscribed(self):
        "Return True if there's an active paying subscription"
        return bool(self.remote_subscription_id or self.paypal_invoice)
        
    def get_extra(self):
        return json.loads(self.extra_json or '{}')
    
    def get_default_admin(self):
        """Return the earliest-created admin for this account.
        Don't use active() here -- it needs to work even if the account itself is inactive!"""
        try:
            return Alias.objects.filter(status=ALIAS_STATUS_ACTIVE, is_admin=True, account=self).order_by('date_created')[0]
        except:
            return None
    
    def get_full_domain(self):
        """Return a domain like mycompany.noted.cc"""
        domain = self.site.domain.split(':')[0]
        if self.name:
            return '.'.join([self.name, domain])
        else:
            return domain
    
    def get_email_domain(self):
        """Return a domain like mycompany.noted.cc"""
        domain = self.site.domain.split(':')[0]
        if self.name:
            return '.'.join([self.name, domain])  
        else:
            return domain
    
    def find_alias_by_tag(self, tag):
        aliases = Alias.objects.active().filter(account=self)
        for a in aliases:
            if a.matches_tag(tag, ez_name_only=True):   # TODO: Make this a query for more efficiency
                return a
        for a in aliases:
            if a.matches_tag(tag, ez_name_only=False):
                return a
        return None    
    
    def set_initial_expiration_date(self, service, promo=None):
        """
        Call this only on new accounts, or accounts for which we just received the first Paypal subscription (not modify) notice
        """
        
        if promo and promo.service and (promo.service != service):  # Don't count the promo if it doesn't match this service
            promo = None
            
        # Only do this for new accounts!
        if service.is_free(promo):
            self.trial_period = False
            self.date_expires = None
        else:
            if promo and promo.trial_dmy:
                trial_num_dmy, trial_dmy = promo.trial_num_dmy, promo.trial_dmy
            else:
                trial_num_dmy, trial_dmy = service.trial_num_dmy, service.trial_dmy
            
            self.trial_period = bool(trial_num_dmy and not (promo and promo.disable_service_trial))
            if self.trial_period:
                self.date_expires = now() + paypal_timedelta('%d %s' % (trial_num_dmy, trial_dmy))
            else:
                self.date_expires = now() + timedelta(days=2)       # a grace period while we wait for first payment
        self.save()
                    
    def get_subscription_info(self):
        """Return a structure describing the currently active subscription for this account (if any),
        or the most recently cancelled subscription (if any))"""
        
        payments = list(Payment.objects.filter(account=self).order_by('date'))
        
        if not payments:
            return None             # No subscriptions, ever
        
        last_payment = payments[-1]
        
        # Get the payments belonging to the most recent subscription
        
        if last_payment.type == Payment.TYPE_GATEWAY:
            payments = [p for p in payments if p.remote_subscription_id == last_payment.remote_subscription_id]
            active = (self.remote_subscription_id == p.remote_subscription_id)
            
        elif last_payment.type == Payment.TYPE_PAYPAL:
            payments = [p for p in payments if p.paypal and last_payment.paypal and (p.paypal.invoice == last_payment.paypal.invoice)]
            active = p.paypal and (self.paypal_invoice == p.paypal.invoice)
            
        else:
            raise NotImplementedError
        
        return {
            'type':             last_payment.type,
            'payments':         payments,
            'first_payment':    payments[0],
            'last_payment':     payments[-1],
            'active':           active
        }
                         
    def start_service(self, service, promo=None, modify=False, max_users=None):
        '''Start off this account with the given service.
        (May be new account, or modifying an existing account.)
        '''
                    
        log("start_service: account=", self, "service=", service, "promo=", promo, "modify=", modify, "max_users=", max_users)
                     
        old_service = self.service
        
        if not modify:
            self.set_initial_expiration_date(service, promo=promo)
            
        self.service = service
        if max_users is not None:
            self.max_users = max_users
        elif service.max_users_flat_rate is not None:
            self.max_users = max(service.max_users_flat_rate, self.num_active_aliases())
        else:
            self.max_users = None
        self.promo = promo
        self.status = ACCOUNT_STATUS_ACTIVE
        log("Setting store_email_howlong...")
        
        if not self.archive_mode:
            # If we're archiving, don't auto-set the retention settings ever
            self.store_email_howlong = service.max_store_email_howlong
        
        self.save()
     
        # If upgrading to a service level that can crawl further back, start a recrawl right now
        if old_service and (service.crawl_email_howlong > old_service.crawl_email_howlong):
            from crawl.crawler import crawl_account
            crawl_account(self, recrawl_from=now()-service.crawl_email_howlong*ONE_DAY, silent=True)
        
    def has_multi_users(self):
        # Is there more than one user on this network? 
        return Alias.objects.active().filter(account=self).count() > 1
    
    def is_account_email(self, email):
        # Is there an alias with this email address in this network?
        return (Alias.objects.active().filter(account=self, email=email).count() > 0)

    def calc_storage_used_mb(self):
        used_mb = self.anon_storage_used_mb_msgs + self.anon_storage_used_mb_files
        for a in Alias.objects.active().filter(account=self):
            used_mb += a.storage_used_mb_msgs + a.storage_used_mb_files
        return used_mb, (1024*self.service.max_storage_gb) if self.service else None
        
    def allow_formmail(self):
        """Only allow the sending of formmail if the service level allows it and we've received
        something from Paypal."""
        if self.service:
            if 'noted' in self.service.name.lower():        # account used by Noted
                return True
            if self.base_url == settings.SALES_ACCOUNT:     # ditto
                return True                             
            if self.service.allow_formmail and \
                (self.service.is_free(promo=self.promo) or (Payment.objects.filter(account=self).count() > 0)):
                return True
            return False
        return False
         
    def get_assistant(self):
        try:
            return Assistant.objects.get(owner_account=self)
        except:
            return None
        
    def num_active_aliases(self):
        """
        Return the number of active aliases within this account.
        
        Do NOT use Alias.objects.active() here! We don't care whether the account itself
        is active, only whether the aliases are. The payment code needs to be able to 
        use this on inactive accounts, for payment activation.
        """
        
        return Alias.objects.filter(account=self, status=ALIAS_STATUS_ACTIVE).count()
    
    def merge_from(self, other_account):
        """
        Merge another account into this one.
        Leaves the admin status of the aliases as is!
        
        Doesn't handle merging contacts by secondary emails (usually not a problem).
        """
                
        my_alias_emails = [a.email for a in self.alias.all()]
        other_alias_emails = [a.email for a in other_account.alias.all()]
        for e in other_alias_emails:
            if e in my_alias_emails:
                log("merge_from: can't merge accounts because they have alias %s in common" % e)
                
        if other_account == self:
            raise Exception()
                
        sid = transaction.savepoint()
        
        try:
            
            log("merge_from: merging aliases...")
            for a in other_account.alias.all():
                a.account = self
                a.save()
                
            log("merge_from: merging emails [2]...")
            for e in other_account.owner_account_emails.all():
                e.owner_account = self
                e.save()
                
            log("merge_from: merging events...")
            for ev in other_account.account_owner.all():        # events
                ev.owner_account = self
                ev.save()
            
            log("merge_from: merging email rules...")
            for er in other_account.emailrule_set.all():
                er.owner_account = self
                er.save()
                
            log("merge_from: merging contacts...")
            for c in other_account.contact_set.all():
                if c.email:
                    try:
                        self_contact = Contact.objects.get(owner_account=self, email=c.email)
                        log("merge_from: --- merging contact %s" % c.email)
                        if c.alias:
                            c.owner_account = self
                            c.merge_from(self_contact)
                            c.save()
                        else:
                            self_contact.merge_from(c)
                            c.save()
                    except:
                        c.owner_account = self
                        c.save()
                else:
                    c.owner_account = self
                    c.save()
                    
            log("merge_from: merging buckets...")
            for b in other_account.bucket_set.all():
                # If the bucket has the same name as one of our buckets, merge them
                try:
                    self_bucket = Bucket.objects.get(account=self, name=b.name)
                    log("merge_from: --- merging bucket %s" % b.name)
                    self_bucket.merge_from(b)
                except:
                    # New bucket, just move it to this account        
                    b.account = self               
                    b.save()
            
            log("merge_from: merging event responses...")
            for er in other_account.get_assistant().eventresponse_set.all():
                er.account = self.get_assistant()
                er.save()
                
            log("merge_from: deleting old assistant...")
            other_account.get_assistant().delete()
            
            log("merge_from: deleting old account...")
            other_account.delete()
            
            transaction.savepoint_commit(sid)
            log("merge_from: done!")

        except:
            log(traceback.format_exc())
            transaction.savepoint_rollback(sid)

    def get_cloud_files_container_name(self):
        "The container name used for all files and message_dicts stored on Cloud Files"
        return settings.PRODUCT_NAME+'_'+settings.SERVER+'_'+self.base_url

    def cancel_subscription(self):
        "Cancel the subscription on the payment gateway"
        
        if self.remote_subscription_id:
            log(" cancel_subscription: Cancelling subscription %d on the payment server" % self.remote_subscription_id)
            from payment_client import get_api, SUBSCRIPTION_STATUS_CANCELLED
            get_api().set_subscription_status(subscription_id=self.remote_subscription_id, status=SUBSCRIPTION_STATUS_CANCELLED, gateway=settings.PAYMENT_GATEWAY)
            self.remote_subscription_id = None
            self.save()
            
    def delete(self, *args, **kwargs):
        "Override the usual delete method so that the subscription gets cancelled, if any"
        self.cancel_subscription()
        return super(Account, self).delete(*args, **kwargs) 

    
    def __unicode__(self):
        return u'%s' % (self.name or self.base_url)


class AccountUserManager(Manager):
        
    def get_account_user_by_email(self, email):
        ''' TODO: check for forgery somewhere '''
        email = email.lower()
        alias = Alias.objects.get_preferred_alias_by_email(email=email)
        if alias:
            return alias.account_user
        else:
            return None

class AccountUser(Model):
    ''' AccountUser describes the user.
        '''
    
    EMAIL_FREQUENCY_TYPES = (
        (EMAIL_FREQUENCY_NONE, 'No digests or reminders'),
        (EMAIL_FREQUENCY_ONLY_DIGESTS, 'Only digests'),
        (EMAIL_FREQUENCY_DIGEST_AND_NOTIFICATIONS, 'Digests and reminders'),
        (EMAIL_FREQUENCY_ONLY_NOTIFICATIONS, 'Only reminders'),
    )

    slug = models.SlugField(null=True, unique=True)
    name = TruncCharField(max_length=255,blank=True,null=True)
    email_frequency = models.CharField(max_length=3, default=EMAIL_FREQUENCY_DIGEST_AND_NOTIFICATIONS, choices=EMAIL_FREQUENCY_TYPES)
    calendar = models.ForeignKey(Calendar, null=True)           # TODO: Kill this
    timezone = models.CharField(max_length=64, default=settings.TIME_ZONE)

    objects = AccountUserManager()
    
    def __unicode__(self):
        return u'%s' % ','.join(uniqify([a.email for a in Alias.objects.filter(account_user=self)]))
    
    def get_aliases_for_display(self):
        try:
#            sorter = lambda a1, a2: cmp(a1.account.name, a2.account.name)
#            aliases = []
#            aliases += sorted(list(Alias.objects.filter(is_admin=True, status=ALIAS_STATUS_ACTIVE, account_user=self, account__status=ACCOUNT_STATUS_ACTIVE)), cmp=sorter)
#            aliases += sorted(list(Alias.objects.filter(is_admin=False, status=ALIAS_STATUS_ACTIVE, account_user=self, account__status=ACCOUNT_STATUS_ACTIVE)), cmp=sorter)
#            aliases += sorted(list(Alias.objects.filter(status=ALIAS_STATUS_ACTIVE, account_user=self, account__status=ACCOUNT_STATUS_INACTIVE)), cmp=sorter)
#            log("get_aliases_for_display: ", aliases)
            return Alias.objects.active(include_inactive_accounts=True).filter(account_user=self)
        
        except:
            return None
                    
    def get_timezone(self):
        return timezone_from_timezonestr(self.timezone)
    
    def local_now(self):
        return now().astimezone(self.get_timezone())


class AliasManager(Manager):
    def active(self, include_pending=False, include_inactive_accounts=False):
        q = Q(status=ALIAS_STATUS_ACTIVE)
        if include_pending:
            q = q | Q(status=ALIAS_STATUS_AWAITING_CONFIRMATION)
        query = self.filter(q)
        if include_inactive_accounts:
            # Don't include ACCOUNT_STATUS_DELETE_PENDING -- we don't show those at all
            query = query.filter(account__status__in=[ACCOUNT_STATUS_ACTIVE, ACCOUNT_STATUS_INACTIVE, ACCOUNT_STATUS_INACTIVE_MAX_USERS_EXCEEDED])
        else:
            query = query.filter(account__status=ACCOUNT_STATUS_ACTIVE)
        return query

    def get_aliases_by_email(self, email, include_pending=False, include_inactive_accounts=False):  
        ''' TODO: check for forgery somewhere '''
        # TODO: What about contact-only ones?
        email = email.lower().strip()
        # log("get_aliases_by_email: email=", email, include_contacts, include_pending)
        query = self.active(include_pending=include_pending, include_inactive_accounts=include_inactive_accounts)
        query = query.filter(email=email)
        return list(query)

    def get_preferred_alias_by_email(self, email, account=None, include_pending=False, require_account_match=False):
        aliases = self.get_aliases_by_email(email, include_pending=include_pending)
        if account:
            for a in aliases:
                if a.account == account:
                    return a
        if require_account_match:
            # None of the aliases match the account, so only return ones whose Account is None
            for a in aliases:
                if a.status == ALIAS_STATUS_AWAITING_CONFIRMATION and a.account is None:
                    return a
        else:
            for a in aliases:
                if (a.status == ALIAS_STATUS_ACTIVE) and a.account.name != 'demo':
                    return a
            for a in aliases:
                if (a.status == ALIAS_STATUS_ACTIVE):
                    return a            
            for a in aliases:
                if a.status == ALIAS_STATUS_AWAITING_CONFIRMATION:
                    return a
            if aliases:
                return aliases[0]
        return None
    
    def is_strong_password(self, password):
        password = password.strip()
        if len(password) < MIN_PASSWORD_LENGTH:
            return False
        if not (any([x in password for x in string.digits]) and \
                any([x in password for x in string.letters])):
            return False
        return True
        #return (password.strip().lower() not in BANNED_PASSWORDS)
        
class Alias(Model):

    ALIAS_STATUSES = (
        (ALIAS_STATUS_ACTIVE, 'Active alias'),
        (ALIAS_STATUS_INACTIVE, 'Inactive alias'),
        (ALIAS_STATUS_AWAITING_CONFIRMATION, 'Pending confirmation'),
    )
    
    MAILSERVER_TYPES = (
        (MAILSERVER_IMAP, 'IMAP4 Server'),
        (MAILSERVER_OWA, 'Outlook Web Access Server'),
    )
    
    MAILSERVER_SERVICES = (
        (MAILSERVICE_GMAIL, 'Gmail'),
        (MAILSERVICE_GOOGLE_APPS, 'Google Apps'),
        (MAILSERVICE_OTHER_IMAP, 'Other IMAP'),
    )
    
    email = models.EmailField(unique=True, help_text='This is the login username')
    
    # For admin use - upon save, trigger some action if any of these fields is non-null
    change_password_to = models.CharField(max_length=32, null=True, blank=True, help_text='Change the Alias password')         # Plaintext password - if set, then set the password_hash from this upon save
    crawl_from = UTCDateTimeField(null=True, blank=True, help_text='Set this to start a crawl going back to this date<br/>Type YYYY-MM-DD for date, HH:MM:SS for time<br/><br/>Note: each Alias must pay ~$250/year to do histrorical archiving on their emails.')

    # other dates
    beta_terms_date = UTCDateTimeField(null=True, blank=True, help_text='What is the date he/she agreed to the terms?') #
    date_created = UTCDateTimeField(default=now)
    last_login_date = UTCDateTimeField(null=True, blank=True, default=DATETIME_LOW, help_text='When was the last successful login from the user? (not counting logins made with the master override password)') #
    
    session_key = models.CharField(_('session key'), max_length=40, null=True, blank=True, help_text='session key of the last web login this user made')
    last_unused_account_reminder_date = UTCDateTimeField(null=True, blank=True, default=DATETIME_LOW)
    slug = models.SlugField(null=True, unique=True)
    slug_ics = models.SlugField(null=True, unique=True)
    date_last_accessed_ics = UTCDateTimeField(null=True, blank=True)
    is_admin = models.NullBooleanField(null=True, help_text='Admins can manage and view anything in the Account (billing history, privacy settigns)') 

    ez_name = models.CharField(max_length=255, null=True, blank=True, help_text='The @name shown in the newsfeed')
    agent_created = TruncCharField(max_length=20, null=True, blank=True, default='', help_text='Which agent the Alias signed up with (Firefox, Outlook, Salesforce)')       # 
    status = models.CharField(max_length=4, choices=ALIAS_STATUSES, default=ALIAS_STATUS_ACTIVE)
    
    show_modal_server = models.NullBooleanField(null=True, blank=True, default=True, help_text='Show modal configuration window for server settings.')    
    show_modal_intro = models.NullBooleanField(null=True, blank=True, default=True, help_text='Show intro video/document modal at Alias login.') 
    show_notice_upgrade = models.BooleanField(default=True, help_text='Show the upgradable packages when Alias logs in.')
    
    # Email settings
    send_notifications = models.NullBooleanField(default=True, help_text='Send email notifications about scheduled follow-ups or other Calenvy emails.')
    send_confirmations = models.NullBooleanField(default=True, help_text='Send confirmation emails when invited to a meeting.')

    
    password_hash = models.CharField(max_length=64, null=True, blank=True)
    
    reset_password_slug_hash = models.CharField(max_length=128, null=True, blank=True)
    reset_password_slug_date = UTCDateTimeField(null=True, blank=True)
        
    # Limits enforcement -- eventually move this to a new model
    parse_last_date = UTCDateTimeField(null=True, blank=True)

    sf_rules_enabled = models.BooleanField(default=True, help_text="Do we use SalesForce EmailRules when processing this Alias's emails?")                           # 
    
    server_enabled = models.NullBooleanField(default=False, null=True, blank=True, help_text='Is the IMAP server enabled and configured?')
    server_valid = models.NullBooleanField(default=False, null=True, blank=True, help_text='IMAP credentials valid since last crawl?')
    server_login = TruncCharField(max_length=255, null=True, blank=True)
    server_password_aes = TruncCharField(max_length=255, null=True, blank=True)
    server_server = TruncCharField(max_length=255, null=True, blank=True)
    server_port = models.IntegerField(null=True, blank=True)
    server_use_ssl = models.NullBooleanField(null=True, blank=True)
    server_type = models.CharField(max_length=4, null=True, blank=True, choices=MAILSERVER_TYPES)
    server_service = models.CharField(max_length=4, default=MAILSERVICE_GMAIL, null=True, choices=MAILSERVER_SERVICES, help_text='None means arbitrary IMAP server.') # None means an arbitrary IMAP server
    server_last_crawled = UTCDateTimeField(null=True, blank=True, help_text='Date last crawled on the server.')
    server_num_crawl_failures = models.IntegerField(default=0, null=True, blank=True, help_text='How many crawl failures have happened on this account?')
    
    server_smtp_enabled = models.NullBooleanField(default=False, null=True, blank=True)
    server_smtp_valid = models.NullBooleanField(default=False, null=True, blank=True)
    server_smtp_server = TruncCharField(max_length=255, null=True, blank=True)
    server_smtp_login = TruncCharField(max_length=255, null=True, blank=True)
    server_smtp_password_aes = TruncCharField(max_length=255, null=True, blank=True)
    server_smtp_port = models.IntegerField(null=True, blank=True)
    server_smtp_use_ssl = models.NullBooleanField(null=True, blank=True)
        
    # Usage
    storage_used_mb_msgs = models.FloatField(default=0, null=True, blank=True)
    storage_used_mb_files = models.FloatField(default=0, null=True, blank=True)
    
    # Enabling/disabling of specific dialog boxes/displays
    # TODO: Collapse all these into a nifty JSON structure
    
    user = models.ForeignKey(User, null=True, blank=True)
    account_user = models.ForeignKey(AccountUser, null=True, help_text='None if this Alias is just a contact with no login privs.')
    account = models.ForeignKey(Account, blank=True, null=True, related_name='alias')  # null if Alias is just a contact
    contact = models.ForeignKey('Contact', blank=True, null=True, related_name='alias_contact')   # the Contact corresponding to this alias

    # twitter
    
    twitter_enabled = models.NullBooleanField(null=True)
    twitter_username = models.CharField(max_length=255, null=True, blank=True)
    
    
    objects = AliasManager()

    def save(self, *args, **kwargs):
        """Override the default save() behavior to set the Alias's password from change_password_to
        (for admin use)"""
        
        if self.change_password_to:
            self.set_password(self.change_password_to, save=False)
        self.change_password_to = None
        
        if self.crawl_from:
            from crawl.crawler import start_crawl_alias
            start_crawl_alias(self, recrawl_from=self.crawl_from, join_existing=False)
            self.account.archive_mode = True
            self.account.save()
        self.crawl_from = None
        
        return super(Alias, self).save(*args, **kwargs) 
        
    def is_active(self):
        if (self.status == ALIAS_STATUS_ACTIVE) and \
            (self.account.status == ACCOUNT_STATUS_ACTIVE):
            return True
        return False
    
    def get_contact(self):
        return Contact.objects.get(owner_account=self.account, alias=self)
        
    def inactivate(self):
        log("Alias.inactivate (this also disables server_smtp_enabled): ", self, self.pk)
        self.status = ALIAS_STATUS_INACTIVE
        self.account = None
        self.account_user = None
        self.is_admin = False
        self.beta_terms_date = None
        self.last_login_date = None
        self.password_hash = None
        self.ez_name = None
        self.server_enabled = False
        self.server_valid = False
        self.server_login = None
        self.server_password_aes = None
        self.server_server = None
        self.server_port = None
        self.server_use_ssl = False
        self.server_type = None
        self.server_service = MAILSERVICE_OTHER_IMAP
        self.server_last_crawled = None
        self.server_num_crawl_failures = 0
        self.server_smtp_enabled = False
        self.server_smtp_valid = False
        self.server_smtp_server = None
        self.server_smtp_login = None
        self.server_smtp_password_aes = None
        self.server_smtp_port = None
        self.server_smtp_use_ssl = None
        self.twitter_enabled = False
        self.twitter_username = None
        self.storage_used_mb_msgs = 0
        self.storage_used_mb_files = 0
        self.show_modal_server = True
        self.show_modal_intro = True
        self.show_notice_upgrade = True
        self.send_notifications = True
        self.send_confirmations = True
        self.send_forwarded = True
        self.save()
    
    def first_name(self):
        return self.contact.first_name
    
    def last_name(self):
        return self.contact.last_name

    def get_full_name(self):
        result = ' '.join([self.contact.first_name, self.contact.last_name]).strip()
        return result

    def get_first_name(self):
        return self.contact.first_name

    def get_session(self):
        # Get the (active) Session object, or None if none
        if self.session_key:
            try:
                s = Session.objects.get(expire_date__gt=datetime.now(), session_key=self.session_key)
                return SessionStore(session_key=self.session_key)
            except:
                return None
        else:
            return None
        
    def billing_allowed(self):
        """Return True if this alias is allowed to visit the /info/billing page
        This will be true when either:
            - the alias is an admin, or
            - the account has not been paid for
            - the account is expired and anyone can pay!
        """
        
        if self.is_admin:
            return True
        if Payment.objects.filter(account=self.account).count() == 0:
            return True
        if self.account.status != ACCOUNT_STATUS_ACTIVE:
            return True
        return False
    
    def notify_all_widgets(self, wmessage_list=None):
        # Get the (active) Session object, or None if none
        log("notify_all_widgets start")
        from network.ajax_feeds import WMUpdateNewsfeeds, WMUpdateContacts
        if wmessage_list is None:
            wmessage_list = [WMUpdateNewsfeeds(), WMUpdateContacts()]
        session = self.get_session()
        try:
            if session and 'widget_managers' in session:
                for wm in session['widget_managers'].values():
                    wm.notify_all(wmessage_list)
                    log("notify_all_widgets: success ", self)
        except:
            log("notify_all_widgets: failure ", self)
        
    def get_status_as_string(self):
        for status, str in self.ALIAS_STATUSES:
            if self.status == status:
                return str
        return _("Unknown status")
        
    def get_full_name_or_email(self, include_email=False):
        result = self.get_full_name()
        if result:
            if include_email:
                result += " (%s)" % self.email
        else:
            result = self.email
        return result
    
    def get_first_name_or_email(self):
        return self.get_first_name() or self.email
    
    def greeting(self):
        name = self.contact.first_name
        if name:
            return random.choice([_('Hi %s')]) % name
        else:
            return _('Hi')
    
    def set_password(self, password, save=True):
        if password == '':
            raise Exception(_('Must provide a password.'))
        saltchars = string.ascii_letters + string.digits + './'
        salt = ''.join(random.choice(saltchars) for i in range(4))
        hash = hashlib.md5(password + salt).hexdigest()
        self.password_hash = '%s$%s' % (salt, hash)
        self.user.set_password(password)
        self.user.save()
        if save:
            self.save()
        
    def validate_password_return_alias(self, password):
        try:            
            salt, hash = self.password_hash.split('$')
            if (hashlib.md5(password + salt).hexdigest() == hash):
                self.user.set_password(password)
                self.user.save()
                return self 
            return False
        except:
            return False    
    
    def validate_password(self, password):
        if password and self.password_hash:
            try:            
                salt, hash = self.password_hash.split('$')
                if (hashlib.md5(password + salt).hexdigest() == hash):
                    self.user.set_password(password)
                    self.user.save()
                    return True      
                return False
            except:
                return False
        else:
            return False
        
    def validate_token(self, token, types):
        "Validate a token for the new API"
        if not isinstance(types, list):
            types = [types]
        try:
            from remote.models import RemoteCredential
            rc = RemoteCredential.objects\
                .filter(Q(date_expires=None) | Q(date_expires__gte=now())) \
                .get(type__in=types,
                token=token,
                alias=self)
            rc.date_last_accessed = now()
            rc.save()
            return rc
        except:
            return None
        
    def validate_master_password(self, password):
        try:
            if '/' in password:
                password, base_url = password.split('/')
            else:
                password, base_url = password, None
            if settings.DEBUG_MASTER_PW_ENABLED:
                if (hashlib.md5(password).hexdigest() == settings.DEBUG_MASTER_PW_HASH):
                    if (base_url is None) or (base_url == self.account.base_url):
                        return True
            return False
        except:
            return False
        
    def email_domain(self):
        email = self.email
        return email.split('@')[1]
    
    def get_site(self):
        if self.account:
            if self.account.site:
                return self.account.site
            else:
                return Site.objects.get_current()
        else:
            return Site.objects.get_current()
    
    def get_tag_for_display(self):
        '''Return a "canonical" tag name which other people within the same account
        can usually use to reach or tag this person.'''
        if self.ez_name:
            return '@' + self.ez_name
        
        return None
        
    def get_tag_or_name_for_display(self, viewer, include_email=False, indicate_self=False):
        '''How to display this person, on a webpage being read by a user of the given account'''
        result, account = None, None
        
        viewer_account = None
        if type(viewer) is Contact:
            viewer_alias = viewer.alias
            viewer_account = viewer.alias.account if viewer.alias else None
        if type(viewer) is Alias:
            viewer_alias = viewer
            viewer_account = viewer.account
        elif type(viewer) is Account:
            viewer_alias = None
            viewer_account = viewer
        if viewer_account and (self.account == viewer_account):
            if indicate_self and (self == viewer_alias):
                result = _('%s [you]') % self.get_tag_for_display()
            else:
                result = self.get_tag_for_display()
        if not result:
            result = self.get_full_name_or_email(include_email=include_email)
        return result

        
    def calc_ez_name(self, other_aliases):
        other_ez = [a.ez_name for a in other_aliases if (a.ez_name and a != self)]
        
        email_parts = self.email.split('@')[0].split('.')
        
        if self.contact.first_name:
            first_name = self.contact.first_name
        elif email_parts:
            first_name = email_parts[0]
        else:
            raise Exception("calc_ez_name: no first name or email address")
        
        if self.contact.last_name:
            last_name = self.contact.last_name
        elif email_parts and len(email_parts) > 1:
            last_name = email_parts[1]
        else:
            last_name = ''
        
        first_name = first_name.strip().lower()
        if last_name:
            last_name = last_name.strip().lower()
            
        first_name = ''.join([ch for ch in first_name if ch in EZ_NAME_ALLOWED_CHARS])
        last_name = ''.join([ch for ch in last_name if ch in EZ_NAME_ALLOWED_CHARS])
        
        ez = first_name.lower()
        if ez in other_ez:
            if last_name:
                ez += last_name[0]
        base_ez, i = ez, 1
        while ez in other_ez:
            ez = base_ez + str(i)
            i += 1
        
        return ez
        
    def matches_tag(self, tag, ez_name_only=True):
        '''Determines whether the given tag matches this person / Alias
        Tag can be of type Tag, or just a string
        ez_name_only: Flag indicating whether to match only on ez_names'''
        if type(tag) is Tag:
            tag = tag.text
        if not tag:
            return False
        if tag[0] == '@':
            tag = tag[1:]
        tag = tag.lower()
        
        # First, try to match the ez_name
        if self.ez_name and tag == self.ez_name:
            return True
        if ez_name_only:            # Don't match anything besides ez_names, if this flag is set
            return False
        
        # Then, try to match firstname, firstname.lastname, email, emailfirst.emaillast
        if self.contact.first_name and tag == self.contact.first_name.lower():
            return True
        if self.contact.first_name and self.contact.last_name and tag == (self.contact.first_name + '.' + self.contact.last_name).lower():
            return True
        addr = self.email.split('@')[0]
        if tag == addr.lower():
            return True
        if tag == addr.split('.')[0]:
            return True
        return False
    
    def get_mail_settings(self):
        if not self.server_enabled:
            raise Exception('Alias.server_login: server not enabled')
        
        if self.server_password_aes is not None:
            password = CRYPTER.Decrypt(self.server_password_aes)
        else:
            raise Exception("get_mail_settings: missing password")

        mail_settings = {\
            'service':          self.server_service,
            'server':           self.server_server,
            'username':         self.server_login,
            'password':         password, 
            'use_ssl':          self.server_use_ssl,
            'port':             self.server_port,
            'server_type':      self.server_type,
            'tzinfo':           self.account_user.get_timezone()
        }
        
        return mail_settings
        
    def get_smtp_connection(self, fail_silently=False):
        from assist.mail import SMTPConnection        
        connection = SMTPConnection(\
            host=self.server_smtp_server,
            port=self.server_smtp_port,
            username=self.server_smtp_login,
            password=CRYPTER.Decrypt(self.server_smtp_password_aes),
            use_tls=self.server_smtp_use_ssl,
            fail_silently=fail_silently\
        )
        return connection
                
    def refresh(self):
        "TODO: Get rid of this when we learn how to use signals"
        new_alias = Alias.objects.get(pk=self.pk)   # ugh
        self.status = new_alias.status
        self.account = new_alias.account
        
    def sees_everything(self, bucket=None, search=None):
        "Whether or not this alias is an admin and should see other team members' private stuff in the newsfeed"
        if self.is_admin:
#            if settings.ADMINS_SEE_EVERYTHING:
#                return True
            if bucket:
                if settings.ADMINS_SEE_EVERYTHING:
                    return True
            if search and search.search_type == SEARCH_TYPE_CONTACT:
                if settings.ADMINS_SEE_EVERYTHING:
                    return True
        return False
    
    def get_default_twitteruser_tuple(self):
        """Return a tuple consisting of:
            * the Twitteruser we use when replying from this user
            * the user's personal Twitteruser"""
        
        from twitter.models import TwitterUser
        try:     
            account_tu = TwitterUser.objects.get(account=self.account)
        except:
            account_tu = False
                
        try:
            alias_tu = TwitterUser.objects.get(alias=self)
        except:
            alias_tu = None
    
        return account_tu or alias_tu, alias_tu
    
    def get_default_twitteruser(self):
        return self.get_default_twitteruser_tuple()[0]
    
    def refresh_allowed(self, request):
        """Determine if the user is allowed to click the 'refresh' button in the newsfeed"""
        
        if self.account.base_url == settings.SALES_ACCOUNT or self.account.base_url in settings.DEBUG_ACCOUNTS:
            return True
        last_crawled = request.session.get('last_crawled')
        if not last_crawled:
            return True
        howlong = HOWLONG_REFRESH_DISABLED
        if not howlong:
            return True
        return datetime_older_than(last_crawled, howlong)    
    
    def get_email_count(self):
        "For the admin view"
        return Email.objects.filter(owner_alias=self).count()
        
    def __unicode__(self):
        return u'%s (%s)' % (self.email, self.account.base_url)
    
    def __str__(self):
        return '%s (%s)' % (self.email, self.account.base_url)
        
    class Meta:
        unique_together = (('ez_name', 'account'), )
        verbose_name_plural = "aliases"


class Payment(Model):
    
    TYPE_GATEWAY = 'GATE'               # Payments using our new payment gateway
    TYPE_PAYPAL = 'PAYP'                # Legacy Paypal payments
    
    PAYMENT_OPTIONS = (
        (TYPE_GATEWAY, 'Calenvy Payments server'),
        (TYPE_PAYPAL, 'Paypal'),
    )
    
    STATUS_SUCCESS = 'sAPP'             # Successful payment (Paypal or credit card)
    STATUS_FAILED = 'sFAI'              # Failed payment
                                        
    STATUS = (
        (STATUS_SUCCESS, 'Success'),
        (STATUS_FAILED, 'Failure')
    )
    
    FAILURE_REASON_EXPIRED = 'rEXP'                         # Credit card expired
    FAILURE_REASON_INVALID = 'rINV'                         # Invalid credit card number
    FAILURE_REASON_OTHER = 'r---'                           # Other failure
    
    FAILURE_REASON = (
        (FAILURE_REASON_EXPIRED, 'Card expired'),
        (FAILURE_REASON_INVALID, 'Invalid credit card number'),
        (FAILURE_REASON_OTHER, 'Other credit card failure')
    )

    type = models.CharField(max_length=4, choices=PAYMENT_OPTIONS, default='PAYP', blank=True, null=True)
    status = models.CharField(max_length=4, choices=STATUS, default=STATUS_SUCCESS)
    failure_reason = models.CharField(max_length=4, null=True, blank=True)
    account = models.ForeignKey(Account, related_name='payment_account')
    alias = models.ForeignKey(Alias, null=True, blank=True, related_name='payment_alias') #who made the payment?    # TODO: Get rid of this
    paypal = models.ForeignKey(PayPalIPN, null=True, blank=True)                    # The PayPalIPN object (if TYPE_PAYPAL)
    amount = models.FloatField(null=True, blank=True)                               # Amount of the payment
    remote_subscription_id = models.IntegerField(null=True, blank=True)             # ID of the corresponding Subscription object on the payment server (if TYPE_GATEWAY)
    remote_billing_event_id = models.IntegerField(null=True, blank=True)            # ID of the corresponding Payment object on the payment server (if TYPE_GATEWAY)
    date = UTCDateTimeField(default=now)
    
    # A JSON structure describing the items being billed, as follows:
    #
    # {
    #    'fee': {                                # this is optional
    #        'label':           'paypal',
    #        'percent':         10,
    #    },
    #    'items': [
    #        {
    #            'label':       'service',
    #            'service':     'Assistant',
    #            'price':       14.95
    #        },
    #        {
    #            'label':       'extra_users',
    #            'quantity':    3,
    #            'price_each':  9.95,
    #            'price':       29.85
    #        },
    #        {
    #            'label':       'emails_synced',
    #            'quantity':    300,
    #            'price_each':  0.02,
    #            'price':       6.00,
    #        }
    
    receipt_json = models.TextField(null=True, blank=True)
    
    def receipt(self):
        if self.receipt_json:
            return json.loads(self.receipt_json)
    
    def items(self):
        r = self.receipt()
        return r and r['items']
    
    def get_item(self, label):
        "Iterate through the items and return the one with the given label"
        items = self.items()
        if items:
            for i in self.items():
                if i['label'] == label:
                    return i
              
    def emails_synced_item(self):
        return self.get_item('emails_synced')
    
    def service_item(self):
        return self.get_item('service')
    
    def extra_users_item(self):
        return self.get_item('extra_users')
    
    def __unicode__(self):
        return u'%s' % (self.type)


class ContactManager(Manager):
    
    def active(self, include_notresponded=False, include_inactive=False):
        if include_inactive:
            return self.all()
        elif include_notresponded:
            return self.filter(status__in=[Contact.STATUS_ACTIVE, Contact.STATUS_NEEDSACTIVATION])
        else:
            return self.filter(status=Contact.STATUS_ACTIVE)
        
    def inactive(self):
        return self.exclude(status=Contact.STATUS_ACTIVE)
    
    def viewable(self, alias, include_notresponded=False, include_inactive=False):
        return self.active(include_notresponded=include_notresponded, include_inactive=include_inactive).filter(owner_account=alias.account)
    
    def editable(self, alias, include_notresponded=False):
        return self.viewable(alias, include_notresponded=include_notresponded)
    
    def find_for_alias(self, other_alias, account):
        # Find a contact within account "account" with the same email address as the given alias
        return self.active().filter(owner_account=account).filter(Q(email=other_alias.email) | Q(extrainfo__data=other_alias.email))
        
    def _update_contact(self, c, **kwargs):
        stats = kwargs.get('stats', None)
        if stats: stats.start('_update_contact')
        if stats: stats.start('_update_contact_1')
        update = kwargs.get('update', True)                 # Whether to update at all
        update_alias = kwargs.get('update_alias', False)    # Whether to allow updates, if contact is a team member
        update_source_user = kwargs.get('update_source_user', False)   # Whether update is due to user action (Contacts widget)
        viewing_alias = kwargs.get('viewing_alias', None)
        status = kwargs.get('status', None)                 # None = don't change
        if (update and not c.alias) or update_alias:
            replace_extrainfo = kwargs.get('replace_extrainfo', False)
            source = kwargs.get('source', Contact.SOURCE_EMAIL)
            source_text = kwargs.get('source_text', '')
            email = trim_nonalnum(kwargs.get('email', '')).lower()
            first_name = trim_nonalnum(kwargs.get('first_name', ''))
            last_name = trim_nonalnum(kwargs.get('last_name', ''))
            phone = kwargs.get('phone', '').strip()
            mobile_phone = kwargs.get('mobile_phone', '')
            department = kwargs.get('department', '').strip()
            description = kwargs.get('description', '')
            company = trim_nonalnum(kwargs.get('company', ''))
            title = kwargs.get('title', '').strip()
            website = kwargs.get('website', '').strip()
            zip_code = trim_nonalnum(kwargs.get('zip_code', ''))
            twitter_screen_name = kwargs.get('twitter_screen_name', '').strip().lower().strip('@')
            categories = kwargs.get('categories', [])
            alias = kwargs.get('alias', None)
            modified = False
            email_modified = False
            if stats: stats.end('_update_contact_1')
            
            if stats: stats.start('_update_contact_2')

            if email:
                extrainfo_emails = list(c.extrainfo_set.filter(type=ExtraInfo.TYPE_EMAIL))
                
                if (replace_extrainfo or not c.email):
                    c.email = email; modified = True; email_modified = True
                    if c.alias:
                        c.alias.email = email
                        c.alias.save()
                    for ei in extrainfo_emails:
                        if ei.data == email:
                            ei.delete()
               
                elif c.email and (email != c.email):
                    if not any([ei.data == email for ei in extrainfo_emails]):
                        ExtraInfo.objects.create(owner_contact=c, type=ExtraInfo.TYPE_EMAIL, data=email)
                        email_modified = True       # not modified=True, since that doesn't change the actual Contact object
                
                if email_modified:
                    contacts_all = Contact.objects.active().filter(owner_account=c.owner_account)
                    others = [o for o in contacts_all.filter(Q(email=email) | Q(extrainfo__data=email)) if o != c]
                    if any([o.alias_id for o in others]):
                        return others[0]                # don't merge from team members
                    for o in others:
                        log("Found a dup - merging with contact %d" % o.pk)
                        c.merge_from(o)
        
            if stats: stats.end('_update_contact_2')                
            
            if stats: stats.start('_update_contact_3')
            
            if (update_source_user or phone) and (phone != c.phone):                        
                c.phone = phone; modified = True
                
#            if phone and (replace_extrainfo or not c.phone):
#                c.phone = phone
#                ExtraInfo.objects.filter(type=ExtraInfo.TYPE_PHONE, data=phone).delete() # kill any secondaries with this
#            elif phone and c.phone and (phone != c.phone):
#                if ExtraInfo.objects.filter(type=ExtraInfo.TYPE_PHONE, data=phone).count() == 0:
#                    ei = ExtraInfo.objects.create(type=ExtraInfo.TYPE_PHONE, data=phone)
#                    c.extra_info.add(ei) 
#           
     
            if (update_source_user or source_text) and (source_text != c.source_text):                        
                c.source_text = source_text; modified = True        
            if (update_source_user or company) and (company != c.company):                        
                c.company = company; modified = True
            if (update_source_user or title) and (title != c.title):                      
                c.title = title; modified = True
            if (update_source_user or website) and (website != c.website):
                c.website = website; modified = True
            if (update_source_user or zip_code) and (zip_code != c.zip_code):
                c.zip_code = zip_code; modified = True
            if update_source_user or (first_name and not c.first_name):         # Only user (Contacts widget) can overwrite an existing first name
                if c.first_name != first_name:
                    c.first_name = first_name; modified = True
            if update_source_user or (last_name and not c.last_name):           # Only user (Contacts widget) can overwrite an existing last name
                if c.last_name != last_name:
                    c.last_name = last_name; modified = True
            if (update_source_user or department) and (department != c.department):
                c.department = department; modified = True
            if (update_source_user or description) and (description != c.description):
                c.description = description; modified = True
            if source and (source != c.source):
                c.source = source; modified = True
            if (update_source_user or twitter_screen_name) and (twitter_screen_name != c.twitter_screen_name):
                c.twitter_screen_name = twitter_screen_name; modified = True
            if alias and (alias != c.alias):
                c.alias = alias; c.owner_alias = alias; modified = True         # Contacts should always be owned by their own aliases
            if status is not None and (status != c.status):
                # We only ever upgrade the status of a contact this way, not downgrade
                status_list = [Contact.STATUS_INACTIVE, Contact.STATUS_NEEDSACTIVATION, Contact.STATUS_ACTIVE]
                if status_list.index(status) > status_list.index(c.status):
                    c.status = status; modified = True
                
            if stats: stats.start('_update_contact_4')
            if modified:    
                c.save()
            if stats: stats.end('_update_contact_4')
            
            for cat_name in categories:
                bucket, created = Bucket.objects.get_or_create_for_alias(viewing_alias, cat_name)
                bucket.contacts.add(c)

            if stats: stats.end('_update_contact_3')
            
        if stats: stats.end('_update_contact')
                        
        return c
         
    def get_by_alias(self, a, viewing_alias):
        # Find a contact, viewable by the viewing_alias, that corresponds to alias a
        queryset = self.viewable(viewing_alias).filter(Q(email=a.email) | Q(extrainfo__data=a.email))
        if queryset.count() > 0:
            return queryset[0]
        return None
        
    def get_or_create_smart(self, **kwargs): 
        
        stats = kwargs.get('stats', None)    
        if stats: stats.start('gcs_intro')
   
        viewing_alias = kwargs.get('viewing_alias', None)
        account = kwargs.get('account', None)
        if viewing_alias and not account:
            account = viewing_alias.account 
        alias = kwargs.get('alias', None)
        contacts_all = Contact.objects.active(include_notresponded=True).filter(owner_account=account)
        kwargs['contacts_all'] = contacts_all
        contacts_all_inactive = Contact.objects.inactive().filter(owner_account=account)
        contacts_editable = Contact.objects.active(include_notresponded=True).filter(owner_account=account)
        update = kwargs.get('update', True)
        update_alias = kwargs.get('update_alias', False)
        contact_to_update = kwargs.get('contact_to_update', None)
        source = kwargs.get('source', Contact.SOURCE_EMAIL)
        source_text = kwargs.get('source_text', '')
        email = kwargs.get('email', '')
        first_name = trim_nonalnum(kwargs.get('first_name', ''))
        last_name = trim_nonalnum(kwargs.get('last_name', ''))
        name = trim_nonalnum(kwargs.get('name', ''))
        phone = kwargs.get('phone', '').strip()
        mobile_phone = kwargs.get('mobile_phone', '')
        department = kwargs.get('department', '').strip()
        description = kwargs.get('description', '')
        company = trim_nonalnum(kwargs.get('company', ''))
        title = kwargs.get('title', '').strip()
        website = kwargs.get('website', '').strip()
        zip_code = trim_nonalnum(kwargs.get('zip_code', ''))
        categories = kwargs.get('categories', [])
        twitter_screen_name = kwargs.get('twitter_screen_name', '').strip().lower().strip('@')
        guess_fields_from_email = kwargs.get('guess_fields_from_email', True)
        notify_user_exception = kwargs.get('notify_user_exception', False)
        status = kwargs.get('status', None)     # None = don't change
                
        email_name = None
        if email:
            email_name, email = parseaddr_unicode(email)
            email = email.lower()
            
        if email_name and not name:
            name = email_name
        
        if name and not first_name and not last_name:
            first_name, last_name = get_first_last_name(name, addr=email)
            kwargs['first_name'], kwargs['last_name'], kwargs['email'] = first_name, last_name, email

        if email:
            if contact_to_update and contact_to_update.pk:
                if stats: stats.end('gcs_intro')
                return self._update_contact(contact_to_update, **kwargs), False
                
        if email:
            if RE_EMAIL.match(email) and len(email) <= MAX_LENGTH_EMAIL:     # must be valid email address
                queryset = contacts_editable.filter(email=email)
                if queryset.count() > 0:
                    c = queryset[0]
                    return self._update_contact(c, **kwargs), False
                queryset = contacts_editable.filter(extrainfo__data=email)
                if queryset.count() > 0:
                    c = queryset[0]
                    return self._update_contact(c, **kwargs), False
                queryset = contacts_all.filter(Q(email=email) | Q(extrainfo__data=email))
                if queryset.count() > 0:
                    c = queryset[0]
                    return self._update_contact(c, **kwargs), False
                queryset = contacts_all_inactive.filter(Q(email=email) | Q(extrainfo__data=email))
                if queryset.count() > 0:
                    c = queryset[0]
                    # Reactivate an inactive contact if it's a deliberate user action(i.e., editing in the Contacts widget)
                    if source == Contact.SOURCE_USER:
                        c.status = Contact.STATUS_ACTIVE
                        c.owner_alias = viewing_alias
                        c.save()
                    if viewing_alias:
                        c.update_last_contacted_date(viewing_alias, type=ContactDate.TYPE_CREATED)

                    return self._update_contact(c, **kwargs), False       
            else:
                return None, False          
                
        if twitter_screen_name:            
            queryset = contacts_editable.filter(twitter_screen_name__iexact=twitter_screen_name)
            if queryset.count() > 0:
                c = queryset[0]
                return self._update_contact(c, **kwargs), False
            queryset = contacts_all.filter(twitter_screen_name__iexact=twitter_screen_name)
            if queryset.count() > 0:
                c = queryset[0]
                return self._update_contact(c, **kwargs), False
        
        if not email and not twitter_screen_name:
            if notify_user_exception:
                raise NotifyUserException(_('Contact must have an email address or a Twitter username.'))
            return None, False              # a contact must have at least one of these
             
        # Only when creating a new contact via email, guess the first name, last name and company fields.
        if guess_fields_from_email and (source == Contact.SOURCE_EMAIL):
            if email and not first_name and not last_name:
                first_name, last_name = get_first_last_name_from_email(email)
            if email and not company:
                company = get_company_from_email(email) or ''
                
        # For now, disable guessing secondary emails based on names [eg 8/26]
#        if first_name and last_name and len(first_name) >= 3 and len(last_name) >= 3:
#            queryset = contacts_editable.filter(first_name__iexact=first_name, last_name__iexact=last_name)
#            if queryset.count() > 0:
#                c = queryset[0]
#                return self._update_contact(c, **kwargs), False
#            queryset = contacts_viewable.filter(first_name__iexact=first_name, last_name__iexact=last_name)
#            if queryset.count() > 0:
#                c = queryset[0]
#                return self._update_contact(c, **kwargs), False
            
        
#        if source not in [Contact.SOURCE_TWITTER, Contact.SOURCE_ALIAS]:
#            first_name_stripped = strip_nonalnum(first_name)
#            last_name_stripped = strip_nonalnum(last_name)
#    
#            if ((first_name != first_name_stripped) and len(first_name_stripped) >= 2) or\
#                ((last_name != last_name_stripped) and len(last_name_stripped) >= 2):
#                for c in contacts_editable:
#                    if first_name_stripped == strip_nonalnum(c.first_name) and last_name_stripped == strip_nonalnum(c.last_name):
#                        return self._update_contact(c, **kwargs), False  
#                for c in contacts_viewable:
#                    if first_name_stripped == strip_nonalnum(c.first_name) and last_name_stripped == strip_nonalnum(c.last_name):
#                        return self._update_contact(c, **kwargs), False                  
#                
        c = Contact() 
        c.source = source
        c.source_text = source_text                  
        c.owner_account=account
        if alias:
            c.owner_alias = alias
        elif viewing_alias:
            c.owner_alias = viewing_alias
        else:
            c.owner_alias = Alias.objects.active().filter(account=account, is_admin=True)[0]
        c.unsubscribe_slug = create_slug(length=20) 
        c.status = status or Contact.STATUS_ACTIVE
        c.email = email
        c.alias=alias                           # for internal contacts, the Alias this contact corresponds to
        c.first_name = first_name
        c.last_name = last_name
        c.phone = phone
        c.mobile_phone = mobile_phone
        c.department = department
        c.description = description
        c.company = company
        c.title = title
        c.website = website[:200]
        c.zip_code = zip_code
        c.twitter_screen_name = twitter_screen_name
        n = now()
        c.last_modified_date = n
        c.created_date = n
        
        try:
            c.save()
            
            if viewing_alias:
                c.update_last_contacted_date(viewing_alias, type=ContactDate.TYPE_CREATED)
                
            for cat_name in categories:
                bucket, created = Bucket.objects.get_or_create_for_alias(viewing_alias, cat_name)
                bucket.contacts.add(c)
            
            return c, True
            
        except:
            log("get_or_create_smart [Contact] exception (usually has to do with an overly long string):") # ????
            log(traceback.format_exc())
            
            return None, False # return Contact, created -- in this case, none was created! so false.
            
class ContactDate(Model):
    
    TYPE_CREATED = 'tCRE'
    TYPE_CONVERSATION = 'tCON'
    
    # Store the last time a contact was contacted (or created) by a particular team member
    type = models.CharField(max_length=4, default=TYPE_CREATED)
    contact = models.ForeignKey('Contact')
    alias = models.ForeignKey('Alias')
    last_contacted_date = UTCDateTimeField(default=DATETIME_LOW)
    

class Contact(Model):
    
# Private actually means what "protected" does for files:
                                    #  you can see the names, but not expand them to get info
                                    #  (might change this later)
    SOURCE_USER         =   'sUSR'
    SOURCE_EMAIL        =   'sEML'
    SOURCE_GOOGLE       =   'sGOO'
    SOURCE_CSV          =   'sCSV'
    SOURCE_TWITTER      =   'sTWI'
    SOURCE_PLAXO        =   'sPLX'
    SOURCE_ALIAS        =   'sALI'  # actual users (alias != None) get this
    STATUS_ACTIVE       =   'sACT'
    STATUS_NEEDSACTIVATION =   'sNRS'  # contact who's sent us something, but we haven't replied yet
    STATUS_INACTIVE     =   'sINA'
    
    SOURCE = (
        (SOURCE_USER, _('Contact entered by user')),
        (SOURCE_EMAIL, _('Contact from email')),
    )
    
    STATUS = (
        (STATUS_ACTIVE, _('Contact is active')),
        (STATUS_NEEDSACTIVATION, _('Incoming contact only -- not responded to yet')),
        (STATUS_INACTIVE, _('Contact is inactive'))
    )
    
    owner_alias = models.ForeignKey(Alias, null=True, related_name='owner_contact_set', help_text=_('Which alias owns this contact?'))
    owner_account = models.ForeignKey(Account, null=True)
    alias = models.ForeignKey(Alias, null=True, blank=True, related_name='contact_set')             
    source = models.CharField(max_length=4, choices=SOURCE, blank=True, null=True)
    source_text = TruncCharField(max_length=64, null=True, blank=True)                  # Text description of the email's source -- used for contacts created by
                                                                                        #  regex, and sent to Salesforce as "Lead Source"
    status = models.CharField(max_length=4, choices=STATUS, blank=True, null=True)
    subscribed = models.NullBooleanField(default=True)  # whether we can send formmail to this contact
    created_date = UTCDateTimeField(null=True, blank=True)
    last_modified_date = UTCDateTimeField(null=True, blank=True)
    last_contacted_date = UTCDateTimeField(default=DATETIME_LOW)                        # last time *any* team member contacted this contact (for individual dates, see ContactDate table)
    first_contacted_to_date = UTCDateTimeField(null=True, blank=True)
    contacted_by_assistant = models.NullBooleanField(default=False, blank=True, null=True)  # whether this contact has ever received an email from the assistant
    unsubscribe_slug = models.SlugField(max_length=20, blank=True, null=True)
    
    sf_sync_pending = models.BooleanField(default=False)                                # whether this contact will be synced to SF on the next SF sync
    
    # common fields
    email = models.EmailField()
    first_name = TruncCharField(max_length=64, default='', null=True, blank=True)
    last_name = TruncCharField(max_length=64, default='', null=True, blank=True)
    title = TruncCharField(max_length=64, default='', null=True, blank=True)
    phone = TruncCharField(max_length=64, default='', null=True, blank=True)
    mobile_phone = TruncCharField(max_length=64, default='', null=True, blank=True)
    department = TruncCharField(max_length=64, default='', null=True, blank=True)
    company = TruncCharField(max_length=64, default='', null=True, blank=True)
    website = models.URLField(default='', max_length=255, null=True, blank=True)
    description = TruncCharField(max_length=255, default='', null=True, blank=True)
    zip_code = TruncCharField(max_length=20, default='', null=True, blank=True)
    twitter_screen_name = TruncCharField(max_length=32, default='', null=True, blank=True)
    social_json = models.TextField(null=True, blank=True)       # Data from the social stuff
                                                                                   
    extra_info = models.ManyToManyField(ExtraInfo, null=True, blank=True)
    
    # TODO: Get rid of these fields
#    actions = generic.GenericRelation('remote.RemoteUserAction', object_id_field='local_id', content_type_field='local_type')

#    remote_id = models.CharField(max_length=32, null=True, blank=True)
#    remote_owner_id = models.CharField(max_length=32, null=True, blank=True)
#    remote_account_id = models.CharField(max_length=32, null=True, blank=True)
    
    #class Meta:
    # unique_together = (("email", "owner_account"),)
        
    objects = ContactManager()
    
    def social_data(self):
        "Get the social data currently stored for this contact"
        if self.social_json is not None:
            return json.loads(self.social_json)
        else:
            return None     # None = default value, means we've never tried to fetch the
                            #  social data for this contact
    
    def update_social_data(self, default_image_size=None):
        """
        Fetch social data for this contact from the social networks, overwriting any previous data
        Returns the new social data (if any)
        """
        
        if self.owner_account.service.allow_social_lookups:
            from lib.social.multi import MultiFinder
    
            multi_finder = MultiFinder(default_image_size=default_image_size)
            social_data = multi_finder.multi_find({
                'email':            self.email,
                'name':             self.get_full_name(),
                'company':          self.company or get_company_from_email(self.email) or None,
                'website':          self.website.replace('http(s|)://','') if self.website else None
            })
                        
            title = multi_finder.get_basic_info(social_data, multi_finder.INFO_TITLE) or ''
            title = re.sub(r'(?i)\s(at|\@).*$', '', title).strip()
            
            if title and not self.title:                # Only overwrite title if it doesn't exist
                self.title = title
            if social_data:                             # Only overwrite social data if we have new social data
                self.social_json = json.dumps(social_data)
            self.save()
            
            log('c.update_social_data: social_data: ' + social_data)
            
            return social_data
        else:
            return None                                 # social lookups not allowed for this service level
        
#    def social(self, key):
#        "Return a field from the set of data return by the social person finder"
#        return json.loads(self.social_json).get(key)
#    
    def get_image_url(self, image_size=40):
        "Return a link to this person's avatar photo from the social data, if any"
        
        images = self.social('images')
        if images:
            return images[0]['url']
        else:
            return None
            
    def greeting(self):
        name = self.first_name
        if name:
            return random.choice([_('Hi %s')]) % name
        else:
            return _('Hi')
        
    def get_full_name(self):
        return (' '.join([self.first_name, self.last_name])).strip()
        
    def get_display_name(self, include_email=False, include_ez_name=False):
        "The name used for sorting the contacts, and in the Contacts widget"
        if self.last_name:
            result = "%s, %s" % (str_or_none(self.last_name), str_or_none(self.first_name))
            if include_email and self.email:
                result += " (%s)" % self.email
            elif include_email and self.twitter_screen_name:
                result += " (%s)" % self.twitter_screen_name
        elif self.first_name:
            result = str_or_none(self.first_name)
            if include_email and self.email:
                result += " (%s)" % self.email
            elif include_email and self.twitter_screen_name:
                result += " (%s)" % self.twitter_screen_name
        elif self.email:
            result = self.email
        elif self.twitter_screen_name:
            result = self.twitter_screen_name
        else:
            result = ''
        if include_ez_name and self.alias and self.alias.ez_name:
            result = ('@%s (%s)' % (self.alias.ez_name, result)).strip()
        return result
        
    def get_tag_or_name_for_display(self, viewer, include_email=False, indicate_self=False):
        # viewer can be Alias, Account or Contact
        if self.alias:
            return self.alias.get_tag_or_name_for_display(viewer, include_email=include_email, indicate_self=indicate_self)
        else:
            return self.get_display_name(include_email=include_email)
              
    def get_last_contacted_alias(self, account):
        queryset = ContactDate.objects.filter(contact=self, type=ContactDate.TYPE_CONVERSATION).order_by('-last_contacted_date')
        if queryset.count() > 0:
            return queryset[0].alias
        else:
            return None
        
    def update_last_contacted_date(self, alias=None, type=ContactDate.TYPE_CONVERSATION):
        n = now()
        if type == ContactDate.TYPE_CONVERSATION:
            self.last_contacted_date = n
            self.save()
        if alias:
            contact_date, created = ContactDate.objects.get_or_create(contact=self, alias=alias)
            contact_date.last_contacted_date = n
            contact_date.type = type
            contact_date.save()
        
    def set_active(self, viewing_alias):
        if self.owner_account != viewing_alias.account:
            raise Exception()
        old_owner_alias = self.owner_alias
        self.status = Contact.STATUS_ACTIVE
        self.owner_alias = viewing_alias
        self.save()
        self.update_last_contacted_date(alias=viewing_alias, type=ContactDate.TYPE_CREATED)
        
    def get_secondary_emails(self):
        queryset = self.extrainfo_set.filter(type=ExtraInfo.TYPE_EMAIL)
        return [str(ei.data) for ei in queryset]
    
    def as_json(self, alias=None, include_secondary_emails=False, viewable_contacts=None):
        
        social_data = self.social_data()
        from lib.social.multi import MultiFinder
        multi_finder = MultiFinder()
        from network.views_proxy import calc_proxy_url 
        
        result = {
            'id':           self.id,
            'slug':         self.unsubscribe_slug,
            'subscribed':   self.subscribed,
            'source':       self.source,
            'is_team':      bool(self.alias),
            'title':        self.title,
            'company':      self.company,
#            'email':        self.email,
#            'first_name':   self.first_name,
#            'last_name':    self.last_name,
            'display_name': self.get_display_name(),
            'display_name_ez_name': self.get_display_name(include_ez_name=True),
            'display_name_only': self.get_full_name(),
            'owner_name':   self.owner_alias.get_tag_for_display(),
            'owner_id':     self.owner_alias.pk,
            'email':        str(self.email),
            'email_md5':    hashlib.md5(self.email or '').hexdigest(),  # for Gravatar
            'twitter_screen_name':  str(self.twitter_screen_name),
            'mouseover_text':   self.get_mouseover_text(),
            'secondary_emails':    self.get_secondary_emails() if include_secondary_emails else [],      # ditto
            'social_data':  social_data,
            'image_url':    calc_proxy_url(multi_finder.get_basic_info(social_data, multi_finder.INFO_IMAGE)) or None
        }
                
        return result
    
    def as_json_more(self, alias=None):
        "Stuff that only viewing aliases can see goes here"
        result = self.as_json(include_secondary_emails=True, alias=alias)
        result.update({
            'source':       self.source,
            'email':        self.email,
            'first_name':   self.first_name,
            'last_name':    self.last_name, 
            'name':         self.get_full_name(),
            'id':           self.id, 
            'title':        self.title,
            'phone':        self.phone,
            'mobile_phone': self.mobile_phone,
            'department':   self.department,
            'company':      self.company,
            'website':      self.website,
            'zip_code':     self.zip_code,
            'description':  self.description
        })
        return result        
                                   
    def as_json_details(self, account=None, alias=None):
        "Stuff that's expensive to compute goes here"
        from twitter.models import TwitterUser
        result = self.as_json_more(alias=alias)
        try:
            if self.twitter_screen_name:
                tu = TwitterUser.objects.get(screen_name__iexact=self.twitter_screen_name)
                twitter_is_followed = tu.followers.filter(account=self.owner_account).count() > 0
                twitter_followers_count = tu.followers_count
            else:
                twitter_is_followed, twitter_followers_count = False, None
        except:
            twitter_is_followed, twitter_followers_count = False, None
        
        reminder_date = None
        if alias:
            queryset = Event.objects.filter(creator=alias, status=EVENT_STATUS_FOLLOWUP_REMINDER, original_email__contacts=self, original_email__contacts__email2contact__type=Email2Contact.TYPE_REF, start__gte=now()).order_by('start')
            if queryset.count() > 0:
                reminder_date = format_datetime(queryset[0].start, display_today=True, display_future_weekdays=True, display_dayonly_if_daystart=True, tzinfo=alias.account_user.get_timezone())
        
        from lib.social.multi import MultiFinder
        multi_finder = MultiFinder()
        social_data = result['social_data']
        
        result.update({  
            'last_contacted_date':  self.last_contacted_date if (self.last_contacted_date > (DATETIME_LOW + 1000*ONE_DAY)) else None,
            'last_contacted_alias': self.get_last_contacted_alias(account) if account else None,
            'owner_alias': self.owner_alias,
            #'secondary_emails': self.get_secondary_emails(),
            'twitter_is_followed': twitter_is_followed,
            'twitter_followers_count': twitter_followers_count,
            'reminder_date': reminder_date,
            
            # Basic info from the social data
            #'image_url': (This is already provided by as_json above)
            'age': multi_finder.get_basic_info(social_data, multi_finder.INFO_AGE) or '',
            'gender': multi_finder.get_basic_info(social_data, multi_finder.INFO_GENDER) or '',
            'location': multi_finder.get_basic_info(social_data, multi_finder.INFO_LOCATION) or '',
            'memberships': multi_finder.get_basic_info(social_data, multi_finder.INFO_MEMBERSHIPS) or [],
            'education': multi_finder.get_basic_info(social_data, multi_finder.INFO_EDUCATION) or [],
            'experience': multi_finder.get_basic_info(social_data, multi_finder.INFO_EXPERIENCE) or []
        })
        
        return result        
    
    
    def get_to_for_display(self):
        "Used for the Contacts Compose window. Assumes the contact has an email address!"        
        if self.first_name or self.last_name:
            result = ' '.join([self.first_name, self.last_name]).strip()
            if self.email:
                result += ' <%s>' % self.email
            return result
        elif self.email:
            return self.email
        elif self.twitter_screen_name:
            return self.twitter_screen_name
        elif self.company:
            return self.company
        elif self.title:
            return self.title
        elif self.website:
            return self.website
        elif self.phone:
            return self.phone
        else:
            return ''
    
    def get_mouseover_text(self):
        # This should only be visible if the user actually has permissions to view the contact details
        text_parts = [
            self.email or None, 
            'Twitter: %s' % self.twitter_screen_name if self.twitter_screen_name else None
        ]
        text = ', '.join(uniqify(text_parts))
        return text
        
    def merge_from(self, other_contact):
        
        # Don't merge away team members, ever
        if other_contact.alias:
            return
                
        # Other stuff whose Foreign Keys point to this contact ...
        for ec in other_contact.email2contact_set.all():
            ec.contact = self
            ec.save()
        
        for er in other_contact.eventresponse_set.all():
            er.contact = self
            er.save()

        for ruc in other_contact.remoteusercontact_set.all():
            ruc.contact = self
            ruc.save()

        for e in other_contact.email_from_contact.all():
            e.from_contact = self
            e.save()

        # Other stuff with Many to Many relationship to this contact ...
        for b in other_contact.contact_buckets.all():
            self.contact_buckets.add(b)
        other_contact.contact_buckets.clear()
   
        # For now, don't merge the actual data [eg 9/27]
        
#        if self.alias:
#            # If merging into a Team contact, be more limited about what we merge (don't overwrite existing stuff,
#            # and don't overwrite first/last name at all)
#            if not self.twitter_screen_name:
#                self.twitter_screen_name = other_contact.twitter_screen_name
#            if not self.title:
#                self.title = other_contact.title
#            if not self.website:
#                self.website = other_contact.website
#            if not self.company:
#                self.company = other_contact.company
#            if not self.phone:
#                self.phone = other_contact.phone
#            if not self.mobile_phone:
#                self.mobile_phone = other_contact.mobile_phone
#            if not self.department:
#                self.department = other_contact.department
#            if not self.zip_code:
#                self.zip_code = other_contact.zip_code  
#        else:
#            if other_contact.first_name:
#                self.first_name = other_contact.first_name
#            if other_contact.last_name:
#                self.last_name = other_contact.last_name
#            if other_contact.twitter_screen_name:
#                self.twitter_screen_name = other_contact.twitter_screen_name
#            if other_contact.title:
#                self.title = other_contact.title
#            if other_contact.website:
#                self.website = other_contact.website
#            if other_contact.company:
#                self.company = other_contact.company
#            if other_contact.phone:
#                self.phone = other_contact.phone
#            if other_contact.mobile_phone:
#                self.mobile_phone = other_contact.mobile_phone
#            if other_contact.department:
#                self.department = other_contact.department
#            if other_contact.zip_code:
#                self.zip_code = other_contact.zip_code      
      
        if not self.alias:
            # Use bool(x op y) rather than |=, &= in case either operand is None (rather than False)
            if (self.status == Contact.STATUS_ACTIVE) or (other_contact.status == Contact.STATUS_ACTIVE):
                self.status = Contact.STATUS_ACTIVE
            elif (self.status == Contact.STATUS_NEEDSACTIVATION) or (other_contact.status == Contact.STATUS_NEEDSACTIVATION):
                self.status = Contact.STATUS_NEEDSACTIVATION

            self.contacted_by_assistant = bool(self.contacted_by_assistant or other_contact.contacted_by_assistant)
            self.subscribed = bool(self.subscribed and other_contact.subscribed)
        
        # Deal with other contact's email addresses (primary and secondary)
        if other_contact.email:
            if (other_contact.email == self.email):
                pass
            elif self.extrainfo_set.filter(type=ExtraInfo.TYPE_EMAIL, data=other_contact.email).count() > 0:
                pass
            elif not self.email:
                self.email = other_contact.email
            else:
                ei = ExtraInfo(type=ExtraInfo.TYPE_EMAIL, data=other_contact.email, owner_contact=self)
                ei.save()        
        
        for ei in other_contact.extrainfo_set.all():
            if (ei.type == ExtraInfo.TYPE_EMAIL and ei.data == self.email):
                ei.delete()
            elif self.extrainfo_set.filter(type=ei.type, data=ei.data).count() > 0:
                ei.delete()
            else:
                ei.owner_contact = self
                ei.save()
                
            
        self.save()
        other_contact.delete()
        
    def inactivate(self):
        self.status = Contact.STATUS_INACTIVE
        self.social_json = None
        self.save()
        
    def link(self):
        if self.email:
            return 'mailto:%s' % self.email
        elif self.twitter_screen_name:
            return 'http://twitter.com/%s' % self.twitter_screen_name
        else:
            return '#'
        
    def twitter_link(self):
        if self.twitter_screen_name:
            return 'http://twitter.com/%s' % self.twitter_screen_name
        else:
            return '#'
        
    def befriend_from(self, tu):
        if self.twitter_screen_name:
            tu.befriend(self.twitter_screen_name)
        else:
            raise Exception()
            
    def __unicode__(self):
        return "<%s>" % self.email
    

    
class Receipt(Model):
    # TODO: Don't assume this is only created once when a new account is created! [eg 8/5]
    
    PAYMENT_GATES = (
        ('PP', 'Paypal'),
        ('2C', '2Checkout'),
    )
    
    STATUS_ACTIVE = 'rACT'
    STATUS_CANCELED = 'rCAN'
    STATUS_EOT = 'rEOT'
    
    STATUS = (
        (STATUS_ACTIVE, _('Active')),
        (STATUS_CANCELED, _('Cancelled')),
        (STATUS_EOT, _('End of subscription term')),
    )
    
    status = models.CharField(default=STATUS_ACTIVE, choices=STATUS, max_length=4)
    gateway = models.CharField(max_length=2, choices=PAYMENT_GATES, default='PP', blank=True, null=True) 
    name = models.CharField(max_length=2, blank=True, null=True)
    account = models.ForeignKey(Account, blank=True, null=True)
    alias = models.ForeignKey(Alias, blank=True, null=True) # TODO: we may want to allow payments per Alias in the future, not sure yet.
    service = models.ForeignKey(Service, blank=True, null=True)
    promo = models.ForeignKey(Promo, blank=True, null=True) # did the Account use some kind of Promo?
    paypal = models.ForeignKey(PayPalIPN, null=True, blank=True)
    date = UTCDateTimeField(default=now)
    
    
   
    def next_payment_date(self):
        # Estimate the next payment date based on the # of payments received so far.
        # TODO: Check this for correctness when a user changes their service type.
        
        queryset = Payment.objects.filter(account=self.account).order_by('-date')
        num_payments = queryset.count()
        
        if self.paypal and self.paypal.period3:     # new style
                    
            payment_tuples = \
                [(self.paypal.period1, self.paypal.amount1),
                 (self.paypal.period2, self.paypal.amount2),
                 (self.paypal.period3, self.paypal.amount3)]
            
            for period, amount in payment_tuples:
                # The order of stuff in this loop needs to be very specific!
                if (not period):        # Unused entries in the table, skip
                    continue
                if num_payments == 0:
                    break
                if (not amount):
                    continue
                num_payments -= 1
                if num_payments == 0:
                    break
                   
            offset = paypal_timedelta(period)
            
        else:
            if self.account.service.num_dmy:
                offset = paypal_timedelta('%d %s' % (self.account.service.num_dmy, self.account.service.dmy))
            else:
                return None
            
        if queryset.count() > 0:
            return queryset[0].date + offset
        else:
            return self.date + offset
        
    
    def __unicode__(self):
        return u'%s (%s)' % (self.name, self.gateway)
    def __str__(self):
        return '%s (%s)' % (self.name, self.gateway)


class ReceiptAdmin(admin.ModelAdmin):
    list_display = ('id','gateway','name','account','alias','service','promo')
    ordering = ('-id',)
    search_fields = ['id','gateway','name','account__base_url','alias__email','service__name']

class EmailFolder(Model):
    '''This represents a folder on a mail server, for crawled mails'''
    alias = models.ForeignKey(Alias)
    name = models.CharField(max_length=128, null=True)
    highest_crawled = models.CharField(max_length=16, null=True)
    highest_path = TruncCharField(max_length=256, null=True, blank=True)        # for Outlook Web Access
    highest_date = UTCDateTimeField(null=True, blank=True)
    sent = models.NullBooleanField(null=True, blank=True, default=False)       # is this an email folder for "sent" mails?

    def as_dict(self):
        '''
        Return the relevant fields of EmailFolder as a dictionary
        This is passed to the crawl task in Celery (celery_worker),
        and returned to the callback API
        '''
        
        return {
            'name':             self.name,
            'highest_crawled':  self.highest_crawled,
            'highest_path':     self.highest_path,
            'highest_date':     self.highest_date                
        }
        
    def __unicode__(self):
        return u'%s %s: %s' % (self.alias.email, self.name, str(self.highest_date))
     
                                        
class EmailManager(Manager):
    
    def twitter_q(self):
        return Q(source=Email.SOURCE_TWITTER) | Q(type=Email.TYPE_TWITTER)
    
    def viewable_q(self, alias):
        """The set of emails visible to the alias in the newsfeed.
        Don't make this query complicated - we don't want to slow it down
        Make sure it filters for the particular account!"""
        if alias.account.transparent:
            # Can see mails that have you in them or have an external (non-team) contact
            # return Q(owner_alias=alias) | Q(email2contact__contact=alias.contact, email2contact__type__in=Email2Contact.TYPES_VIEWABLE) | Q(contacts__alias=None)
            return Q(owner_account=alias.account) & (Q(owner_alias=alias) | Q(contacts__alias=None) | Q(source=Email.SOURCE_QUICKBOX))
        else:
            # Can see only mails that have you in them
            # return Q(owner_alias=alias) | Q(email2contact__contact=alias.contact, email2contact__type__in=Email2Contact.TYPES_VIEWABLE)  
            return Q(owner_alias=alias)     # This also filters for the account, so we don't need a separate owner_account filter

        
    def viewable(self, alias):
        """The set of emails visible to the alias in the newsfeed.
        The caller must apply distinct() to these results."""
        return self.filter(self.viewable_q(alias))
    
class Email(Model):
    
    objects = EmailManager()
    
    VERSION = 4
    
    TYPE_RFC822 = 'rfc822'          # Message from an email
    TYPE_TWITTER = 'twitter'        # Twitter message
    TYPE_OTHER = 'other'            # Other
       
    TYPE = (
        (TYPE_RFC822, 'Regular email'),
        (TYPE_TWITTER, 'Twitter message'),
        (TYPE_OTHER, 'Other message')
    )
    
    MESSAGE_TYPE = (
        (EMAIL_TYPE_USER, 'Emails CCed to us by the user'),
        (EMAIL_TYPE_OTHER, 'Replies by other people'),
        (EMAIL_TYPE_NOTED, 'Non-debug mails we sent'),
        (EMAIL_TYPE_DEBUG, 'Debug output we sent'),
        (EMAIL_TYPE_INCOMPLETE, 'Email not parsed yet (cleared periodically by cron job)')
    )
    MESSAGE_SUBTYPE = (
        (EMAIL_SUBTYPE_USER_REGISTER, 'User registration email'),
        (EMAIL_SUBTYPE_USER_CREATE_REMINDER, 'User creating a simple reminder'),
        (EMAIL_SUBTYPE_USER_CREATE_EVENT, 'User creating a new event'),
        (EMAIL_SUBTYPE_USER_PARSE_FAILED, 'Cannot translate the email'),
        (EMAIL_SUBTYPE_USER_FORMMAIL_CONFIRM, 'Placeholder in the newsfeed -- a form mail was sent out'),
        (EMAIL_SUBTYPE_NOTED_REGISTER, 'Welcome email'),
        (EMAIL_SUBTYPE_NOTED_CONFIRM_USER, 'Tell admin to confirm a new user'),
        (EMAIL_SUBTYPE_NOTED_PENDING_USER, 'Sent to user letting them know they are pending.'),
        (EMAIL_SUBTYPE_NOTED_ADMIN_NOTIFY, 'Tell admin a user is now activated'),
        (EMAIL_SUBTYPE_NOTED_REMINDER, 'Reminder email'),
        (EMAIL_SUBTYPE_NOTED_DIGEST, 'Reminder daily digest'),
        (EMAIL_SUBTYPE_NOTED_CONTACT_REPLY, 'Reply to contact'),
        (EMAIL_SUBTYPE_NOTED_REGISTER_FAIL, 'Failed to register'),
        (EMAIL_SUBTYPE_NOTED_CONFIRMATION, 'Email confirmation to account_user post-add-event'),
        (EMAIL_SUBTYPE_NOTED_TAG_LOOKUP, 'List of tags with corresponding emails'),
        (EMAIL_SUBTYPE_NOTED_ASSISTANT_CHANGED, 'Assistant name/email change notification'),
        (EMAIL_SUBTYPE_NOTED_REQUEST_FILE_PERMS, 'Assistant asking someone to grant permission for a file'),
        (EMAIL_SUBTYPE_NOTED_GRANT_FILE_PERMS, 'Assistant telling someone permission is granted'),
        (EMAIL_SUBTYPE_NOTED_ASK_STATUS, 'Assistant asking someone for a status update'),
    )
    
    SOURCE_DIRECT =         'sEML'              # Email was emailed directly to the assistant
    SOURCE_API =            'sAPI'              # Email was sent to us via the API
    SOURCE_CRAWLED =        'sCWL'              # Email was crawled from somebody's inbox
    SOURCE_QUICKBOX =       'sQUI'              # Email was typed into the quickbox
    SOURCE_TWITTER =        'sTWI'              # Email came from Twitter
    SOURCE_FOLLOWUP_REMINDER = 'sFWR'           # Email is to make a followup reminder
    SOURCE_FORMMAILCONF =   'sFRC'              # Email was auto-generated to confirm that your form mails were sent out
 
    SOURCE_SUBTYPE_EMAIL_QUOTED     = 'emQO'    # ... This message was extracted from the quoted part of another email
    SOURCE_SUBTYPE_TWITTER_DM_EMAIL = 'twEM'    # ... Twitter sent us an email for a direct message
    SOURCE_SUBTYPE_TWITTER_DM       = 'twDM'    # ... We crawled this Direct Message from Twitter
    SOURCE_SUBTYPE_TWITTER_REPLY    = 'twRE'    # ... We crawled this Reply from Twitter
    SOURCE_SUBTYPE_TWITTER_RETWEET  = 'twRT'    # ... We retweeted another Twitter tweet
    SOURCE_SUBTYPE_CSV_NOTE         = 'twCN'    # ... generated by a CSV import
    
    ALLOWED_SOURCES_FOR_REMINDERS = [SOURCE_DIRECT, SOURCE_TWITTER, SOURCE_QUICKBOX, SOURCE_FOLLOWUP_REMINDER]
    
    # fields migrated from schedule/model.py Events
    
    summary = TruncCharField(_("summary"), max_length=255, null=True, blank=True, help_text=_('This is the snippet generated from the email body.'))
    message_dict_path = models.FilePathField(path=settings.FILE_STORE_PATH, null=True, help_text=_('Where the message body (and other stuff) is stored on Cloud Files'))

    # Fields filled in when Email object is created
    version = models.IntegerField(null=True, help_text=_('Which version of the Email model this is (latest=%d)') % VERSION)
    type = models.CharField(max_length=16, default=TYPE_RFC822, choices=TYPE, help_text=_('What kind of message this is: "rfc822" or "twitter"'))
    source = models.CharField(max_length=4, null=True, help_text=_('Where this email came from'))
    source_subtype = models.CharField(max_length=4, null=True, blank=True, help_text=_('Subtype of the email (mostly for Twitter)'))
    headers = TextField(null=True)                                      # Python dictionary of headers (rfc822 only)
    message_id = TruncCharField(max_length=255, null=True, help_text=_('Message-Id: header'))
    in_reply_to = TruncCharField(max_length=255, null=True, help_text=_('In-Reply-To: header'))
    references = TruncCharField(max_length=1024, null=True, help_text=_('References: header'))
    date_sent = UTCDateTimeField(default=now, help_text=_('When this message was sent, converted to UTC time'))
    timezone = models.CharField(max_length=64, null=True, help_text=_('Time zone this message was sent from'))
    
    from_name = TruncCharField(max_length=128,null=True)
    from_address = models.CharField(max_length=128,null=True)
    to_name = TruncCharField(max_length=128,null=True)                  # Just the first name and address in the To: field; this isn't very useful
                                                                        #  (used mostly for Twitter messages where there is at most one "To")
    to_address = models.CharField(max_length=128,null=True)
    subject = TruncCharField(max_length=256,null=True,blank=True)
    subject_normalized = TruncCharField(max_length=256,null=True,blank=True, help_text=_('Subject line, cleaned up and trimmed'))
    date_processed = UTCDateTimeField(default=now)
    date_migrated = UTCDateTimeField(null=True, blank=True, help_text=_('Last date this Email was run through a migration script'))
    
    message_body = models.TextField(blank=True, null=True)                      # Body of the email, with footers but without replies
                 
    message_type = models.CharField(max_length=4,null=True, choices=MESSAGE_TYPE)
    message_subtype = models.CharField(max_length=4,null=True, choices=MESSAGE_SUBTYPE)
    events = models.ManyToManyField('schedule.Event', null=True, blank=True, related_name='email_events')
    from_contact = models.ForeignKey(Contact,null=True, related_name='email_from_contact')  # Who wrote this message
    contacts = models.ManyToManyField(Contact, through='Email2Contact', related_name='email_set') 
    remote_users = models.ManyToManyField('remote.RemoteUser', through='Email2RemoteUser', related_name='email_remote')
    owner_alias = models.ForeignKey(Alias, null=True, related_name='email_owner')
    owner_account = models.ForeignKey('Account', null=True, blank=True, related_name='owner_account_emails')
    crawled_alias = models.ForeignKey(Alias,null=True, related_name='email_crawled')        # Whose IMAP account we crawled this from
    parent = models.ForeignKey('self', null=True, related_name='email_children')    # The parent of this email in a thread (null=root node)
    original_email = models.ForeignKey('self', null=True, blank=True, related_name='email_cc_emails')
    original_parent = models.ForeignKey('self', null=True, blank=True, related_name='original_email_children')
    newsfeed_thread_date = UTCDateTimeField(null=True, default = now)   # Date used for the newsfeed timeline in threaded view
    completed = models.NullBooleanField(null=True, blank=True, default=False)   # is this thread "completed"?
    server_emailfolder = models.ForeignKey(EmailFolder,null=True)       # IMAP folder name this message came from
    server_msg_path = TruncCharField(max_length=255, null=True, blank=True)  # for Outlook Web Access
        
    def __unicode__(self):
        return u'%s' % (self.subject)
    def __str__(self):
        return self.subject
            
    # Leave this as a static method --
    # it doesn't belong in a Manager, since it doesn't actually save the Email structure that it returns
    
    @staticmethod
    def email_from_rfc822_message(msg, crawled_alias=None, source=SOURCE_DIRECT, source_subtype=None):
        """
        Based on a Python message structure representing an rfc822 message, return a models.Email object.
        
        """
            
        from email.Utils import formatdate, parsedate_tz, mktime_tz
        
        n = now()
        e = Email()
        e.version = Email.VERSION
        e.type = Email.TYPE_RFC822
        if 'X-Twitteremailtype' in msg:
            e.source = Email.SOURCE_TWITTER
            if msg['X-Twitteremailtype'] == 'direct_message':
                e.source_subtype = Email.SOURCE_SUBTYPE_TWITTER_DM_EMAIL
            elif msg['X-Twitteremailtype'] == 'reply':
                e.source_subtype = Email.SOURCE_SUBTYPE_TWITTER_REPLY
            if 'X-Twitterdirectmessageid' in msg:
                e.message_id = msg['X-Twitterdirectmessageid']
            else:
                e.message_id = msg.get('Message-ID', None)
        else:
            e.source = source
            e.source_subtype = source_subtype
            e.message_id = msg.get('Message-ID', None)
                    
        headers_struct = dict([(key, str(value)) for key, value in msg.items()])
        
        # Delivered-To and X-Forwarded-To may have multiple values,
        #  and we need them all (for messages that are auto-forwarded to the assistant) --
        #  store these as a list
        if 'Delivered-To' in msg:
            headers_struct['Delivered-To'] = msg.get_all('Delivered-To')
        if 'X-Forwarded-To' in msg:
            headers_struct['X-Forwarded-To'] = msg.get_all('X-Forwarded-To')
            
        e.headers = json.dumps(headers_struct)
        
        e.in_reply_to = msg.get('In-Reply-To', None)
        e.references = msg.get('References', None)
        e.date_sent, e.timezone = parsedate_from_email(msg['Date'])               
        e.newsfeed_thread_date = e.date_sent
        
        if 'From' in msg:
            from_name, from_address = parseaddr_unicode(msg['From'])          
            e.from_name, e.from_address = from_name, from_address.strip().lower()
        if 'To' in msg:
            e.to_name, e.to_address = parseaddr_unicode(msg['To'])  # Just get the first one
            e.to_address = e.to_address.strip().lower()
            
        e.subject = strip_linebreaks(decode_header_unicode(str(msg.get('subject', ''))))
        e.subject_normalized = normalize_subject(e.subject)
        
        extras = e.get_extras()
        
        edata = msg.as_string()
        
        return e, edata, extras
    
        
    # Leave this as a static method --
    # it doesn't belong in a Manager, since it doesn't actually save the Email structure that it returns
    @staticmethod
    def email_from_content(**kwargs):
    
        type_ = kwargs.get('type', Email.TYPE_OTHER)
        alias = kwargs.get('alias')
        content = kwargs.get('content', '')
        subject = kwargs.get('subject')
        from_name = kwargs.get('from_name')
        from_address = kwargs.get('from_address')
        to_name = kwargs.get('to_name')
        to_address = kwargs.get('to_address')
        to_all = kwargs.get('to_all')               # from the new desktoppy API
        date = kwargs.get('date') or kwargs.get('date_sent')
        user_tz = kwargs.get('user_tz')
        in_reply_to = kwargs.get('in_reply_to')
        references = kwargs.get('references')
        source = kwargs.get('source', Email.SOURCE_QUICKBOX)
        source_subtype = kwargs.get('source_subtype')
        message_id = kwargs.get('message_id')
        extra = kwargs.get('extra')                 # from the new desktoppy API
        
        # Fields for the quickbox -- what object this email is in reference to
        ref_contact_id = kwargs.get('ref_contact_id')
        ref_bucket_slug = kwargs.get('ref_bucket_slug') 
        reply_id = kwargs.get('reply_id', '')
        reply_to_all = kwargs.get('reply_to_all', False)
        private = kwargs.get('private', False)      
        
        n = now()
        e = Email()
        e.version = Email.VERSION
        e.type = type_
        e.message_id = message_id or make_msgid()   
        # TODO: Use email ids, not event ids, for reply_to!!
        
        if alias:
            e.owner_alias = alias
            e.crawled_alias = alias
        
        if alias and alias.account and alias.account.status == ACCOUNT_STATUS_ACTIVE and reply_id:
            try:
                in_reply_to = Email.objects.get(pk=int(reply_id), owner_account=alias.account)
                references = in_reply_to
            except:
                pass
                             
        if type(in_reply_to) is Email:
            e.in_reply_to = in_reply_to.message_id            
        elif type(in_reply_to) is str:
            e.in_reply_to = in_reply_to
            
        if type(references) is Email:
            e.references = references.message_id
        elif type(references) is str:
            e.references = references       
                                             
        if from_address:
            e.from_name, e.from_address = from_name or '', from_address
        elif alias and (e.get_addr_space() == ADDR_SPACE_RFC822):
            e.from_name, e.from_address = alias.contact.get_full_name() or '', alias.contact.email
        if to_address:
            e.to_name, e.to_address = to_name or '', to_address
            # TODO: Cc name
        elif alias and (e.get_addr_space() == ADDR_SPACE_RFC822):
            assistant = alias.account.get_assistant()
            e.to_name, e.to_address = assistant.get_full_name() or '', assistant.email
        
        e.date_sent = date or n
        e.date_processed = date or n
        e.newsfeed_thread_date = date or n
        
        if subject:
            e.subject = subject
        else:
            if type(in_reply_to) is Email:
                from mail import strip_subject_reply_or_forward
                e.subject = _('Re: ') + strip_subject_reply_or_forward(in_reply_to.subject)
            else:
                e.subject = ellipsize(content, length_min=60, length_max=75)
        e.subject_normalized = normalize_subject(e.subject)    

        headers = {}        
        
        if to_all is not None:                      # New-style JSON structure for representing to/cc folks, see desktoppy API for structure
            headers['to_all'] = to_all
                           
        if ref_contact_id is not None:
            headers['x-noted-ref-contact'] = str(ref_contact_id)
        if ref_bucket_slug is not None:
            headers['x-noted-ref-bucket'] = str(ref_bucket_slug)
        if reply_id is not None:
            headers['x-noted-reply-message-id'] = str(reply_id)
        if reply_to_all is not None and reply_id is not None:
            headers['x-noted-reply-to-all'] = str(reply_to_all)
        if private:
            headers['x-noted-perms'] = 'private'
    
        if kwargs.get('twitter_email_type'):
            headers['x-twitteremailtype'] = kwargs['twitter_email_type']
        if kwargs.get('twitter_sender_id'):
            headers['x-twittersenderid'] = kwargs['twitter_sender_id']
            headers['x-twittersendername'] = encode_header_unicode(kwargs['twitter_sender_name'])
            headers['x-twittersenderscreenname'] = encode_header_unicode(kwargs['twitter_sender_screen_name'])
        if kwargs.get('twitter_recipient_id'):
            headers['x-twitterrecipientid'] = kwargs['twitter_recipient_id']
            headers['x-twitterrecipientname'] = encode_header_unicode(kwargs['twitter_recipient_name'])
            headers['x-twitterrecipientscreenname'] = encode_header_unicode(kwargs['twitter_recipient_screen_name'])

        if extra:                               # The 'extra' field from the new desktoppy API
            headers.update(extra)             #  (not to be confused with 'extras' below)

        e.headers = json.dumps(headers)

        e.source = source
        e.source_subtype = source_subtype
        e.message_body = content

        edata = content

        extras = e.get_extras()                 # A representation of the email's headers and metadata

        return e, edata, extras

    def get_attendees(self):
        attendees = self.contacts.filter(email2contact__type__in=Email2Contact.TYPES_ATTENDEES)
        return attendees


    def retrieve_message_part(self, mimepath, container_name, file_name):
        """ Ask celery_worker to retrieve one part of a message from IMAP (a file attachment) and
        store it on Cloud Files.

        Returns the path on Cloud Files (starts with "mosso:").
        """
        from crawl.tasks import retrieve_and_store_file_attachment_task

        log("retrieve_message_part start...")

        if not self.crawled_alias:
            raise Exception('Email.retrieve_message_part: no source for message')

        try:
            q = retrieve_and_store_file_attachment_task(
                self.crawled_alias.get_mail_settings(),
                self.server_emailfolder.as_dict(),
                self.server_msg_path,
                mimepath,
                container_name,
                file_name)
            result = q.get()
            if result['success']:
                return result['file_path']
            else:
                return None
        except:
            log("retrieve_message_part failed:", traceback.format_exc())
            return None
        
            
    def delete_storage(self):
        "If this file is stored locally or on Mosso, delete the data"
        from lib.cloud import CloudFilesManager
        if self.message_dict_path:
            log(" -- Email.delete_storage: Deleting from %s ..." % self.message_dict_path)
            if self.message_dict_path.startswith('mosso:'):
                if settings.MOSSO_MESSAGES_ENABLED:
                    if CloudFilesManager().delete_file(self.message_dict_path):
                        self.message_dict_path = None
                        self.save()
                        log("... done")
                    else:
                        log("File.delete_storage(): can't delete %s from Mosso" % self.message_dict_path)
                else:
                    log("File.delete_storage(): MOSSO_FILES_ENABLED is false, can't delete %s" % self.message_dict_path)
            else:
                raise NotImplementedError
    
    def delete(self, *args, **kwargs):
        "Override the usual delete method so that the Mosso file gets deleted too"
        self.delete_storage()
        return super(Email, self).delete(*args, **kwargs) 
           
    def get_timezone(self):
        return timezone_from_timezonestr(self.timezone)
    
    def get_headers(self):
        if self.headers:
            return CaselessDict(json.loads(self.headers))
        return None
    
    def get_addr_space(self):
        # Tell what *kind* of address the from and to addresses are
        if self.type == Email.TYPE_TWITTER:
            return ADDR_SPACE_TWITTER
        else:
            return ADDR_SPACE_RFC822
    
    def get_body(self):   
        "Get the body of the email, for ... expansion and for syncing to Salesforce"
        if self.message_body is not None:
            return self.message_body
        elif self.message_dict_path:
            from lib.cloud import CloudFilesManager
            j = CloudFilesManager().get_file(self.message_dict_path)
            return json.loads(j)['body']
        
    def get_extras(self):
        
        addr_space = self.get_addr_space()
        headers = self.get_headers()

        extras = {}
        extras['addr_space'] = addr_space
        extras['headers'] = headers or []
        
        def get_name_tuple(addr_struct):
            "Get a name, addr tuple from a to_all element"
            if 'name_normalized' in addr_struct:
                return (addr_struct['name_normalized'], addr_struct['addr'])
            elif 'name' in addr_struct:
                return (addr_struct['name'], addr_struct['addr'])
            else:
                return ('', addr_struct['addr'])
            
        if addr_space == ADDR_SPACE_RFC822 and headers and 'to' in headers:
            extras['to'] = get_name_addr_list(headers, 'to')
            extras['cc'] = get_name_addr_list(headers, 'cc')
            extras['bcc'] = get_name_addr_list(headers, 'bcc')
        else:
            if 'to_all' in headers:             # New to_all structure from desktoppy
                extras['to'] = [get_name_tuple(t) for t in headers['to_all'] if t['type'].lower() == 'to']
                extras['cc'] = [get_name_tuple(t) for t in headers['to_all'] if t['type'].lower() == 'cc']
                extras['bcc'] = [get_name_tuple(t) for t in headers['to_all'] if t['type'].lower() == 'bcc']
            elif self.to_address:
                extras['to'] = [(self.to_name or '', self.to_address)]
                extras['cc'] = []
                extras['bcc'] = []
            else:
                extras['to'] = []
                extras['cc'] = []
                extras['bcc'] = []
            
            
        extras['tocc'] = extras['to']+extras['cc']
        extras['tocc_noted'] = []
        extras['tocc_other'] = []
        
        for name, addr in extras['tocc']:
            addr = addr.strip().lower()
            if addr_space == ADDR_SPACE_RFC822:
                if is_valid_email_address(addr):
                    if is_assistant_address(addr):
                        extras['tocc_noted'].append((name, addr))
                    else:
                        extras['tocc_other'].append((name, addr))
            else:
                extras['tocc_other'].append((name, addr))

        if headers:
            if 'x-noted-ref-contact' in headers:
                try:
                    extras['ref_contact_id'] = int(headers['x-noted-ref-contact'])
                except:
                    pass
            if 'x-noted-ref-bucket' in headers:
                extras['ref_bucket_slug'] = headers['x-noted-ref-bucket']
            if 'x-noted-reply-to-all' in headers:
                extras['reply_to_all'] = (headers['x-noted-reply-to-all'].lower() == 'true')
            if 'x-noted-perms' in headers:
                extras['private'] = (headers['x-noted-perms'].lower() == 'private')
            
            # For rfc822 envelopes of Twitter messages    
            if 'x-twitteremailtype' in headers:
                extras['twitter'] = True
                extras['twitter_email_type'] = headers['x-twitteremailtype']
            if 'x-twittersenderid' in headers:
                extras['twitter_sender_id'] = int(headers['x-twittersenderid'])
                extras['twitter_sender_name'] = decode_header_unicode(headers['x-twittersendername'])
                extras['twitter_sender_screen_name'] = decode_header_unicode(headers['x-twittersenderscreenname'])
            if 'x-twitterrecipientid' in headers:
                extras['twitter_recipient_id'] = int(headers['x-twitterrecipientid'])
                extras['twitter_recipient_name'] = decode_header_unicode(headers['x-twitterrecipientname'])
                extras['twitter_recipient_screen_name'] = decode_header_unicode(headers['x-twitterrecipientscreenname'])
    
            if 'sender' in headers:
                extras['sender'] = parseaddr_unicode(headers['sender'])
            
            # These two are stored in the extras as a list
            if 'delivered-to' in headers:
                if type(headers['delivered-to']) is list:
                    extras['delivered_to'] = headers['delivered-to']
                else:
                    extras['delivered_to'] = [headers['delivered-to']]
            if 'x-forwarded-to' in headers:             # Gmail-specific header?
                if type(headers['x-forwarded-to']) is list:
                    extras['forwarded_to'] = headers['x-forwarded-to']
                else:
                    extras['forwarded_to'] = [headers['x-forwarded-to']]                
                    
        return extras
    
    def has_viewing_perms(self, alias):
        """Determine whether this mail can be viewed by this alias, according to the account transparency setting
        For a filter that gets viewable mails, try Email.objects.viewable_q"""
        # Does NOT filter for belonging to the right account!
        try:
            if self.owner_account != alias.account:         # paranoia
                return False
            
            if self.owner_alias == alias:
                return True
            
            # If the mail has you in it, it's good
            if self.contacts.filter(alias=alias, email2contact__type__in=Email2Contact.TYPES_VIEWABLE).count() > 0:
                return True
            
            if alias.account.transparent:
                # If any of the contacts are external (non-team), it's good
                if self.contacts.filter(alias=None).count() > 0:
                    return True
                
            return False
                
            
            if alias.account.transparent:
                # Can see mails that have you in them or have an external (non-team) contact
                return (self.owner_alias==alias) or Q(email2contact__contact=alias.contact, email2contact__type__in=Email2Contact.TYPES_VIEWABLE) | Q(contacts__alias=None)
            else:
                # Can see only mails that have you in them
                return Q(owner_alias=alias) | Q(email2contact__contact=alias.contact, email2contact__type__in=Email2Contact.TYPES_VIEWABLE)
            
            if alias is None:
                return False
            if self.contacts.filter(alias=alias).count() > 0:
                return True
            return False
        except:
            return False
    
    def find_thread_root(self):
        result = self
        while result.original_parent:
            result = result.original_parent
        return result 
    
    def set_newsfeed_thread_date(self, dt=None):
        """Set the newsfeed_thread_date of this email and propagate it up the chain.
        None means recalculate the date from the children."""
        
        e = self
        if dt is None:
            dt = self.date_sent
            for child in e.original_email_children.all():
                dt = max(child.newsfeed_thread_date, dt)
            e.newsfeed_thread_date = dt
              
        while e:
            dt = max(e.newsfeed_thread_date, dt)
            e.newsfeed_thread_date = dt
            e.save()
            e = e.original_parent       
            
            
    # Add a name/address to an email's ManyToMany relationship
    def create_email2contact_by_nameaddr(self, name, addr, owner_alias, account, type, update_date=None, status=None, addr_space=ADDR_SPACE_RFC822):
        if addr_space == ADDR_SPACE_RFC822:
            if is_valid_email_address(addr) and not is_assistant_address(addr):
                contact, created = Contact.objects.get_or_create_smart(\
                    name = name,
                    email = addr,
                    viewing_alias = owner_alias,        # this can be None, in which case it becomes a public contact
                    account = account,
                    status = status
                )
                if contact:
                    if update_date:
                        if contact.first_contacted_to_date is None:
                            contact.first_contacted_to_date = update_date
                        contact.update_last_contacted_date(owner_alias)
                        contact.save()
                    ec, created = Email2Contact.objects.get_or_create(account=account or self.owner_account, contact=contact, email=self, type=type, active=True)
                    return contact
        elif addr_space == ADDR_SPACE_TWITTER:
            contact, created = Contact.objects.get_or_create_smart(\
                name = name,
                twitter_screen_name = addr,
                viewing_alias = owner_alias,        # this can be None, in which case it becomes a public contact
                account = account,
                status = status
            )
            if contact:
                if update_date:
                    if contact.first_contacted_to_date is None:
                        contact.first_contacted_to_date = update_date
                    contact.update_last_contacted_date(owner_alias)
                    contact.save()
                ec, created = Email2Contact.objects.get_or_create(account=account or self.owner_account, contact=contact, email=self, type=type, active=True)
                return contact
            return None
    
    # Add a name/address to an email's ManyToMany relationship
    def create_email2contact_by_contact(self, contact, account, type, viewing_alias=None, footer_fields=None):
        if footer_fields:
            # update the contact
            kwargs = {}
            for key, value in footer_fields.items():
                if key == 'url':
                    kwargs['website'] = value
                elif key == 'email':
                    pass                # for now, don't try to update email addresses from footers [eg 8/9]
                else:
                    kwargs[key] = value
            Contact.objects._update_contact(contact, **kwargs)
            contact.last_modified_date = now()
            contact.save()
        ec, created = Email2Contact.objects.get_or_create(account=account or self.owner_account, contact=contact, email=self, type=type, active=True)
        return ec, created
    
    def get_all_emails_down(self):
        results = [self]
        for child in self.original_email_children.all():
            results.extend(child.get_all_emails_down())
        return results
    
    def get_latest_contact_and_email_from_thread(self):
        all_emails = self.get_all_emails_down()
        all_emails.sort(key=lambda e: e.date_sent)
        for e in reversed(all_emails):
            queryset = e.contacts.filter(alias=None)
            if queryset.count() > 0:
                return e, queryset[0]
        return all_emails[-1], None             
    
    # Find all thread contacts starting from the given event and going up the chain of parents
    def get_all_thread_contacts_up(self, contact):    
        contacts = []
        email = self
        while email is not None:
            if email.has_viewing_perms(contact.alias):
                contacts.extend(list(email.contacts.all())) # TODO: Filter by only the active ones
            email = email.original_parent
        return uniqify(contacts)
    
    def get_reply_contacts(self, reply_mode, contact):
        # If the given contact wants to reply to this event, who would they be replying to?
        
        if reply_mode in ['default']:           
            reply_contacts = []                             # Most mails, especially crawled ones, shouldn't auto-forward to anyone
        else:
            reply_contacts = [self.from_contact, self.owner_alias.contact if self.owner_alias else None]
            if reply_mode == 'email_assistant':             # Replies by email that 'to' or 'cc' the assistant   
                pass
            elif reply_mode == 'sf_create':                 # Used for the "[SF] Create contact" items in the select box --
                                                            #  return a single, preferentially non-team, contact
                reply_contacts = []
                if self.from_contact.alias is None:
                    reply_contacts = [self.from_contact]
                if not reply_contacts:                      # Prefer To: contacts over Cc: contacts
                    non_team = self.contacts.filter(email2contact__type=Email2Contact.TYPE_TO, alias=None)
                    if len(non_team) > 0:
                        reply_contacts = [non_team[0]]
                if not reply_contacts:
                    non_team = self.contacts.filter(alias=None)
                    if len(non_team) > 0:
                        reply_contacts = [non_team[0]]
                if not reply_contacts:
                    reply_contacts = [self.from_contact]
            elif reply_mode == 'nearby_only':               # Replies by quickbox do this if you don't select reply all
                e = self
                reply_contacts = []
                while e:
                    if e.from_contact != contact:
                        reply_contacts = [e.from_contact]
                    else:
                        reply_contacts = [c for c in e.contacts.all() if c != contact]
                    if reply_contacts:
                        break
                    e = e.original_parent
            elif reply_mode == 'all_parent':
                reply_contacts += list(self.contacts.all())
            elif reply_mode == 'all_thread':
                reply_contacts += list(self.get_all_thread_contacts_up(contact))
        reply_contacts = uniqify([c for c in reply_contacts if c and (c.status in [Contact.STATUS_ACTIVE, Contact.STATUS_NEEDSACTIVATION])])
        if not reply_contacts:
            reply_contacts = [contact]
        return reply_contacts
    
    # Helper functions used by the newsfeed
    
    # TODO: Eventually phase out the entire Event model
    def is_twitter(self):
        return (self.source == Email.SOURCE_TWITTER) or (self.type == Email.TYPE_TWITTER)
    
    def is_sf_synced(self):
        return self.email2remoteuser_set.filter(status='sSYN', remote_user__type='tSFO').count() > 0
    
    def is_quickbox(self):
        return self.source == Email.SOURCE_QUICKBOX
            
    def get_summary(self):
        "Generate the snippet that appears in the newsfeed"
        if self.is_quickbox() and self.message_body is not None:    # For quickbox messages, display the whole thing
            return self.message_body.strip()
        
        if self.summary is not None:
            return self.summary
        
        # TODO: Eliminate code below this line - the above should be all we need    
        from mail import strip_subject_reply_or_forward                 
        if (self.source in [Email.SOURCE_QUICKBOX, Email.SOURCE_TWITTER]):
            # These mails have the same subject and body, so don't display the snippet twice
            subject_summary = ''
        elif (self.original_parent_id is not None) and strip_subject_reply_or_forward(self.original_parent.subject_normalized) == strip_subject_reply_or_forward(self.subject_normalized):
            # This is a reply to some other mail and the subject line hasn't changed (except for Re:, etc.), so don't display the subject
            subject_summary = ''
        else:
            subject_summary = self.subject_normalized
        subject_summary = ellipsize(subject_summary, length_min=60, length_max=80, dotdotdot=True, length_with_url_max=80)
        
        body_summary = self.message_body or ''
        # Don't use the footer when generating the snippet
            
        if not self.is_quickbox():
            body_summary = ellipsize(body_summary, length_min=180, length_max=220, dotdotdot=True, length_with_url_max=260)

        summary = (' '.join(['[' + subject_summary + ']', body_summary])).strip()
        return summary
        
    def get_buckets(self):
        """
        Return a list of all buckets associated with this email
        Used by the Conversations widget
        It's simply the union of all buckets that contacts in this email belong to
        (non-team contacts only, otherwise it could get pretty annoying)
        """
        result = []
        for c in self.contacts.filter(alias=None):
            result.extend(list(c.contact_buckets.all()))
        result = uniqify(result)
        result.sort(key=lambda b: b.name)
        return result
    
    def get_to_contacts(self, limit=10):
        result = self.contacts.filter(email2contact__type__in=Email2Contact.TYPES_RECIPIENT)
        result = [c for c in uniqify(result) if c != self.from_contact]
        if limit is not None:
            result = result[:limit]
        return result
    
    def get_ref_contacts(self):
        return uniqify(self.contacts.filter(email2contact__type__in=Email2Contact.TYPES_REFERENCE))
    
    def get_assigned_contacts(self):
        return uniqify(self.contacts.filter(email2contact__type=Email2Contact.TYPE_ASSIGNED))
                
    def get_files(self):
        return list(File.objects.filter(email=self))
        
    def get_scheduled_events(self):
        return self.events.filter(status__in=[EVENT_STATUS_SCHEDULED, EVENT_STATUS_TENTATIVE, EVENT_STATUS_FOLLOWUP_REMINDER], end__gte=now())    

    def get_reply_info(self, alias):
        contact = alias.contact
        
        reply_twitter = self.is_twitter()
        reply_all_contacts = self.get_reply_contacts('all_thread', contact)
        reply_contacts_nearby_only = self.get_reply_contacts('nearby_only', contact)
        if reply_all_contacts and reply_contacts_nearby_only:
            reply_allowed = True
            reply_all_contacts_not_me = [a for a in reply_all_contacts if (a != contact)]
            reply_to_all_allowed = (len(reply_all_contacts_not_me) > 1) or \
                ((len(reply_all_contacts_not_me) == 1) and (reply_all_contacts_not_me != reply_contacts_nearby_only))
        else:
            # Some very old events don't have event creators, just disable reply
            reply_allowed, reply_contact_individual, reply_to_all_allowed = False, False, False
            
        if reply_contacts_nearby_only:
            to_names = [c.get_tag_or_name_for_display(contact, include_email=not reply_twitter) for c in reply_contacts_nearby_only]
            to_name = ', '.join(to_names[:3]) + ('...' if len(to_names) > 3 else '')
            if reply_twitter:
                to_name += _(' via Tweet')
        else:
            to_name = ''
        
        if reply_twitter:
            reply_to_all_allowed = False
            
        d = {
            'reply_allowed':        reply_allowed,
            'reply_to_all_allowed': reply_to_all_allowed,
            'reply_twitter':        reply_twitter,
            'to_name':              to_name
        }
             
        return d
            
            
class Email2Contact(Model):
    "The 'through' model for Email's M2M relationship to Aliases"
    TYPE_FROM       =   'tFRM'
    TYPE_TO         =   'tTO '
    TYPE_CC         =   'tCC '
    TYPE_BCC        =   'tBCC'
    TYPE_FWD        =   'tFWD'  # Not in the email headers, but assistant will forward to this person
    TYPE_CRAWLED    =   'tCWL'  # Not in the email headers, but email was crawled from this person
    TYPE_ASSIGNED   =   'tASN'  # Not in the email headers, but thread is "assigned" to this person for completion
    TYPE_ICS        =   'tICS'  # Not in the email headers, but mentioned in an attached .ics file
    TYPE_ADDED      =   'tADD'  # Not in the email headers, but this person was added to the event later on
    TYPE_FORMMAIL   =   'tFOR'  # Not in the email headers, but was mailed via form mail
    TYPE_REF        =   'tREF'  # Not in the email headers, but the mail is "about" this person (via quickbox)
    TYPE_RULE       =   'tRUL'  # Not in the email headers, but this contact was extracted from the email via an EmailRule / EmailRegex
    
    # The types of contacts that are attendees of a given email's events
    
    TYPES_RECIPIENT = [TYPE_FROM, TYPE_TO, TYPE_CC, TYPE_BCC, TYPE_ADDED, TYPE_ICS, TYPE_FWD, TYPE_FORMMAIL, TYPE_CRAWLED]
    TYPES_ATTENDEES = TYPES_RECIPIENT + [TYPE_FROM]
    TYPES_VIEWABLE = TYPES_ATTENDEES + [TYPE_ASSIGNED]      # The types of contacts that can see a given email
    TYPES_REFERENCE = [TYPE_REF, TYPE_RULE]                     # The email is 'in reference to' these people
    
    # The types of contacts that can get reminders for a scheduled event
    ALLOWED_TYPES_FOR_REMINDERS = [TYPE_FROM, TYPE_TO, TYPE_CC, TYPE_FWD, TYPE_ASSIGNED, TYPE_ADDED]
    
    email = models.ForeignKey(Email)
    contact = models.ForeignKey(Contact)
    type = models.CharField(max_length=4, null=True, blank=True)    
    active = models.NullBooleanField(default=True)
    account = models.ForeignKey(Account, null=True)    # to make joins faster

class Email2RemoteUser(Model):
    '''The 'through' model for email's M2M relationship to a RemoteUser (for syncing to/from external sources)
    '''
    
    STATUS_SENT_ACTIONS_ONLY = 'sSAC'       # We sent the actions associated with this email to the external source, but not the actual email yet
    STATUS_SENT     =   'sSEN'              # The email was sent to/from the external source, but no response yet
    STATUS_SYNCED   =   'sSYN'              # The email was successfully synced with the external source
    STATUS_FAILED   =   'sFAI'              # The email could not be synced with the external source; don't try to sync it again
    
    MAX_TIMES_SENT = 3                      # After sending an email this many times, mark it as failed
    
    email = models.ForeignKey(Email)
    remote_user = models.ForeignKey('remote.RemoteUser')
    remote_userid = models.CharField(max_length=32, null=True, blank=True)          # equal to remote_user.remote_userid
    remote_organizationid = models.CharField(max_length=32, null=True, blank=True)  # equal to remote_user.remote_organizationid
    date_created = UTCDateTimeField(default=now)
    date_updated = UTCDateTimeField(default=now)
    account = models.ForeignKey(Account, null=True)     # to make joins faster
    
    status = models.CharField(max_length=4, null=True, blank=True)                                        
    num_times_sent = models.IntegerField(default=0)
    
    class Meta:
        unique_together = ("email", "remote_user")

    
# Filters on incoming emails -- we'll make this more general later

class EmailRuleManager(Manager):
    
    def active(self):
        return self.filter(status=EmailRule.STATUS_ACTIVE)
    
    # TODO: Migrate this over to the new rule scheme
    def find_procmail_rule(self, e):
        # Find a matching rule for this email, in order from most specific to least specific rule
                
        if not e.owner_alias:
            return None    
        for er in self.active().filter(type=EmailRule.TYPE_PROCMAIL, owner_alias=e.owner_alias).exclude(from_address=None):
            if er.procmail_matches(e):
                return er
        for er in self.active().filter(type=EmailRule.TYPE_PROCMAIL, owner_alias=e.owner_alias).filter(from_address=None):
            if er.procmail_matches(e):
                return er            
        for er in self.active().filter(type=EmailRule.TYPE_PROCMAIL, owner_account=e.owner_account).exclude(from_address=None):
            if er.procmail_matches(e):
                return er 
        for er in self.active().filter(type=EmailRule.TYPE_PROCMAIL, owner_account=e.owner_account).filter(from_address=None):
            if er.procmail_matches(e):
                return er           
        return None
    
    def find_email_rules(self, e, type):
        """Find the rules that pertain to a specific email.
        Returns None if any of the rules' actions say the email is to be ignored.
        Otherwise, returns a list of rules.
        """
        result = []
        for rule in self.active().filter(owner_account=e.owner_account, type=type):
            if rule.matches(e):
                actions = rule.actions()
                for a in actions:
                    if a['action'] == 'ignore':
                        return None             # do nothing with this mail
                result.append(rule)
        return result
    
    def get_default_for_account(self, alias, account):
        """Return a new rule for an alias with sensible defaults.
        Used by create_initial_for_account below and when creating a new email rule in the UI.
        """
        
        rule = EmailRule()
        rule.status = EmailRule.STATUS_ACTIVE
        rule.type = EmailRule.TYPE_SF
        rule.owner_alias = alias
        rule.owner_account = account
        rule.enabled_incoming = True
        rule.enabled_outgoing = True
        
        rule.match_json = json.dumps({
            'addr': {
                'matches':  True,               # True if matches, False to invert the sense of the test
                'type': 'team',                 # 'team', 'external', 'str', 'regex'
                'data': None
            }
        })
        
        rule.actions_json = json.dumps([{
            'action':      'sync',
            'task_type':   _('Email') 
            #'type':     'Contact',            # TODO: Are we using these?
            #'source':   'regex'
        }])
        
        return rule     
            
    def create_initial_for_account(self, account):
        """Initialize the rules for an account"""
        
        # The default rule (sync all emails) - not owned by any specific alias
        rule = self.get_default_for_account(None, account)      
        rule.save()
        
class EmailRegex(Model):
    '''If we are auto-creating a contact on an incoming mail (EmailRule), 
    use these to extract the fields we care about
    We use named capturing groups in the regexes to represent the fields we want:
    (?P<email>blah), etc
    '''
    
    owner_account = models.ForeignKey(Account, null=True, blank=True)
    description = models.TextField(null=True, blank=True)                       # description for the edit page
    regex = models.CharField(max_length=256, null=True, blank=True)
        
    def __unicode__(self):
        return self.description or ''
    
       
class EmailRule(Model):
    
    STATUS_ACTIVE = 'sACT'
    STATUS_INACTIVE = 'sINA'
    
    # Type -- determines when the rule is applied
    TYPE_PROCMAIL = 'tPRC'                                                      # Procmail rules -- apply when the mail is parsed (e.g., delete message)
    TYPE_SF = 'tSFO'                                                            # Salesforce rule -- applies at Salesforce sync time
    TYPE_FOLLOWUP = 'tFOL'                                                      # Followup rule -- applies when we get an incoming followup request via the API
    
    ACTION_DELETE = 'erDL'                                                      # delete this email immediately (for TYPE_PROCMAIL only)

    status = models.CharField(max_length=4, null=True, blank=True)
    type = models.CharField(max_length=4, default=TYPE_PROCMAIL)
    
    owner_account = models.ForeignKey(Account, null=True, help_text='Account this rule pertains to')                       # Account this rule pertains to
    owner_alias = models.ForeignKey(Alias, null=True, blank=True, help_text='Author of the rule -- but rule pertains to everyone in the team (according to their use_regexes setting) -- can be blank')           # 
    date_created = UTCDateTimeField(default=now)
    date_modified = UTCDateTimeField(default=now)
    
    # Fields for TYPE_SF
    enabled_incoming = models.BooleanField(default=True, help_text='Is this rule enabled for incoming mail?')                        # 
    enabled_outgoing = models.BooleanField(default=True, help_text='Is this rule enabled for outgoing mail?')                        #
    
    match_json = models.TextField(null=True, blank=True, help_text='Criteria for matching an email')                        # 
    actions_json = models.TextField(null=True, blank=True, help_text='What to do if there is a match? (Usually create a Lead or Contact -- or record to Activity History)')                      # 
    regexes = models.ManyToManyField(EmailRegex, null=True, blank=True, help_text='Which regexes to apply if there is a general match (email from/to a particular address matches match_json above)')
    
    extra_json = models.TextField(null=True, blank=True, help_text='Other parameters for the rule matching (highly optional)')                        # 

    # Fields that apply at email crawl time - TYPE_PROCMAIL
    # (TODO: Migrate these to the new scheme)
    from_name = TruncCharField(max_length=128,null=True,blank=True,editable=False,help_text='???? not sure if we are using this one')             # 
    from_address = models.CharField(max_length=128,null=True,blank=True,editable=False,help_text='???? not sure if we are using this one.')        # 
    subject_normalized = TruncCharField(max_length=256,null=True,blank=True,editable=False,help_text='???? not sure if we are using this one.<br/><br/>subject line, decoded and with extra spaces, etc. removed')    # 
    action = models.CharField(max_length=4, null=True, blank=True,editable=False,help_text='???? not sure if we are using this one.')

    # Fields for TYPE_FOLLOWUP
    enabled_contacts = models.BooleanField(default=False, help_text='Is this followup rule enabled for SF Contacts?')                       # Is this followup rule enabled for SF Contacts?
    enabled_leads = models.BooleanField(default=False, help_text='Is this followup rule enabled for SF Leads?')                          # 
    
    remote_user = models.ForeignKey('remote.RemoteUser', null=True, blank=True)
    objects = EmailRuleManager()
    
    def is_equal(self, other):
        """Return True if this rule is equal to some other rule, in the fields that matter when trying to save a rule"""
        if (self.type == other.type) and \
            self.owner_account == other.owner_account and \
            self.enabled_incoming == other.enabled_incoming and \
            self.enabled_outgoing == other.enabled_outgoing and \
            self.match() == other.match() and \
            self.actions() == other.actions():
            return True
        return False
    
    def _fuzzy_string_match(self, s1, s2):
        import difflib
        if s1 is None or s2 is None:
            return False
        sq = difflib.SequenceMatcher(None, s1.lower(), s2.lower())
        if sq.quick_ratio() > 0.8:
            return True
        return False
    
    def match(self):
        "Return the match criteria"
        return self.match_json and json.loads(self.match_json) or {}

    def actions(self):
        return self.actions_json and json.loads(self.actions_json) or {}
    
    def is_action_create_lead(self):
        "Return True if any of the actions are a create lead on Salesforce action"
        if self.type == EmailRule.TYPE_SF:
            if any([a['action'] == 'create' and a['type'] == 'lead' for a in self.actions()]):
                return True
        return False
    
    def is_action_create_contact(self):
        "Return True if any of the actions are a create contact on Salesforce action"
        if self.type == EmailRule.TYPE_SF:
            if any([a['action'] == 'create' and a['type'] == 'contact' for a in self.actions()]):
                return True
        return False
    
    def is_action_create_case(self):
        "Return True if any of the actions are a create contact on Salesforce action"
        if self.type == EmailRule.TYPE_SF:
            if any([a['action'] == 'create' and a['type'] == 'case' for a in self.actions()]):
                return True
        return False
    
    def is_action_followup(self):
        "Return True if any of the actions are a sync to followup server action"
        if self.type == EmailRule.TYPE_FOLLOWUP:
            if any([a['action'] == 'followup' for a in self.actions()]):
                return True
        return False
        
    def get_followup_campaign(self):
        if self.type == EmailRule.TYPE_FOLLOWUP:
            if any([a['action'] == 'followup' for a in self.actions()]):
                return a['campaign']
        return None
        
    def is_action_sync(self):
        "Return True if any of the actions are a sync to Salesforce action"
        if self.type == EmailRule.TYPE_SF:
            if any([a['action'] == 'sync' for a in self.actions()]):
                return True
        return False    
    
    def is_action_sync_opp(self):
        "Return True if any of the actions are a sync to Salesforce action"
        if self.type == EmailRule.TYPE_SF:
            if any([a['action'] == 'sync_opp' for a in self.actions()]):
                return True
        return False   
    
    def action_sync_task_type(self):
        """Return the custom Salesforce Task type for this rule, if it's a sync action
        None if not defined or not a sync action"""
        if self.type == EmailRule.TYPE_SF:
            for a in self.actions():
                if a['action'] == 'sync':
                    return a.get('task_type')
                    
    def is_action_ignore(self):
        "Return True if any of the actions are an ignore action"
        if self.type == EmailRule.TYPE_SF:
            if any([a['action'] == 'ignore' for a in self.actions()]):
                return True
        return False    
    
    def get_extra(self):
        return self.extra_json and json.loads(self.extra_json) or {}
    
 
    # TODO: Migrate over old "delete" email rules to the new scheme
    def procmail_matches(self, e):
        extra = self.get_extra()
        # Rule for matching whether an email is from someone in your team or not
        if ('from_team' in extra) and extra['from_team'] in [True, False]:
            return (Alias.objects.active().filter(email=e.from_address, account=self.owner_account).count() > 0) == extra['from_team']  
        # Rule for matching whether an email is from a specific address
        if self.from_address and e.from_address == self.from_address:
            return True  
        if e.from_address and self.from_address and self.subject_normalized:
            from_domain = e.from_address.split('@')[-1]
            domain = self.from_address.split('@')[-1]
            if from_domain == domain:
                if self._fuzzy_string_match(e.subject_normalized, self.subject_normalized):
                    return True
        return False
            
        
    def matches(self, e):
        """Return true if this EmailRule applies to this specific email.
        Uses these fields:
            matches_json
            enabled_incoming
            enabled_outgoing
        """
        if e.from_contact is None:                      # mails from assistant, etc. never match any rule
            return False
        
        m = self.match()['addr']
                
        if m['type'] == 'team' and m['data'] is None:   # matches any team email
            if self.enabled_incoming and not e.from_contact.alias:
                return True                             # mail is incoming to the team, from the outside world
            if self.enabled_outgoing and e.from_contact.alias and e.contacts.filter(alias=None, email2contact__type__in=Email2Contact.TYPES_RECIPIENT).count() > 0:
                return True                             # mail is outgoing from the team, to the outside world
        
        elif m['type'] == 'team' and m['data']:         # matches a specific alias
            if self.enabled_incoming and not e.from_contact.alias and e.contacts.filter(alias__id=m['data'], email2contact__type__in=Email2Contact.TYPES_RECIPIENT).count() > 0:
                return True                             # mail is incoming to the specific alias, from the outside world
            if self.enabled_outgoing and (e.from_contact.alias_id==m['data']) and e.contacts.filter(alias=None, email2contact__type__in=Email2Contact.TYPES_RECIPIENT).count() > 0:
                return True                             # mail is outgoing from the specific alias, to the outside world
        
        elif m['type'] == 'str' and m['data']:          # matches a specific email address (which may or may not be a team alias, but we treat it as a non-team alias
                                                        #  for the purposes of incoming/outgoing
            # For incoming mail, we also need to handle mail that is sent through a forwarding address -
            #  so allow either the From address or any of the To/Cc addresses to match the data, so long as the sender isn't a team member
            if self.enabled_incoming and not e.from_contact.alias and e.contacts.filter(email=m['data'], email2contact__type__in=Email2Contact.TYPES_ATTENDEES).count() > 0:
                return True                             # mail is incoming to the team, from this outside email address
            if self.enabled_outgoing and e.from_contact.alias and e.contacts.filter(email=m['data'], email2contact__type__in=Email2Contact.TYPES_RECIPIENT).count() > 0:
                return True                             # mail is outgoing from the team, to the outside world
              
        elif m['type'] == 'domain' and m['data']:       # matches a specific email domain 
            # For incoming mail, we also need to handle mail that is sent through a forwarding address -
            #  so allow either the From address or any of the To/Cc addresses to match the data, so long as the sender isn't a team member
            if self.enabled_incoming and not e.from_contact.alias and e.contacts.filter(Q(email__endswith='@'+m['data']) | Q(email__endswith='.'+m['data']), email2contact__type__in=Email2Contact.TYPES_ATTENDEES).count() > 0:
                return True                             # mail is incoming to the team, from this outside email address
            if self.enabled_outgoing and e.from_contact.alias and e.contacts.filter(Q(email__endswith='@'+m['data']) | Q(email__endswith='.'+m['data']), email2contact__type__in=Email2Contact.TYPES_RECIPIENT).count() > 0:
                return True                             # mail is outgoing from the team, to the outside world
 
        return False
  
    
    def find_email_contacts(self, e, content=None, viewing_alias=None):
        """Find the relevant contacts for this email that we will sync to some external source."""
        
        log("find_email_contacts")
                
        # The non-team contacts
        contacts = list(e.contacts.filter(alias=None))
        
        # Also, don't sync to email addresses that have the same domain as the account (unless gmail, etc.)
        exclude_domains = e.owner_account.get_extra().get('sync_exclude_domains', [])
        primary_domain = e.owner_account.get_default_admin().email.split('@')[-1]
        if not is_common_domain(primary_domain):
            exclude_domains.append(primary_domain)
        exclude_domains = [d.lower() for d in exclude_domains]
        contacts = [c for c in contacts if c.email.split('@')[-1].lower() not in exclude_domains]
            
        if (self.regexes.all().count() > 0) and (len(contacts) > 0):
            # Return only one contact, created by regex
            default_email = contacts[0].email
            extra = self.get_extra()
            extra_fields = extra.get('contact_extra_fields')
            contact, created = self._get_or_create_contact_from_regex(content, default_email, viewing_alias=viewing_alias, extra_fields=extra_fields)
            if contact:
                return [contact], True
            else:
                return None, False        # An email regex wasn't able to extract an email address -- ignore this mail completely
        else:
            return contacts, False
                    
    def _get_or_create_contact_from_regex(self, content, default_email, viewing_alias=None, extra_fields=None):
        """Get or create a contact for the viewing_alias based on regexes.
            content: the message body to which we apply regexes
            default_email: the email address of the contact to create, if we don't find it by regex
            viewing_alias: the owner of the contact, if it gets created
        """
                        
        viewing_alias = viewing_alias or self.owner_alias
        if not viewing_alias:
            raise NotImplementedError
    
        kwargs = {
            'viewing_alias':    viewing_alias,
            'status':           Contact.STATUS_ACTIVE,                          # For now, all contacts created this way are active
            'email':            default_email                                   # If we don't 
        }
        
        if extra_fields:
            # keys must be string, not unicode
            kwargs.update(dict([(str(key), value) for key, value in extra_fields.items()]))
                
        for r in self.regexes.all():
            m = re.search(r.regex, content)
            if m:                
                # keys must be string, not unicode, in order to use them as keyword args for **kwargs below
                kwargs.update(dict([(str(key), value) for key, value in m.groupdict().items()]))
            else:
                # If we can't extract the email field from this email, don't sync this email
                if '?P<email>' in r.regex:
                    return None, False
                 
        if kwargs['email']:
            contact, created = Contact.objects.get_or_create_smart(**kwargs)
            return contact, created
        else:
            return None, False
        
class EventResponse(models.Model):
    EVENT_RESPONSE_STATUS = (
        (EVENT_RESPONSE_STATUS_NONE,     'No response'),                        # Awaiting response from someone, but don't send notifications
        (EVENT_RESPONSE_STATUS_AWAITING, 'Awaiting response, send reminders'),  # Awaiting response from someone
        (EVENT_RESPONSE_STATUS_RECEIVED, 'Received response')                   # Received response 
    )
    
    EVENT_RESPONSE_TYPE = (
        (EVENT_RESPONSE_TYPE_YES, 'Event accepted'),          # Can do the event (for tentative events only)
        (EVENT_RESPONSE_TYPE_TENTATIVE, 'Event tentatively accepted'),
        (EVENT_RESPONSE_TYPE_NO, 'Event rejected'),           # Can't do the event (for tentative events only)
        (EVENT_RESPONSE_TYPE_UNKNOWN, "Couldn't parse response")
    )
    
    slug = models.SlugField(null=True)
    status = models.CharField(max_length=4, choices=EVENT_RESPONSE_STATUS, null=True)
    event = models.ForeignKey(Event, null=True)     # which event this is about
    child_event = models.ForeignKey(Event, null=True, related_name='eventresponse_thischild_set')  # which event = this response
    contact = models.ForeignKey(Contact, null=True) # who we're waiting for
    alias = models.ForeignKey(Alias, null=True)     # who we're waiting for (TODO: migrate and delete this)
    assistant = models.ForeignKey(Assistant, null=True) # who should send the reminders
    request_email = models.ForeignKey(Email, null=True, related_name='eventresponse_request')
    response_email = models.ForeignKey(Email, null=True, related_name='eventresponse_response') 
    last_request_sent = UTCDateTimeField(null=True)
    num_requests_sent = models.IntegerField(null=True, default=0)
    response_type = models.CharField(max_length=4, choices=EVENT_RESPONSE_TYPE, null=True)
    
    def is_active(self):
        if self.status != EVENT_RESPONSE_STATUS_AWAITING:
            return False
        if self.num_requests_sent >= HOWMANY_ASK_EVENT_RESPONSE:
            return False
        if not self.event:
            return True
        elif self.event.status in [EVENT_STATUS_TENTATIVE, EVENT_STATUS_SCHEDULED]:
            return self.event.end >= now()
        else:
            return False
            
    def should_send_request(self):
        if not datetime_older_than(self.last_request_sent, HOWLONG_ASK_EVENT_RESPONSE):
            return False
        return self.is_active()
    
    def __unicode__(self):
        return "<ER: %d, %s, %s, %s>" % (self.event_id, str(self.contact), str(self.status), str(self.response_type))
    

 
class File(Model):
    
    FILE_PERMS = (
        (FILE_PERMS_PRIVATE,    'Only From:, To: or Cc: people can see that the file exists'),
        (FILE_PERMS_PROTECTED,  'Anyone can see that the file exists, but non From/To/Cc people need to request permission'),
        (FILE_PERMS_PUBLIC,     'Anyone can see and download the file'),
    )
    
    slug = models.SlugField(null=True, unique=True)
    active = models.NullBooleanField(default=True)              # when file can't be found anymore, on IMAP or anywhere else, retire it
    name = TruncCharField(max_length=128, null=True)
    date = UTCDateTimeField(null=True)
    mimetype = TruncCharField(max_length=64, null=True)
    extension = TruncCharField(max_length=16, null=True)
    size = models.IntegerField(null=True, blank=True)
    md5 = models.CharField(max_length=40, null=True)
    sender_alias = models.ForeignKey(Alias, null=True)
    email = models.ForeignKey(Email, null=True)             # which email this file was attached to, if any  
    mimepath = models.CharField(max_length=256, null=True)  # which part of the message is this attachment (IMAP)
                                                            #  (example: 1.2.3)
    file_path = models.FilePathField(path=settings.FILE_STORE_PATH, null=True)  # Where the file is stored locally, if anywhere
    #                                                         #  (Relative to MEDIA_PATH)
    viewing_aliases = models.ManyToManyField(Alias, null=True, related_name="file_viewing_aliases")  # Anyone else who has permission for this file
    
    def retrieve(self):
        from lib.cloud import CloudFilesManager
        try:
            if not self.file_path and self.email and self.mimepath:
                # Not stored on Cloud Files, try to get it from IMAP 
                
                # Log into server, select folder, retrieve message                
                file_path = self.email.retrieve_message_part(self.mimepath, 
                    self.email.owner_account.get_cloud_files_container_name(), 'file_' + self.slug)
                if file_path is None:
                    log("File.retrieve(): no longer on IMAP")
                
                # Save the file path; the next if block will retrieve the file
                self.file_path = file_path
                self.save()
                  
            if self.file_path:
                # If file is stored on Cloud Files, get it from there directly
                if self.file_path.startswith('mosso:'):
                    if settings.MOSSO_FILES_ENABLED:
                        log("File.retrieve: retrieving from %s ..." % self.file_path)
                        return CloudFilesManager().get_file(self.file_path)
                    else:
                        log("File.retrieve(): MOSSO_FILES_ENABLED is false, can't retrieve %s" % self.file_path)
                        return None           
                else:
                    return None               # No other file_paths besides mosso: supported currently
                
            else:
                log("File.retrieve(): don't know where file is stored [2]")
#                self.active = False
#                self.save()
                return None
        except:
            log(traceback.format_exc())
            return None        
    
    @staticmethod
    def calc_file_stats(s):
        size = len(s)
        md5sum = hashlib.md5(s).hexdigest()
        return (size, md5sum)
            
    def update_file_stats(self, s):
        self.size, self.md5 = File.calc_file_stats(s)
        
    def store_local(self, s, size=None, md5sum=None):
        '''Store the byte string s as the contents of this file on Cloud Files.
        This is only needed for emails sent directly to the assistant.'''
                                
        from lib.cloud import CloudFilesManager
        if (not self.file_path):
            try:
                if (size is not None) and (md5sum is not None):
                    self.size, self.md5 = size, md5sum
                else:
                    self.update_file_stats(s)
                    
                if settings.MOSSO_FILES_ENABLED:
                    # store on Mosso
                    container_name = self.email.owner_account.get_cloud_files_container_name()
                    file_name = 'file_' + self.slug
                    self.file_path = CloudFilesManager().save_file(s, container_name, file_name)
                    self.save()
                else:
                    raise NotImplementedError
                
                return bool(self.file_path)
            except:
                log("Couldn't store %s locally", self.name, self.slug)
                return False
        else:                                           # Already stored locally, do nothing
            return True
    
    def delete(self, *args, **kwargs):
        "Override the usual delete method so that the Mosso file gets deleted too"
        self.delete_storage()
        return super(File, self).delete(*args, **kwargs) 
        
    def delete_storage(self):
        "If this file is stored locally or on Mosso, delete the data"
        from lib.cloud import CloudFilesManager
        if self.file_path:
            log(" -- File.delete_storage: Deleting from %s ..." % self.file_path)
            if self.file_path.startswith('mosso:'):
                if settings.MOSSO_FILES_ENABLED:
                    if CloudFilesManager().delete_file(self.file_path):
                        self.file_path = None
                        self.save()
                        log("... done")
                    else:
                        log("File.delete_storage(): can't delete %s from Mosso" % self.file_path)
                else:
                    log("File.delete_storage(): MOSSO_FILES_ENABLED is false, can't delete %s" % self.file_path)
            else:
                # Stored as file on the server
                fullpath = os.path.join(settings.FILE_STORE_PATH, self.file_path)
                try:
                    os.remove(fullpath)
                except:
                    log("File.delete_storage(): Can't delete %s" % fullpath)
                self.file_path = None
                self.save()
                log("... done")

    def get_mimetype_using_ext(self):
        if self.extension in ['doc', 'docx']:
            return 'application/msword'
        elif self.extension == 'ppt':
            return 'application/mspowerpoint'
        elif self.extension == 'xls':
            return 'application/msexcel'
        elif self.extension == 'pdf':
            return 'application/pdf'
        return self.mimetype
    
    def get_token(self, alias):
        n = now()
        # Try permanent tokens first
        tokens = FileToken.objects.filter(file=self, alias=alias,\
            active=True).exclude(type=FILETOKEN_TYPE_TEMP)
        if (tokens.count() > 0):
            return tokens[0]
        # Now try getting the temporary token (if any)
        tokens = FileToken.objects.filter(file=self, alias=alias,\
            active=True, type=FILETOKEN_TYPE_TEMP)
        if (tokens.count() > 0):
            return tokens[0]        
        return None
        
    def has_owner_perms(self, alias):
        '''Determine if the alias has 'owner' permissions on this file'''
        if alias.is_admin and alias.account == self.email.owner_account:
            # TODO: If alias is the admin of ANY account who has at least one person who can see the file (?)
            return True        
        token = self.get_token(alias)
        if token and token.is_owner_token():
            return True
        return False
    
    def has_download_perms(self, alias):
        # Can this alias download this file without needing to request permission?
        if alias.is_admin:
            return True
        if self.email.contacts.filter(alias=alias).count() > 0:
            return True
        if bool(self.get_token(alias)):
            return True
        return False
                       
    def get_owner_alias(self, alias):
        '''Get the "owner" of this file, relative to a particular alias
        (i.e., the one we ask for permissions)'''
        # TODO: Make this take advantage of the new FileToken stuff
        
        account = alias.account
        if not account:
            raise Exception("get_owner_alias: account must not be None")
        if self.email.from_contact and self.email.from_contact.alias and \
            (self.email.from_contact.alias.account == account) and (self.email.from_contact.alias.status == ALIAS_STATUS_ACTIVE):
            owner_alias = self.email.from_contact.alias
        elif self.email.crawled_alias and (self.email.crawled_alias.account == account) and (self.email.crawled_alias.status == ALIAS_STATUS_ACTIVE):
            owner_alias = self.email.crawled_alias
        elif self.viewing_aliases.all():
            owner_alias = self.viewing_aliases.all()[0]
        else:
            # We give up, just ask an admin
            owner_alias = Alias.objects.active().filter(account=account, is_admin=True)[0]
        return owner_alias
          
    def __unicode__(self):
        return u'%s: %s' % (self.slug, str_or_none(self.name))
    def __str__(self):
        return '%s: %s' % (self.slug, str_or_none(self.name))    
    

class FileTokenManager(Manager):
    # def get_or_create_token(alias, file):

    def create_token(self, alias, file, type=FILETOKEN_TYPE_TEMP):
        date_start = now()
        if type == FILETOKEN_TYPE_TEMP:
            date_end = date_start + HOWLONG_FILE_PERMISSION
        else:
            date_end = None
        token = FileToken(slug=create_slug(length=20),\
            type = type,
            date_start = date_start,
            date_end = date_end,
            active = True,
            file = file,
            alias = alias)
        token.save()
        return token
    
    def get_or_create_token(self, alias, file, type=FILETOKEN_TYPE_TEMP):
        queryset = FileToken.objects.filter(alias=alias, file=file, type=type, active=True)
        if queryset.count() > 0:
            return queryset[0], False
        else:
            token = self.create_token(alias, file, type=type)
            return token, True
    
    def inactivate_old_tokens(self):
        for ft in FileToken.objects.exclude(date_end=None).filter(date_end__lt=now(), active=True):
            ft.active = False
            ft.save()
    
    def delete_old_tokens(self):
        FileToken.objects.exclude(date_end=None).filter(date_end__lt=now()).delete()  
    
class FileToken(Model):
        
    FILETOKEN_TYPE_CHOICES = (
        (FILETOKEN_TYPE_FROM, 'This person sent the file'),          
        (FILETOKEN_TYPE_CRAWLED, "File crawled from this person's email box"),
        (FILETOKEN_TYPE_TOCC, 'File sent to: or cc: this person'),
        (FILETOKEN_TYPE_OTHER, 'Anyone else who can always see this file'),
#        (FILETOKEN_TYPE_GRANTED, 'A limited-time file token granted by the owner'),
        (FILETOKEN_TYPE_TEMP, 'A limited-time file token')
    )
    
    slug = models.SlugField(unique=True, max_length=20)
    active = models.NullBooleanField(default=True, null=True)
    type = models.CharField(max_length=4, default=FILETOKEN_TYPE_TEMP, choices=FILETOKEN_TYPE_CHOICES)
    date_start = UTCDateTimeField(null=True)
    date_end   = UTCDateTimeField(null=True)
    file = models.ForeignKey(File)
    alias = models.ForeignKey(Alias)
     
    objects = FileTokenManager()
    
    def get_url(self):
        return '/dash/%s/retrieve_file?token=%s' % (self.alias.account.base_url, self.slug) 
    
    def is_owner_token(self):
        return self.type in [FILETOKEN_TYPE_FROM, FILETOKEN_TYPE_CRAWLED]
    
    def __unicode__(self):
        return "%s: %s %s" % (self.type, self.alias.email, self.file.name)
    

class BucketManager(Manager):
    
    def get_or_create_for_alias(self, alias, name):
        name = ''.join([c for c in name.strip().lower() if c.isalnum() or c == ' '])
        name = ' '.join(name.split())     # collapse multiple spaces into single space
        b, b_created = Bucket.objects.get_or_create(name=name, account=alias.account)
        if b_created:
            b.slug = create_slug(length=10)
            tag, t_created = Tag.objects.get_or_create(text=name.replace(' ', '_'), type=TAG_TYPE_BUCKET)
            b.tag = tag
            b.owner_alias = alias
            b.save()
        return b, b_created
    
class Bucket(Model):
    
    DOMAIN          =   'bDOM'
    FILENAME        =   'bFIL'
    FROM_NAMEADDR   =   'bFNA'
    TOCC_NAMEADDR   =   'bTNA'
    SUBJECT         =   'bSUB'
    BODY            =   'bBOD'
    TAG             =   'bTAG'
    
    version         =   models.IntegerField(default=BUCKET_VERSION)
    sub_version     =   models.IntegerField(default=BUCKET_SUBVERSION)
    slug            =   models.SlugField(null=True)
    last_updated    =   UTCDateTimeField(default=now)
    name            =   TruncCharField(max_length=30)
    tag             =   models.ForeignKey(Tag, null=True)
    account         =   models.ForeignKey(Account, null=True)
    owner_alias     =   models.ForeignKey(Alias, null=True, blank=True)
    contacts        =   models.ManyToManyField(Contact, related_name='contact_buckets')
    num_events      =   models.FloatField(default=0)
    num_mails_to    =   models.FloatField(default=0)
    num_mails_from  =   models.FloatField(default=0)
    num_files_to    =   models.FloatField(default=0)
    num_files_from  =   models.FloatField(default=0)
    num_files_name  =   models.FloatField(default=0)
    num_from_nameaddr_words     =   models.FloatField(default=0)
    num_tocc_nameaddr_words     =   models.FloatField(default=0)
    num_subject_words           =   models.FloatField(default=0)
    num_body_words              =   models.FloatField(default=0)
    num_tags        =   models.FloatField(default=0)
    score           =   models.FloatField(default=0)
    type            =   models.CharField(max_length=4, null=True)
    
    objects         =   BucketManager()
    
    def update_score(self):
        "Calculate a new score, but don't actually save the bucket to the DB"
        mail_score = (min(self.num_events, 10) / 10.0) * (self.num_mails_to + self.num_mails_from * (min(self.num_mails_to, 10) / 10.0))
        file_score = (self.num_files_to + self.num_files_from * (min(self.num_files_to, 3) / 3.0)) * 3.0 + self.num_files_name * 1.3
        # word_score = self.num_subject_words * 1.5 + self.num_body_words * 0.9 # + self.num_from_nameaddr_words * 3 + self.num_tocc_nameaddr_words * 2
        word_score = 0              # For now ...
        result = mail_score + file_score + word_score
        self.score = result
        self.last_updated = now()
        return result
    
    def merge_from(self, other_bucket):
        if (self == other_bucket):
            return
        for c in other_bucket.contacts.all():
            self.contacts.add(c)
        other_bucket.delete()
        
    def __cmp__(self, other):
        c = cmp(self.score, other.score)
        if c:
            return -c                           # Higher scores come first
        else:
            return cmp(self.name, other.name)   # To break a tie, alphabetically earlier names come first
        
    def __unicode__(self):
        return u"<%s: %f>" % (self.name, self.score)  
#       return u"<%s: %s   | %f %f %f %f | %f>" % (self.name, self.score, self.num_mails_to, self.num_mails_from, self.num_files_to, self.num_files_from, \
#                                                  self.num_files_name)
                     

