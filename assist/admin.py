"Admin views for assist."

from models import *
####from tasks import *
from django.utils.translation import ugettext as _
from crawl.admin import *

class EmailAdmin(admin.ModelAdmin):
    list_display = ('id','message_id','in_reply_to','references','date_sent','from_name','from_address','to_name','to_address','subject')
    ordering = ('-id',)
    search_fields = ['message_id','in_reply_to','references','message','from_name','from_address','to_name','to_address','subject']
    inlines = [] 

    
class AliasAdmin(admin.ModelAdmin):
            
    list_display = ('id','email','server_server','server_enabled','server_smtp_server','server_smtp_enabled','account','server_enabled','last_login_date','date_created','agent_created')
    ordering = ('-last_login_date','-id',)
    search_fields = ['id','slug','email','account__base_url']
    raw_id_fields = ('user', 'account_user', 'account', 'contact')
    exclude = ('session_key', 'server_password_aes', 'user', 'account_user', 'contact', 'account', 'storage_used_mb_files', 'twitter_enabled', 'twitter_username', 'server_smtp_password_aes', 'password_hash', 'reset_password_slug_hash', 'reset_password_slug_date', 'parse_last_date', 'slug', 'slug_ics', 'date_last_accessed_ics', 'last_unused_account_reminder_date')
    inlines = [NotedTaskMetaInline] 
#    
#    def render_change_form(self, request, context, **kwargs):
#        """
#        The 'render change form' admin view for this model -- 
#        adapted from django.contrib.admin
#         - adds the object itself to the context
#        """
#        
#        obj = kwargs.get('obj')
#        context['obj'] = obj            # the actual alias
#        from crawl.models import NotedTaskMeta
#        context['tasks'] = NotedTaskMeta.objects.filter(alias=obj, status__in=[NotedTaskMeta.STATUS_STARTED, NotedTaskMeta.STATUS_FINISHED]).order_by('date_updated')
#        
#        return super(AliasAdmin, self).render_change_form(request, context, **kwargs) 
#        

class AliasInline(admin.TabularInline):
    model = Alias
    extra = 0

class PaymentInline(admin.TabularInline):
    model = Payment
    extra = 0

class ContactInline(admin.TabularInline):
    model = Contact
    extra = 0

class AssistantInline(admin.TabularInline):
    model = Assistant
    extra = 0

class ReceiptInline(admin.TabularInline):
    model = Receipt
    extra = 0

class ContactAdmin(admin.ModelAdmin):
    list_display = ('id','email','alias','owner_alias','source','status','created_date')
    ordering = ('-id',)
    search_fields = ['id','email','alias','owner_alias','source','status','created_date']

class AccountAdmin(admin.ModelAdmin):
    list_display = ('id','base_url','service','promo','status','name','date_created','anon_storage_used_mb_files','anon_storage_used_mb_msgs')
    ordering = ('-id',)
    search_fields = ['id','base_url','alias__email']
    #inlines = [,] # PaymentInline, AliasInline, AssistantInline,ContactInline,AliasInline
    exclude = ('site','extra_json','slug','public_domains')

    def delete_view(self, request, object_id, extra_context=None):
        """
        The 'delete' admin view for this model -- 
        adapted from django.contrib.admin.options.delete_view
         - marks the account as ACCOUNT_STATUS_DELETE_PENDING, to be deleted later by a daily cron job
           doesn't actually delete the account (that would be slow!)
        """
        
        from django.contrib.admin.util import unquote, flatten_fieldsets, get_deleted_objects, model_ngettext, model_format_dict
        from django.core.exceptions import PermissionDenied
        from django import forms, template

        opts = self.model._meta
        app_label = opts.app_label

        try:
            obj = self.queryset(request).get(pk=unquote(object_id))
        except self.model.DoesNotExist:
            # Don't raise Http404 just yet, because we haven't checked
            # permissions yet. We don't want an unauthenticated user to be able
            # to determine whether a given object exists.
            obj = None

        if not self.has_delete_permission(request, obj):
            raise PermissionDenied

        if obj is None:
            raise Http404(_('%(name)s object with primary key %(key)r does not exist.') % {'name': force_unicode(opts.verbose_name), 'key': escape(object_id)})

        if request.POST: # The user has already confirmed the deletion.
            obj_display = force_unicode(obj)
            self.log_deletion(request, obj, obj_display)
            
            obj.status = ACCOUNT_STATUS_DELETE_PENDING
            obj.cancel_subscription()
            obj.save()
            
            self.message_user(request, _('We have marked the %(name)s "%(obj)s for deletion. It will be deleted within 24 hours.') % {'name': force_unicode(opts.verbose_name), 'obj': force_unicode(obj_display)})

            if not self.has_change_permission(request, None):
                return HttpResponseRedirect("../../../../")
            return HttpResponseRedirect("../../")
                
        # Find the Paypal payer email, this is convenient so they can use it to cancel the sub on Paypal 
        try:
            paypal_payer_email = Payment.objects.filter(account=obj, type=Payment.TYPE_PAYPAL).order_by('-date')[0].paypal.payer_email
        except:
            paypal_payer_email = None
            
        context = {
            "title": _("Are you sure?"),
            "object_name": force_unicode(opts.verbose_name),
            "object": obj,
            "opts": opts,
            "root_path": self.admin_site.root_path,
            "app_label": app_label,
            "is_deleting": obj.status == ACCOUNT_STATUS_DELETE_PENDING,
            "paypal_payer_email": paypal_payer_email
        }
        context.update(extra_context or {})
        context_instance = template.RequestContext(request, current_app=self.admin_site.name)
        return render_to_response(self.delete_confirmation_template or [
            "admin/%s/%s/delete_confirmation.html" % (app_label, opts.object_name.lower()),
            "admin/%s/delete_confirmation.html" % app_label,
            "admin/delete_confirmation.html"
        ], context, context_instance=context_instance)
    
    actions = None          # Don't allow the default "delete" action for Accounts
    

class AccountUserAdmin(admin.ModelAdmin):
    list_display = ('id','name','slug',)
    ordering = ('-id',)
    search_fields = ['id','slug']
    #inlines = [AssistantInline]
    
class EmailInline(admin.TabularInline):
    model = Email
    extra = 3

class EventAdmin(admin.ModelAdmin):
    list_display = ('id','start','end','timezone','description','created_on')
    ordering = ('-id',)
    search_fields = ['id','description']
    inlines = []
    exclude = ('original_email','parent','ref_contacts','formmail_contacts')
 
class EmailRuleAdmin(admin.ModelAdmin):
    list_display = ('id','owner_account', 'match_json', 'actions_json')
    ordering = ('owner_account__base_url',)
    search_fields = ['id','owner_account__base_url']
    
class EmailRegexAdmin(admin.ModelAdmin):
    list_display = ('id','owner_account', 'description','regex')
    ordering = ('-id',)
    search_fields = ['id','owner_account__base_url','description','regex']
    
#class TagInline(admin.TabularInline):
#    model = EventRelation
#    extra = 3
    
class TagAdmin(admin.ModelAdmin):
    list_display = ('id','text','type')
    ordering = ('-id',)
    search_fields = ['id','text']
    # inlines = [TagInline] 


admin.site.register(Account,AccountAdmin)
#admin.site.register(AccountUser,AccountUserAdmin)
admin.site.register(Event,EventAdmin)
admin.site.register(Alias,AliasAdmin)
admin.site.register(Email,EmailAdmin)
#admin.site.register(Tag)
# admin.site.register(Assistant)
admin.site.register(Promo)
admin.site.register(Service)
admin.site.register(File)
admin.site.register(Payment)
#admin.site.register(Bucket)
#admin.site.register(BucketRule)
#admin.site.register(BucketClause)

#admin.site.register(TagRelation)
#admin.site.register(TagRelationNotify)

admin.site.register(Receipt,ReceiptAdmin)
admin.site.register(EmailRule,EmailRuleAdmin)
admin.site.register(EmailRegex,EmailRegexAdmin)

# admin.site.register(Event,EventAdmin)

admin.site.register(Contact,ContactAdmin)