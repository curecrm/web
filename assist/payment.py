'''
Handlers for callbacks from the payment gateways:
New payment server, and (legacy) Paypal IPN callbacks
'''

from django.db import models
from django.db import connection
from django.db.models import *
from django.conf import settings
from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from paypal.standard.signals import payment_was_successful, payment_was_flagged

from django.contrib import admin
from django.template import Context, loader
from django.utils.translation import ugettext as _
import hashlib, random, email
import traceback, pdb
import json, pprint, copy, urllib
from assist import mail
from utils import *
from datetime import *
from models import *
from constants import *


def handle_payment_notify(data):
    '''
    Handler for callbacks from the payment server.
    
    This gets called when a customer's credit card is charged (or the card fails to get charged).
    '''
        
    try:            
        log("handle_payment_notify start")

        customer = data['customer']
        m = re.search(r'\bAccount\.(?P<account_id>\d+)$', customer)
        account = Account.objects.get(id=m.group('account_id'))
        
        # Create a payment object                               
        payment = Payment()
        payment.status = Payment.STATUS_SUCCESS if data['success'] else Payment.STATUS_FAILED
        payment.failure_reason = data.get('failure_reason')
        payment.type = Payment.TYPE_GATEWAY
        payment.account = account
        payment.paypal = None                       # not a PayPal payment
        payment.amount = data.get('amount')
        payment.date = now()            
        payment.receipt_json = json.dumps(data['receipt']) if 'receipt' in data else None
        payment.remote_billing_event_id = data.get('billing_event_id')
        payment.remote_subscription_id = data.get('subscription_id')
        payment.save()

        if payment.status == Payment.STATUS_SUCCESS:
            # Extend the account's service
            account.trial_period = False
                        
            # If necessary, modify the service level and the # of users
            service_item = payment.service_item()
            if service_item is not None:
                account.service = Service.objects.get(name=service_item['service'])
                
            extra_users_item = payment.extra_users_item()
            if account.service.max_users_flat_rate is None:
                # unlimited users
                account.max_users = None
            elif extra_users_item is not None:
                account.max_users = account.service.max_users_flat_rate + extra_users_item['quantity']
            
            account.date_expires = now() + paypal_timedelta('%d %s' % (account.service.num_dmy, account.service.dmy))
            account.status = ACCOUNT_STATUS_ACTIVE
            account.save()
            log("Received payment, extended service for account %s" % account.base_url)                
            
            # TODO: Send an invoice
            #send_payment(payment, next=account.date_expires, signup=signup)
        else:
            log("Received failed payment %d for account %s" % (payment.pk, account.base_url))                
            
            if payment.failure_reason == Payment.FAILURE_REASON_EXPIRED:
                alias = account.get_default_admin()
                extra = {'amount': '$%.2f' % float(payment.amount) if payment.amount else ''}
                
                from network.ajax_admin import admin_followup
                admin_followup(alias.contact, settings.FOLLOWUP_CAMPAIGN_CREDIT_CARD_EXPIRED, source="Calenvy Authorize.net card expired", extra=extra)
                            
        # Return success regardless of whether or not the payment itself is a success or failure
        return {'success': True}
    
    except:
        log("handle_payment_notify failed")
        log(traceback.format_exc())
        return {'success': False}


def paypal_parse_item_name(item_name):
    '''
    Parse the item_name from an ipn_obj
    Returns a service and a max # of users for the given price
    
    Valid item_name examples:
        "Calenvy Assistant Account"                    (old format, or unlimited # of users)
        "Calenvy Assistant Account (3 users)"          (new format, starting 2/2/10)  
    '''
    
    log("service_from_ipn_obj: parsing", item_name)
    m = re.match(r"\w+ (?P<service>\w+) Account( \((?P<users>\d+) \w+\))?", item_name)
    if m:
        try:
            service = m.group('service') and Service.objects.get(name__iexact=m.group('service'))
        except:
            service = None
        
        try:
            users = int(m.group('users'))
        except:
            users = None
            
        return service, users
    return None, None
            

def paypal_ipn_success(sender, **kwargs):
    """Handler for legacy Paypal IPN success messages.
    This is registered with the Paypal code in urls.py"""
    
    silent = kwargs.get('silent', False)

    log("paypal_ipn_success")
    ipn_obj = sender
    ipn_obj.save()
    log("saved ipn_obj: pk=", ipn_obj.pk)
    
    try:
        log("[a]")
        
        try:
            alias_slug, service_slug, promo_code, flags = ipn_obj.custom.split('_')
        except:
            # Older Paypal subscriptions don't have the flags
            alias_slug, service_slug, promo_code = ipn_obj.custom.split('_')
            flags = ''
            
        # Was a 10% fee added on at subscription time?
        paypal_fee = ('F' in flags)

        log("alias_slug:", alias_slug)
        log("service slug:", service_slug)
        log("promo code:", promo_code)
        alias = Alias.objects.get(slug=alias_slug)
        account = alias.account
        if account is None:
            raise Exception("paypal_ipn_success: alias %s account is None" % alias.slug)
        service = Service.objects.get(Q(slug=service_slug) | Q(extra_slugs__contains=service_slug))
        if promo_code:
            promo = Promo.objects.get(code__iexact=promo_code)
        else:
            promo = None
            
        log("alias:", alias)
        log("service:", service)
        log("promo:", promo)
                
        txn = ipn_obj.txn_type
        
        if txn == 'subscr_signup':
            
            log("Looking for service", ipn_obj.item_name)
            service, max_users = paypal_parse_item_name(ipn_obj.item_name)
                
            # Don't use modify=True here, because we're setting the date_expires below
            account.start_service(service, promo=None, modify=False, max_users=max_users)
                        
            log("[b]")
            
            # Make a Receipt object
            # TODO: Send a receipt email (may not be necessary)
            receipt = Receipt()
            receipt.gateway = 'PP'
            receipt.account = alias.account
            receipt.alias = alias
            receipt.service = service
            receipt.invoice_num = ipn_obj.invoice
            # leave Promo alone for the time being
            receipt.date = now()
            receipt.paypal = ipn_obj
            receipt.save()
            
            r = Receipt.objects.filter(account=account).order_by('-date')
            if r.count() > 0:
                account.date_expires = r[0].next_payment_date()
            else:
                account.date_expires = now() + paypal_timedelta('%d %s' % (service.num_dmy, service.dmy))
                
            log("[c]")
            payment = Payment()
            payment.type = Payment.TYPE_PAYPAL
            payment.account = alias.account
            payment.alias = alias
            payment.paypal = ipn_obj
            payment.amount = ipn_obj.mc_gross
            payment.date = now()
            # payment.save()                    # Don't save this payment, since there's no $ for this one --
                                                #  it's just to send an email
                                                
            #    account.service = service            # Don't use this for now!
            account.paypal_invoice = ipn_obj.invoice
            account.set_initial_expiration_date(service, promo=promo)
            account.trial_period = False
            account.status = ACCOUNT_STATUS_ACTIVE
            account.save()
            log("Received subscription notice, extended service for alias %s" % alias.slug)     
            
            log("account.date_expires:", account.date_expires)
            if not silent:
                send_payment(payment, next=account.date_expires, signup=True)
                   
            log("[d]")

        elif txn == 'subscr_payment':
            log("[e]")
                            
            log("Looking for service", ipn_obj.item_name)
            new_service, new_max_users = paypal_parse_item_name(ipn_obj.item_name)
            if new_service:
                # Modifying an existing service
                if (account.service != new_service) or (new_max_users and (account.max_users != new_max_users)):  
                    account.start_service(new_service, promo=None, modify=True, max_users=new_max_users)
                 
            log("[e2:]", ipn_obj)
            # Make a Payment object
            
            old_payments = Payment.objects.filter(account=alias.account).order_by('-date')
            if old_payments:
                prev = old_payments[0].date
            else:
                prev = None
            
            payment = Payment()
            payment.type = Payment.TYPE_PAYPAL
            payment.account = alias.account
            payment.alias = alias
            payment.paypal = ipn_obj
            payment.amount = ipn_obj.mc_gross
            payment.date = now()
            
            # Create a receipt structure, for the invoice emails and for the display of past payments
            items = [
                {
                    'label':       'service',
                    'service':     account.service.name,
                    'price':       account.service.price_per_num_dmy
                }
            ]
            
            if (account.service.max_users_flat_rate is not None) and (new_max_users > account.service.max_users_flat_rate):
                items.append({
                    'label':       'extra_users',
                    'quantity':    new_max_users - account.service.max_users_flat_rate,
                    'price_each':  account.service.price_per_extra_user,
                    'price':       account.service.price_per_extra_user * (new_max_users - account.service.max_users_flat_rate)                     
                })
            
            receipt = {
                'items': items
            }
                        
            if paypal_fee:
                receipt['fee'] = {
                    'label':       'paypal',
                    'percent':     service.percent_paypal_fee 
                }
            
            if promo:
                receipt['promo'] = {
                    'label':        'promo',
                    'percent':      promo.percent_discount
                }
                
            payment.receipt_json = json.dumps(receipt)
            payment.save()
            
            #    account.service = service            # Don't use this for now!
            account.trial_period = False
            r = Receipt.objects.filter(account=account).order_by('-date')
            if r.count() > 0:
                account.date_expires = r[0].next_payment_date()
            else:
                account.date_expires = now() + paypal_timedelta('%d %s' % (service.num_dmy, service.dmy))
            account.paypal_invoice = ipn_obj.invoice
            account.status = ACCOUNT_STATUS_ACTIVE
            account.save()
            log("Received payment, extended service for alias %s" % alias.slug)                
            
            if not silent:
                send_payment(payment, prev=prev, next=account.date_expires)
            
            log("[f]")
            
        # Do nothing in these cases -- the service will expire on its own, eventually
        elif txn == 'subscr_cancel':
            log("[f1]: subscr_cancel")
            
            # No active subscription
            account.paypal_invoice = None
            account.save()
            
            for r in Receipt.objects.filter(paypal__custom=ipn_obj.custom):
                log("Marking Receipt %d as canceled" % r.pk)
                r.status = Receipt.STATUS_CANCELED
                r.save()
                            
            try:  
                from network.ajax_admin import admin_ping, admin_followup
                t = loader.get_template('newsfeed_payment_cancel.txt')           
                c = Context({\
                    'alias':                    alias,
                    'account':                  alias.account,
                    'date':                     now().strftime('%m/%d/%Y'),
                    'paypal':                   ipn_obj
                }) 
                content = t.render(c)   
                admin_ping(alias.contact, content, categories=['payments', 'customers', 'cancellations'])
                admin_followup(alias.contact, settings.FOLLOWUP_CAMPAIGN_CANCELLATION, source="Calenvy PayPal cancellation")

            except:
                log("send_registration: couldn't create admin ping for alias", alias, traceback.format_exc())
            
        elif txn == 'subscr_failed':
            pass
        elif txn == 'subscr_eot':
            log("[f2]: subscr_eot")
            for r in Receipt.objects.filter(paypal__custom=ipn_obj.custom):
                log("Marking Receipt %d as EOT" % r.pk)
                r.status = Receipt.STATUS_EOT
                r.save()
        elif txn == 'subscr_modify':            
            log("[g]: subscr_modify")
            account.trial_period = False
            account.date_expires = now() + paypal_timedelta('%d %s' % (service.num_dmy, service.dmy))
            account.status = ACCOUNT_STATUS_ACTIVE
            account.paypal_invoice = ipn_obj.invoice

            # Modify the service level. 
            # UGLY HACK! Can't use the service part of the custom field, because this is not modified
            # by a subscription change. Parse the item_name instead.
            # TODO: Look up the 'invoice' field in an Invoice model table. (Invoice # isn't modified either.)
            
            old_payments = Payment.objects.filter(account=alias.account).order_by('-date')
            if old_payments:
                prev = old_payments[0].date
            else:
                prev = None
                
            log("Looking for service", ipn_obj.item_name)
            new_service, new_max_users = paypal_parse_item_name(ipn_obj.item_name)
            old_service, old_max_users = account.service, account.max_users
            if new_service:
                if (account.service != new_service) or (new_max_users and (account.max_users != new_max_users)):  
                    account.start_service(new_service, promo=None, max_users=new_max_users, modify=True)
            account.save()            
            
            if new_service:
                log("[h]")
                payment = Payment()                 # Fake Payment object, no $ for this one --
                payment.type = Payment.TYPE_PAYPAL  # it's just to send an email
                payment.account = alias.account
                payment.alias = alias
                payment.paypal = ipn_obj
                payment.amount = ipn_obj.mc_gross
                payment.date = now()
                # payment.save()                    # Don't save this payment, since there's no $ for this one --
                                                    #  it's just to send an email
                if not silent:
                    send_payment(payment, prev=prev, next=account.date_expires, signup=True, modify=True, old_service=old_service, old_max_users=old_max_users)
      
        
    except:
        log(traceback.format_exc())
        log("paypal_ipn_success: invalid data from IPN")
        
    
#    log("ipn_obj:\n", pprint.pformat(ipn_obj))
#    log("kwargs:", pprint.pformat(kwargs))
#    # Undertake some action depending upon `ipn_obj`.
#    if ipn_obj.cust == "Upgrade all users!":
#        Users.objects.update(paid=True)  

def paypal_ipn_flagged(sender, **kwargs):
    '''This gets called when the payment is still successful, but it
    still has some issues. See paypal.standard.models for explanation'''
    log("paypal_ipn_flagged")
    ipn_obj = sender
    ipn_obj.save()
    log("ipn_obj:\n", pprint.pformat(ipn_obj))
    log("kwargs:", pprint.pformat(kwargs))
        
    try:
        try:
            alias_slug, service_slug, promo_code, flags = ipn_obj.custom.split('_')
        except:
            # Older Paypal subscriptions don't have the flags
            alias_slug, service_slug, promo_code = ipn_obj.custom.split('_')
            flags = ''
            
        log("alias_slug:", alias_slug)
        log("service slug:", service_slug)
        log("promo code:", promo_code)
        alias = Alias.objects.get(slug=alias_slug)

        if ipn_obj.payment_status in ['Denied', 'Expired', 'Failed', 'Pending']:
            from network.ajax_admin import admin_followup
            admin_followup(alias.contact, settings.FOLLOWUP_CAMPAIGN_PAYMENT_FAILED, source="Calenvy PayPal payment failed")

    except:
        log(traceback.format_exc())
        log("paypal_ipn_flagged: invalid data from IPN")
        
   

#    # Undertake some action depending upon `ipn_obj`.
#    if ipn_obj.cust == "Upgrade all users!":
#        Users.objects.update(paid=True)  


def paypal_generate_url(alias, service, promo, return_url, cancel_url, site=None):
    '''
    Helper function used by the billing page.
    Generates a URL to take the user to Paypal with the correct information filled in.
    '''
        
    log("paypal_generate_url start: ", alias, service, promo, return_url, cancel_url)
              
    def pricestr(price):    return '%.2f' % price
    
    # Paypal not needed for free services (duh)
    if service.is_free(promo=promo):
        return None
    
    if not site:
        site = Site.objects.get_current()
    
    # Is this even necessary anymore?
    disable_trial = True                    # For now, Paypal payments always end the trial period. [eg 5/12/2010]
    
#    if promo and promo.disable_service_trial: 
#        disable_trial = True  
#             
    custom = "%s_%s_%s_%s" % (alias.slug, service.slug, promo.code if promo else '', 'F' if service.percent_paypal_fee else '')
    account = alias and alias.account
    
    # If there's an existing Paypal subscription, then we're modifying it
    modify = (account.paypal_invoice is not None)
    
    total_price_per_num_dmy = service.calc_discounted_price(account=account, promo=promo)
    
    # Tack on the 10% (or whatever) fee for Paypal
    if service.percent_paypal_fee:
        total_price_per_num_dmy *= (1 + (service.percent_paypal_fee / 100.0))
    
    # Format a string representing the item name.
    #  Be careful making any changes here -- parse_item_name depends on format of this string!
    if service.max_users_flat_rate is None:
        # unlimited users
        item_name = "%s %s Account" % (site.name, service.name)
    else:
        max_users = max(service.max_users_flat_rate, account.num_active_aliases())
        item_name = "%s %s Account (%d user%s)" % (site.name, service.name, max_users, 's' if max_users > 1 else '')
            
    paypal_dict = {
        "business": settings.PAYPAL_RECEIVER_EMAIL,
        "currency_code": "USD",
        "item_name": item_name,
        "custom": custom,
        "invoice": create_slug(length=10),
        #"notify_url": 'http://calenvy.dyndns.biz:3636/%s' % (domain, settings.PAYPAL_NOTIFY_URL),
        "notify_url": 'http://%s%s' % (Site.objects.get_current().domain, settings.PAYPAL_NOTIFY_URL),
        "return": return_url,
        "no_shipping": "1",
        "no_note": "1",         # don't display feedback form
        "auto_return": "on",
        "rm": "2", #2-everything gets posted...
        "cbt": "Return to %s for more information" % settings.PRODUCT_NAME,
        "cancel_return": cancel_url,
        "sra":  "1"            # reattempt payment on failure 
    }
    
    if service.recurring:
        paypal_dict.update({\
            'cmd':          "_xclick-subscriptions",
            'src':          "1",
            'modify':       "1" if modify else "0"  
        })   
        
        if promo and promo.trial_dmy:
            trial_num_dmy, trial_dmy = promo.trial_num_dmy, promo.trial_dmy
        else:
            trial_num_dmy, trial_dmy = service.trial_num_dmy, service.trial_dmy
            
        period_id = 1
        if (trial_dmy is not None) and (not disable_trial): 
            # Free trial period
            
            paypal_dict.update({\
                "a%d" % period_id:   str(0),
                "p%d" % period_id:   str(trial_num_dmy),
                "t%d" % period_id:   str(trial_dmy)
            })
            period_id += 1
            
        period_id = 3                       # Final, recurring price is always #3 in Paypal, no matter what
        
        paypal_dict.update({\
            "a%d" % period_id:      pricestr(total_price_per_num_dmy or 0),
            "p%d" % period_id:      str(service.num_dmy),
            "t%d" % period_id:      str(service.dmy)
        })                                            
    else:
        paypal_dict['cmd'] = "_xclick"
        
        if promo is not None:
            paypal_dict['amount'] = pricestr(total_promo_price_per_num_dmy or 0)
        else:
            paypal_dict['amount'] = pricestr(total_price_per_num_dmy or 0)

    log('[4]')

    # Form the Paypal GET URL
    paypal_url = settings.PAYPAL_URL 
    paypal_url += '?' + '&'.join(["%s=%s" % (key, urllib.quote_plus(value)) for key, value in paypal_dict.items()])

    return paypal_url
    
    

def send_payment(payment, next=None, prev=None, max_blog_posts=10, signup=False, modify=False, old_service=None, old_max_users=None): 
    """Send an invoice to the alias after an (automatic) payment is made.
    Used for both payment-gateway (Authorize.net) and Paypal payments."""
    
    from crawl.procmail import procmail_quickbox
    from assist.mail import send_email
    from basic.blog.models import Post

    log("send_payment: ", payment.alias)
    alias = payment.alias
    account = payment.account
    assistant = payment.account.get_assistant()      
    noted = {'message_type': EMAIL_TYPE_NOTED,\
        'message_subtype': EMAIL_SUBTYPE_NOTED_PAYMENT_RECEIPT,
        # 'owner_account': assistant and assistant.owner_account
    }\
    
    if (modify and not signup):
        raise Exception()
    
    if prev:
        blog_posts = Post.objects.published().filter(created__gte=prev).order_by('-created')[:max_blog_posts]
    else:
        blog_posts = Post.objects.published().order_by('-created')[:max_blog_posts]
    
    site = payment.alias.get_site()
    c = Context({\
        'item_name':                payment.paypal.item_name,
        'alias':                    payment.alias,
        'account':                  payment.account,
        'type':                     payment.type,
        'blog_posts':               blog_posts,
        'amount':                   '%.2f %s' % (payment.amount, payment.paypal.mc_currency if payment.paypal else 'USD'),
        'first_amount':             '%.2f %s' % (payment.account.service.calc_discounted_price(account=payment.account) or 0, 'USD'),
        'assistant':                assistant,
        'site':                     site,
        'date':                     now().strftime('%m/%d/%Y'),
        'next':                     next.strftime('%m/%d/%Y') if next else None,
        'signup':                   signup,
        'modify':                   modify,
        'old_service':              old_service,
        'old_max_users':            old_max_users,
        'payer_email':              payment.paypal.payer_email if payment.paypal else payment.alias.email
    }) 
    
    to = [(alias.get_full_name(), alias.email)]
    
    send_email('main_payment_receipt.html', c,
        (_("Your %s subscription") % site.name) if (signup) else (_("Your payment receipt from %s") % site.name),
        (assistant.get_full_name(), assistant.email),
        to,        
        noted=noted
    )    
    
    try:  
        from network.ajax_admin import admin_ping
        t = loader.get_template('newsfeed_payment_receipt.txt')
        content = t.render(c)   
        admin_ping(alias.contact, content, categories=['customers'])
    except:
        log("send_payment: couldn't create customer contact", traceback.format_exc())
                     



