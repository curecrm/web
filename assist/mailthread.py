from assist.models import *
from utils import *
import hashlib, re
from datetime import *
import random
import json
import traceback, pdb
import email, difflib
import mail
from assist import ics


# Threading settings -- tweak these as we figure out stuff
MIN_SUBJECT_LEN = 4
HOWLONG_REPLY_AGO = timedelta(days=14)                  # How far back we look for the parent of an email with missing reply id
HOWLONG_REPLY_AGO_FUZZY = timedelta(days=7)             # How far back we look for the parent of an email with missing reply id if fuzzy-matching addresses
HOWLONG_REPLY_AGO_THREAD_TOPIC = timedelta(days=7)      # How far back we look for the parent of an email with missing reply id if using thread-topic
HOWLONG_REPLY_SLACK = timedelta(minutes=2)              # How far forward we look for the parent of an email with missing reply id
                                                        #  (since clocks may not be perfectly synced across mail servers)
MIN_FUZZY_ADDRESS_MATCH = 0.7
MESSAGE_ID_PART_LENGTH = 10                             # For guessing based on message ids (Outlook)


class EmailThreader(object):
        
    def find_parent_by_in_reply_to(self, email, queryset_account):
        """
        Find parent message based on in-reply-to field        
        """
        
        if not email.in_reply_to:
            return None
        
        parent_ids = self.parse_message_ids(email.in_reply_to)
        for parent_id in parent_ids:
            for e in queryset_account.filter(message_id=parent_id):
                return e
        return None
        
    def find_parent_by_references(self, email, queryset_account):
        """
        Find parent message based on in-reply-to field
        Look for exact match only
        """
    
        if not email.references:
            return None
        
        parent_ids = self.parse_message_ids(email.references)
        for parent_id in parent_ids:
            for e in queryset_account.filter(message_id=parent_id):
                return e
        return None
    
    
    def find_parent_by_ics(self, email, assistant_email, ics_files):
        """
        If this email contains an ics event that's a reply to another ics event, try to find that an earlier email with the same event
        """
        
        for s in ics_files:
            uids = ics.reply_uids_from_ics_file(s)
            for uid in uids:
                if assistant_email:
                    q = Q(uid_uid, original_email__owner_account__assistant_set__email=assistant_email)
                else:
                    q = Q(uid=uid, original_email__owner_account=email.owner_account)                
                for e in Event.objects.filter(q):
                    return e.original_email
        return None
     
      
    def parse_message_ids(self, s):
        """Helper function: Parse a string like "<hello@blah.com><bbbb@gggg.com>" and return a list of the individual "<whatever>" items.
        Useful for parsing In-Reply-To: and References: fields"""
        if re.match(r"^\d+$", s):
            return [s]                        # Twitter message
        else:
            return re.findall(r"<[^<>]+>", s)

        
                                   
    def find_parent_email(self, email, ics_files=None, assistant_email=None):
        """
        Master function -- try to determine the parent of an email
        
        If assistant_email is given, use that to try to guess which account an email belongs to
        (since email.owner_account might not have been filled in in that case)
        """
        
        if not ics_files:
            ics_files = []
        
        if assistant_email:
            queryset_account = Email.objects.filter(owner_account__assistant_set__email=assistant_email)
        else:
            queryset_account = Email.objects.filter(owner_account=email.owner_account)
        
        # For now, we only do the *simplest* kind of threading,
        #  since threading isn't so important to our current product [eg 7/16/10]
                                      
        p = self.find_parent_by_in_reply_to(email, queryset_account)
        if p and (p != email):
            return p
        
        p = self.find_parent_by_references(email, queryset_account)
        if p and (p != email):
            return p
        
        p = self.find_parent_by_ics(email, assistant_email, ics_files)
        if p and (p != email):
            return p
        
        return None


    def find_child_emails(self, email):
        return []   # TODO: find_child_emails given a fixed set of emails (everything in the crawl
                    #  up to now 
