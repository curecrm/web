from django import template
from django.utils.translation import ugettext as _
from utils import *
from assist import mail
from assist.models import *
import copy

register = template.Library()

# Filters that can be used in the formmails

@register.filter
def alnum(s):
    return strip_nonalnum(s)


