from django.conf.urls.defaults import *
from django.conf import settings
from django.contrib import admin
from django.conf.urls import *
admin.autodiscover()

urlpatterns = patterns('',
    (r'^openid_start/$', 'openid_auth.views.openid_start'),
    (r'^openid_return/$', 'openid_auth.views.openid_return')
)

