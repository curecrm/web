{% load i18n %}
{% load account_tags %}
{% minifyjs %}

// from http://www.somacon.com/p355.php
String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
	return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
	return this.replace(/\s+$/,"");
}

if(!this.JSONLib){this.JSONLib={};}(function(){function f(n){return n<10?'0'+n:n;}if(typeof Date.prototype.toJSON!=='function'){Date.prototype.toJSON=function(key){return isFinite(this.valueOf())?this.getUTCFullYear()+'-'+f(this.getUTCMonth()+1)+'-'+f(this.getUTCDate())+'T'+f(this.getUTCHours())+':'+f(this.getUTCMinutes())+':'+f(this.getUTCSeconds())+'Z':null;};String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(key){return this.valueOf();};}var cx=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,escapable=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,gap,indent,meta={'\b':'\\b','\t':'\\t','\n':'\\n','\f':'\\f','\r':'\\r','"':'\\"','\\':'\\\\'},rep;function quote(string){escapable.lastIndex=0;return escapable.test(string)?'"'+string.replace(escapable,function(a){var c=meta[a];return typeof c==='string'?c:'\\u'+('0000'+a.charCodeAt(0).toString(16)).slice(-4);})+'"':'"'+string+'"';}function str(key,holder){var i,k,v,length,mind=gap,partial,value=holder[key];if(value&&typeof value==='object'&&typeof value.toJSON==='function'){value=value.toJSON(key);}if(typeof rep==='function'){value=rep.call(holder,key,value);}switch(typeof value){case'string':return quote(value);case'number':return isFinite(value)?String(value):'null';case'boolean':case'null':return String(value);case'object':if(!value){return'null';}gap+=indent;partial=[];if(Object.prototype.toString.apply(value)==='[object Array]'){length=value.length;for(i=0;i<length;i+=1){partial[i]=str(i,value)||'null';}v=partial.length===0?'[]':gap?'[\n'+gap+partial.join(',\n'+gap)+'\n'+mind+']':'['+partial.join(',')+']';gap=mind;return v;}if(rep&&typeof rep==='object'){length=rep.length;for(i=0;i<length;i+=1){k=rep[i];if(typeof k==='string'){v=str(k,value);if(v){partial.push(quote(k)+(gap?': ':':')+v);}}}}else{for(k in value){if(Object.hasOwnProperty.call(value,k)){v=str(k,value);if(v){partial.push(quote(k)+(gap?': ':':')+v);}}}}v=partial.length===0?'{}':gap?'{\n'+gap+partial.join(',\n'+gap)+'\n'+mind+'}':'{'+partial.join(',')+'}';gap=mind;return v;}}if(typeof JSONLib.stringify!=='function'){JSONLib.stringify=function(value,replacer,space){var i;gap='';indent='';if(typeof space==='number'){for(i=0;i<space;i+=1){indent+=' ';}}else if(typeof space==='string'){indent=space;}rep=replacer;if(replacer&&typeof replacer!=='function'&&(typeof replacer!=='object'||typeof replacer.length!=='number')){throw new Error('JSONLib.stringify');}return str('',{'':value});};}if(typeof JSONLib.parse!=='function'){JSONLib.parse=function(text,reviver){var j;function walk(holder,key){var k,v,value=holder[key];if(value&&typeof value==='object'){for(k in value){if(Object.hasOwnProperty.call(value,k)){v=walk(value,k);if(v!==undefined){value[k]=v;}else{delete value[k];}}}}return reviver.call(holder,key,value);}text=String(text);cx.lastIndex=0;if(cx.test(text)){text=text.replace(cx,function(a){return'\\u'+('0000'+a.charCodeAt(0).toString(16)).slice(-4);});}if(/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,'@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,']').replace(/(?:^|:|,)(?:\s*\[)+/g,''))){j=eval('('+text+')');return typeof reviver==='function'?walk({'':j},''):j;}throw new SyntaxError('JSONLib.parse');};}}());

var Prompt = this.Prompt = {
	text:		'',
	callback:	null,

	// Display a fancybox modal dialog that replaces javascript prompt,
	// or just use regular prompt on platforms that don't need it (iphone)
	prompt: function(text, callback) {
		Prompt.text = text;
		Prompt.callback = callback;
		p = $('#prompt_link');
		if (p.length > 0) {
			p.click();			// bring up the modal dialog
		} else {
			// No prompt link -- use regular prompt for this agent
			callback(prompt(text));
		}
	},

	//callback from the box
	return_result: function(result) {
		if (Prompt.callback != null) {
			Prompt.callback(result);
		}
		Prompt.callback = null;	// so that the close box doesn't call the callback again
	},

	on_show: function() {
		$('#prompt_text').text(Prompt.text);
		$('#prompt_input')[0].focus();
	},

	on_close: function() {
		if (Prompt.callback != null) {
			Prompt.callback(null);
		}
	}
}

/**
 * Insert content at caret position (converted to jquery function)
 * @link
http://alexking.org/blog/2003/06/02/inserting-at-the-cursor-using-javascript
 */
$.fn.insertAtCaret = function (myValue) {
        return this.each(function(){
                //IE support
                if (document.selection) {
                        this.focus();
                        sel = document.selection.createRange();
                        sel.text = myValue;
                        this.focus();
                }
                //MOZILLA/NETSCAPE support
                else if (this.selectionStart || this.selectionStart == '0') {
                        var startPos = this.selectionStart;
                        var endPos = this.selectionEnd;
                        var scrollTop = this.scrollTop;
                        this.value = this.value.substring(0, startPos)
                                      + myValue
                              + this.value.substring(endPos,
this.value.length);
                        this.focus();
                        this.selectionStart = startPos + myValue.length;
                        this.selectionEnd = startPos + myValue.length;
                        this.scrollTop = scrollTop;
                } else {
                        this.value += myValue;
                        this.focus();
                }
        });

};
var Wizard = this.Wizard = {
	initial_wizard_list: [],
    reload_when_done: false,
    location_when_done: '?',
	add_to_initial_list: function(w) {
		Wizard.initial_wizard_list.push(w);
	},

	hide_and_show_next: function() {
		$('.wizard').hide();
		if (Wizard.initial_wizard_list.length > 0) {
			var w = Wizard.initial_wizard_list.shift();
            //if (w != '.wizard_server') {
                $(w).show();
            //}

		} else if (Wizard.reload_when_done) {
			window.location = Wizard.location_when_done;
		}
	}
};
var QuickBox = this.QuickBox = {
	twitter_allowed: {% if alias.account.service.allow_twitter %} true {% else %} false {% endif %},
	twitter_enabled: {% if alias.get_default_twitteruser %} true {% else %} false {% endif %},
	scroll_to_conversations: {% ifequal agent 'iphone' %} true {% else %} false {% endifequal %},
	contact_id:		'',
	contact_name:	'',
	bucket_slug:	'',
	bucket_name:	'',
	reply_id:		'',
	reply_name:		'',
	reply_to_all:	false,
	reply_twitter:	false,
	public_twitter:	false,
	init: function() {
		QuickBox.update_tooltip();
	},
	update_tooltip:	function() {
		var tooltip = '';
		var close_link = "<span style='margin-left: 5px;'><a href='#' onclick='QuickBox.cancel();return false;'><img border='0' src='/static/tango/actions/process-stop.png'/></a></span>";
		var button = '{% trans "Go!" %}';
		if (Ajax.agent != 'firefox')
			var at_syntax = "";
		else
			var at_syntax = "";
		if (QuickBox.reply_to_all) {
			tooltip = '{% trans "Reply to: all" %}';
			button = '{% trans "Reply" %}';
		} else if (QuickBox.reply_id) {
			tooltip = '{% trans "Reply to" %}: ' + QuickBox.reply_name;
			button = '{% trans "Reply" %}';
		} else if (QuickBox.contact_name) {
			tooltip = '{% trans "Enter a message or schedule something regarding" %}: ' + '<b>' + QuickBox.contact_name + '</b>';
			close_link = '';
		} else if (QuickBox.bucket_slug && QuickBox.bucket_name) {
			tooltip = '{% trans "Tag" %}: ' + QuickBox.bucket_name;
			close_link = '';
		} else if (QuickBox.public_twitter) {
			tooltip = '{% trans "Enter a Tweet" %}';
			button = '{% trans "Tweet" %}';
		} else {
			tooltip = '{% trans "Enter a message or schedule something" %}';
			close_link = "<span style=''></span>";
		}
		tooltip = tooltip.replace('/\n$/', '');
		$('#parse_submit').val(button);

		if (tooltip) {
			$('#tooltip_container').show();
			$('#tooltip_msg').html(tooltip + close_link + at_syntax);
		} else {
			$('#tooltip_container').hide();
		}

	},
	submit:	function() {
		if ( !((QuickBox.reply_twitter || QuickBox.public_twitter) && $('#parse').val().length > 140) ||
			confirm('{% trans "Your message is too long for a tweet and will be cut off. Is this OK?" %}')) {
			Ajax.submit_parse($('#parse').val(), QuickBox.contact_id, QuickBox.bucket_slug, QuickBox.reply_id, QuickBox.reply_to_all, QuickBox.public_twitter);
		}
	},
	scroll_to: function() {
		try {
			$('#parse').focus();
		} catch (msg) { }
	},
	select_public_twitter: function() {
		if (!QuickBox.twitter_enabled) {
			if (confirm('{% trans "You do not have a Twitter account setup. Would you like to setup Twitter now?" %}')) {
				if (Ajax.agent == 'firefox') {
					window.open('http{% if has_ssl %}s{% endif %}://{{site.domain}}/dash/{{alias.account.base_url}}/{% if alias.is_admin %}admin{% else %}settings{% endif %}/', '_blank');
				} else {
					window.location = '/dash/{{alias.account.base_url}}/{% if alias.is_admin %}admin{% else %}settings{% endif %}/';
				}
			} else {
				return;
			}
		}
		QuickBox.clear_reply();
		QuickBox.public_twitter=true;
		QuickBox.update_tooltip();
	},
	clear_reply: function() {
		QuickBox.reply_id = '';
		QuickBox.reply_name = '';
		QuickBox.reply_to_all = '';
		QuickBox.reply_twitter = false;
		QuickBox.public_twitter = false;
		QuickBox.update_tooltip();
	},
	clear_note_only:	function() {
		//alert("clear_note_only");
		//console.trace();
		//$('#parse').val('{% trans "Type something..." %}');
		$('#parse').attr('rows', '3');
		QuickBox.clear_reply();
	},
	clear_contact_bucket: function() {
		QuickBox.contact_id = '';
		QuickBox.contact_name = '';
		QuickBox.bucket_slug = '';
		QuickBox.bucket_name = '';
	},
	clear:	function() {
		QuickBox.clear_reply();
		QuickBox.clear_contact_bucket();
		QuickBox.clear_note_only();		// call this last because it updates the tooltip
		try {
			Page.kb_action_icons_enabled = true;
		} catch (msg) {
			//
		}
	},
	cancel: function() {
		// When they hit the red X
		QuickBox.clear_contact_bucket();
		QuickBox.clear_reply();
		try {
			Page.kb_action_icons_enabled = true;
		} catch (msg) {
			//
		}
	}
};

var Ajax = this.Ajax = {
    BASE_AJAX_URL: '{% ifequal agent 'firefox' %}http{% if has_ssl %}s{% endif %}://{{site.domain}}{% endifequal %}{{url_prefix}}/ajax/',
    agent: {% if agent %} "{{agent}}" {% else %} null {% endif %},
    people: false,
    current_ajax_request: null,
    init: function() {

		if (typeof($['orig_ajax']) == 'undefined') {

			// We only replace the $.ajax function if it hasn't been replaced before
			// If Calenvy and Follow-up Robot plugins are both installed, this takes
			// care of CSRF for both

			$.orig_ajax = $.ajax;
			$.ajax = function(options) {

		    	// Find all unique csrf tokens that we need to pass, join them with a comma
				var csrf_tokens = [];
				$('.csrfmiddlewaretoken').each(function(index) {
					var token = $(this).val();
					var unique = true;
					var i;
					for (i=0; i<csrf_tokens.length; i++) {
						if (csrf_tokens[i] == token) {
							unique = false;
							break;
						}
					}
					if (unique)
						csrf_tokens.push(token);
				});

				if (csrf_tokens.length > 0) {
					if (!options.data) {
						options.data = new Object();
					}
					options.data.csrfmiddlewaretoken = csrf_tokens.join(',');
				}

				if (Ajax.agent) {
					if (!options.data) {
						options.data = new Object();
					}
					options.data.agent = Ajax.agent;
				}

				// Prevent the browser from caching Ajax requests
				options.data.nocache = (new Date().getTime()) + '_' + Math.floor(Math.random() * 10000);

				return $.orig_ajax(options);
			}
		}

		$('#loading_bar').ajaxStart(function() {
            $(this).show().fadeTo(.8,.8);
        });
        $('#loading_bar').ajaxStop(function() {
            $(this).hide();
        });

	},
	failure: function(msg, redirect) {
		//alert('{% trans "Warning: you may be logged out of" %} {{site.name}}:\n\n'+msg);
		if (typeof(redirect) == "undefined") {
			//don't do anything.
		} else {
			if (Ajax.agent != 'firefox') {
				//window.location = redirect;
			} else {
				if (typeof(Page.logout) != "undefined") {
					Page.logout();
				}
			}

		}
	},
	failure_logout: function(msg) {
		Ajax.failure(msg, "/logout/?to=%2Flogin%2F{% if agent %}%3Fagent%3D{{agent}}{% endif %}");
	},
	load_people_select: function(element, default_selection) {

		var parent_element = $(element);

		if (!!Ajax.people) {

			var people = Ajax.people;

			parent_element.html('');

			if (!!default_selection) {
				$("<option selected='selected'>"+default_selection+"</option>")
					.appendTo(parent_element);
			}

			for (i=0; i<people.length; i++) {

				$("<option>"+people[i].ez_name+"</option>")
					.val(people[i].ez_name)
					.appendTo(parent_element);
            }

		}
	},
	load_people_div: function(select_person_function) {

		var people = Ajax.people;

		var peopleDiv = false;
        if (people.length == 0) {
        	peopleDiv = $("<div>{% trans 'No one else is part of your network. ' %} </div>");
        } else {
			peopleDiv = $("<div> </div>");

			//append the @all.

			$("<div></div>")
				.text('@all')
				.attr('title','@all')
				.data('ez','@all')
				.css({	borderBottom:'1px solid #ccc',
						cursor: 'pointer',
						marginBottom: '3px'
				})
				.click(function(e) {
					if (select_person_function != undefined) {
						select_person_function($(this));
					} else {
						$('#parse').insertAtCaret($(this).data('ez')+' ');
					}
				})
				.append('<span style="border-bottom: 0px;"> - Everyone</span>')
				.appendTo(peopleDiv);

			// append each user.

        	for (i=0; i<people.length; i++) {

				$("<div></div>")
					.text(people[i].ez_name)
					.data('ez',people[i].ez_name)
					.attr('title',people[i].ez_name)
					.css({	borderBottom:'1px solid #ccc',
							cursor: 'pointer',
							marginBottom: '3px'
					})
					.click(function(e) {
						if (select_person_function != undefined) {
							select_person_function($(this));
						} else {
							$('#parse').insertAtCaret($(this).data('ez')+' ');
						}
					})
					.append('<span style="border-bottom: 0px; color: #777"> -' + people[i].full_name + ' (' + people[i].email + ')</span>')
					.appendTo(peopleDiv);
            }
        }
        $('#people_div').html('').append(peopleDiv).toggle();//.html(people_html);

	},
	load_people: function(load_function) {

		if (!Ajax.people) {

				Ajax.send_data('load_people/', {}, function(data) {
	            if (data.return_data.success) {

	                var people = data.return_data.people;
	                Ajax.people = people;

					if (load_function != undefined) {
						load_function();
					}

	            } else {
	                alert("{% trans 'Could not get list of people.' %}");
	            }
	        }, "GET");

		} else {

			if (load_function != undefined) {
				load_function();
			}

		}

	},
	set_modals: function(name, val) {
		Ajax.send_data('set_modals/', {name: name, val: val}, function(data) {
			if (data.return_data.success) {

				if (val == 1) {
					$('.wizard_'+name).show();
					$('.wizard_'+name+'_hidden').hide();
				} else {
					if (Wizard.hide_and_show_next) {
						Wizard.hide_and_show_next();
					} else {
						$('.wizard_'+name).hide();
                        if (name != 'help')
						  $('.wizard_'+name+'_hidden').show();
					}
				}

				//window.location='/';
			} else {
				alert('{% trans "Failed to toggle your notification settings." %}');
			}
		}, "GET");
	},
	submit_parse: function(str, ref_contact_id, ref_bucket_slug, reply_id, reply_to_all, public_twitter, require_at_symbol) {
        if (str == "{% trans 'Example: Meeting with jessica on monday at 5'%}" || str == "") {
            alert("{% trans 'Enter something like: running to lunch with @bob @status, or remind me to chat with @james next monday' %}");
            return;
        }
        $('#parse_submit').attr('disabled', 'disabled');
        ref_contact_id = typeof(ref_contact_id) != 'undefined' ? ref_contact_id : false;
        ref_bucket_slug = typeof(ref_bucket_slug) != 'undefined' ? ref_bucket_slug : false;
        reply_id = typeof(reply_id) != 'undefined' ? reply_id : false;
        reply_to_all = typeof(reply_to_all) != 'undefined' ? reply_to_all : false;
        public_twitter = typeof(public_twitter) != 'undefined' ? public_twitter : false;
        require_at_symbol = typeof(require_at_symbol) != 'undefined' ? require_at_symbol : true;
        Ajax.send_data('parse/', {str: str, ref_contact_id: ref_contact_id, ref_bucket_slug: ref_bucket_slug, reply_id: reply_id, reply_to_all: reply_to_all, public_twitter: public_twitter, require_at_symbol: require_at_symbol}, function(data) {
        	$('#parse_submit').removeAttr('disabled');
            if (data.return_data.success) {
	            $('#parse').val('');
	            QuickBox.clear_note_only();
	            if (data.return_data.group_id) {
		            Page.highlight_group_id = data.return_data.group_id;
	            }
	            if (Page.update_widgets) {
		            Page.update_widgets();
		            if (QuickBox.scroll_to_conversations) {
			            //$('#note_box').toggle('fast');
		            }
	            } else {
	                window.location = '/';
	            }
            } else {
	            alert(data.return_data.msg);
	            {% comment %} Parse might fail for normal reasons, don't log them out {% endcomment %}
	            //Ajax.failure_logout(data.return_data.msg);
                //Ajax.failure_logout("{% trans 'Sorry, could not save or send your query. Are you sure that you are still logged in?' %}");
            }
        }, "GET");
    },
    eval_json: function(data) {
        if (typeof(data) == "string")
            var res=eval('(' + data + ')');
        else
            var res=data;
        return res;
    },
	login: function() {
		var email = $('#email_login').val();
		var password = $('#email_password').val();
		Ajax.send_data('login/', {email: email, password: password, base_url: '{{base_url}}' }, function(data) {
            if (data.return_data.success) {
            	if (data.return_data.redirect)
					window.location=data.return_data.redirect;
				else
					alert(data.return_data.msg);

            } else {
                alert(data.return_data.msg);
            }
		}, "GET");
	},

	toggle_perms: function(widget_id, id) {

		var img = $('#kb_'+widget_id+'_'+id+'_perms');
		var private = 0;
		if (img.hasClass('perms_private')) {
			private = 1;
		}

		private = 1-private;		// toggle

        Ajax.send_data('edit_email/',{cmd: 'set_perms', id:id, private:private},function(data){
            if (data.return_data.success) {

                if (private) {
                    img.removeClass('perms_public');
                    img.addClass('perms_private');
                    img.attr('src', '/static/tango/emblems/emblem-readonly.png');
                    img.attr('title', '{% trans "This is private: only you can see this." %}');
                } else {
                    img.addClass('perms_public');
                    img.removeClass('perms_private');
                    img.attr('src', '/static/tango/emblems/emblem-public.png');
                    img.attr('title', '{% trans "This is public: your whole team can see this." %}');
                }
            } else {
                alert("{% trans 'Could not toggle permissions.' %}");
            }
        },"GET");

	},

    send_data: function(ajax_function, data, call_func, method, type, abort_previous) {
          var that = this;
          var callFunction = function(func, object) { func(object); }

          if (typeof(type) == "undefined" || Ajax.agent == 'firefox') {
        	  type = (Ajax.agent == 'firefox') ? 'jsonp' : 'json';
          }

          if (typeof(method) == "undefined")
            method = "GET";
          var url;
          if (ajax_function.indexOf('http') == 0) {
              url = ajax_function;
          } else {
              url = that.BASE_AJAX_URL+ajax_function;
          }

          if (abort_previous == true) {
              Ajax.abort();
          }

          $(function() {
              Ajax.current_ajax_request = $.ajax({
                    type: method,
                    url: url,
                    dataType: type,
                    data: data,
                    error: function(){
                    	Ajax.current_ajax_request = null;
                        //Common.logger('Error loading data');
                    },
                    success: function(return_data)
                    {
      					Ajax.current_ajax_request = null;
      					// return_data is null when this Ajax call has been aborted
                        if ((call_func != null) && (return_data != null)) {
                            callFunction(call_func, {return_data: return_data, input_data: data});
                        }
                    }
              });
          });
    },

    abort: function() {
    	if (Ajax.current_ajax_request != null) {
    		Ajax.current_ajax_request.abort();
    		Ajax.current_ajax_request = null;
    	}
    }
};


var CalenvyOpenID = this.CalenvyOpenID = {
	BASE_URL: '{% ifequal agent 'firefox' %}http{% if has_ssl %}s{% endif %}://{{site.domain}}{% endifequal %}{{url_prefix}}/',

	popup_window: null,			// the actual window that was popped up

	/*
	 * Popup a window (within Calenvy) that looks up the OpenID endpoint and redirects to the proper URL
	 */
	popup: function(email) {
		var url = CalenvyOpenID.BASE_URL + 'openid_auth/openid_start?email=' + encodeURI(email);
		CalenvyOpenID.popup_window = window.open(url, '', 'toolbar=0,scrollbars=0,location=1,statusbar=0,menubar=0,resizable=0,width=500,height=400');
		CalenvyOpenID.popup_window.focus();
		CalenvyOpenID.check_popup_interval = setInterval("CalenvyOpenID.check_popup()", 50);
	},

	check_popup_interval: null,

	// Call this periodically to check if the popup window has been closed; if so, take some action
	check_popup: function() {
		// Sidebar already exists -- check if the OpenID authentication window has been closed
		if (CalenvyOpenID.popup_window != null && CalenvyOpenID.popup_window.closed) {
			CalenvyOpenID.popup_window = null;
			clearInterval(CalenvyOpenID.check_popup_interval);
			CalenvyOpenID.check_popup_interval = 0;

			// Click a hidden button to tell the plugin to reload the sidebar
			var button = document.getElementById('calenvy_reload_sidebar_button');
			if (button != null) {
				button.click();
			} else {
				window.location.reload();
			}
		}
	},

	/*
	 * First step in the Google OpenID authentication process
	 * Called by the popup window, tells it where to redirect
	 * Get a URL for the popup window
	 *
	 * email: The user's Google email address
	 */
	get_endpoint: function(email) {
		Ajax.send_data('openid_auth/get_endpoint/', {email: email},
			function(data) {
				if (data.return_data.success) {
					window.location = data.return_data.url;
				} else {
					alert(data.return_data.msg);
				}
			}, "GET");
	}
};



$(document).ready(function() {
	var prompt_link = $("a#prompt_link");

	if (prompt_link.length > 0) {
		// For agents which use the prompt link, initialize the fancy box
		// Others (iphone, firefox) don't use this
		try {
			$("a#prompt_link").fancybox({ 'frameWidth': 170, 'frameHeight': 120, 'hideOnContentClick': false, 'callbackOnClose': Prompt.on_close, 'callbackOnShow': Prompt.on_show });
			// Make the fancybox be on top of everything else, including modals
			//  thanks to http://commonmanrants.blogspot.com/2008/11/fancybox-jquery-and-z-index.html
			$('#fancy_wrap').css('z-index', 1001000);
			$('div#fancy_overlay').css('z-index', 1001001);
			$('div#fancy_outer').css('z-index', 1001002);
		} catch (msg) { }
	}
	Ajax.init();
	QuickBox.init();

});

{% endminifyjs %}
