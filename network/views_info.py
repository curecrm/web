try:
        import json
        class Encoder(json.JSONEncoder):
            def default(self, obj):
                try:
                    return obj.to_json()
                except AttributeError:
                    return json.JSONEncoder.default(self, obj)
except ImportError:
        pass

#from publisher.models import *
from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.http import require_http_methods
from django.template import Context, RequestContext, loader, Template
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.utils.translation import ugettext as _
from django.db.models import Q
from django.conf import settings
from lib.csrf.middleware import csrf_exempt
import hashlib
import re
from datetime import datetime, timedelta
import time
import random
import sys, os, pprint
from utils import *
from django.core import serializers
from assist import mail
from assist.models import *
from twitter.models import *
import traceback

def down(request):
    t = loader.get_template('main_error_down.html')
    c = RequestContext(request, dict={\
        'request': request,
        'messages': request.messages
    })
    c.update(global_context(request))
    return HttpResponse(t.render(c))

@global_def   
def success(request):
    "Page showing a bunch of company logos who have used our product"
    t = loader.get_template('main_info_success.html')
    featured_accounts = Account.objects.active().exclude(logo_url=None).exclude(logo_url='').values('logo_url','logo_weight','name').order_by('-logo_weight','name')
    featured_accounts = featured_accounts[:(len(featured_accounts)/3)*3] # make it divisible by 3 (to look nice)
    
    c = RequestContext(request, dict={
        'featured_accounts': featured_accounts
    })
    c.update(global_context(request))
    return HttpResponse(t.render(c))

@global_def   
def learn(request):
    t = loader.get_template('main_info_learn.html')
    c = RequestContext(request, dict={
    })
    c.update(global_context(request))
    return HttpResponse(t.render(c))

@global_def   
def prompt(request):
    t = loader.get_template('modals/prompt.html')
    c = RequestContext(request, dict={
    })
    c.update(global_context(request))
    return HttpResponse(t.render(c))

#@global_def    
def order(request):
    """
    Page showing the different packages we offer
    For mobile templates, this also acts as the "go" page
    """
    
    agent = get_agent(request)
    
    upgrade = bool(int(request.GET.get('upgrade', 0)))
    
    if 'alias' in request.session:
        alias = request.session['alias']
        
        # For mobile agents, if they're not upgrading then we log them out when they visit this page....
        #  just in case they were logged in as somebody else when they clicked on a "register as new user" button.
        if alias and agent and not upgrade:
            auth.logout(request)      
            clear_session(request)
            alias = None
        
        #expired accounts 
        if alias and not alias.billing_allowed():
            request.messages['notices'].append(_('Only a team admin can visit the order page when logged in.') )
        #    return HttpResponseRedirectAgent(request, '/')
        
        if alias and alias.is_admin and alias.account.status == ACCOUNT_STATUS_ACTIVE:
            if alias.account.service.allow_agent(agent):
                request.messages['notices'].append(_('You are logged in. Select a service level below and complete payment to alter your account service.') )
            else:
                if agent in ['outlook', 'firefox', 'salesforce']:
                    agent_name = agent.title()
                else:
                    agent_name = _('mobile')
                request.messages['notices'].append(_('Your service level does not allow %s integration. Select a different service level below and complete payment to alter your account service.') % agent_name)
        elif alias and alias.is_admin and alias.account.status == ACCOUNT_STATUS_INACTIVE:
            request.messages['notices'].append(_('Your account has expired and we have not received a payment from your payment gateway. Please make a payment below.'))                
        elif alias and alias.is_admin and alias.account.status == ACCOUNT_STATUS_INACTIVE_MAX_USERS_EXCEEDED: 
            request.messages['notices'].append(_('Your account has been temporarily suspended because you have more users than your service allows. Please modify your subscription below or select a different service, or <a href="/dash/%s/admin">remove at least %d user(s)</a> to restore service.') % (alias.account.base_url, alias.account.num_active_aliases() - alias.account.max_users))                
 
        else:
            alias = None
    else:
        alias = None
            
    handle_special_session_subdomains(request)
    
    # Preselected service name (select this one in the drop down list -- only for mobile agents)
    service_name = request.GET.get('service')
    if service_name:
        service = Service.objects.get(name__iexact=service_name)
    else:
        service = None
        
    # list of all services (for the mobile/agent signup form).
    # List free services first, then free trial services, then fully paid ones
    services = Service.objects.get_available_services(agent=agent)
    
    # Dictionary of all services keyed by name, used for the main (non-mobile) signup form.
    services_dict = CaselessDict([(s.name, s) for s in Service.objects.filter(hidden=False)])
    payment_required = service and service.is_payment_required(upgrade=bool(alias))
    
    t = loader.get_template('main_info_order.html')
    c = RequestContext(request, dict={
        'messages': request.messages,
        'alias': alias,
        'show_payment': upgrade or payment_required,
        'payment_required': payment_required,
        'services': services,                                       # list of all services (for the mobile/agent signup form)
        'service': service,                                         # preselected service name, if any (for the mobile/agent signup form)
        'services_dict': services_dict,
        'show_salesforce': request.session.get('show_salesforce', False) or (alias and alias.account.service.name.lower() == 'salesforce'),
        'parent_url': request.GET.get('parent_url'),
        'version': request.GET.get('version'),               # version of the agent (for agents that have a direct link to the registration page)
    })
    c.update(global_context(request))
    return HttpResponse(t.render(c))
    
 
        

@global_def
def terms(request, terms_type):
    try:
        log (terms)
        if terms_type == 'terms' or terms_type == 'privacy':
             t = loader.get_template('terms/'+terms_type+'.html')
        else:
            raise Exception('Illegal terms access.')
        c = RequestContext(request)
        c.update(global_context(request))
        return HttpResponse(t.render(c))
    except:
        return HttpResponse('Cannot render terms.')

#@second_level_domain_only
def go(request, service_name=None):
    """"
    Page allowing user to order (or upgrade to) a specific package
    For regular website only -- agents like iphone use the order page
    """
    
    log("views_info.go: service_name=", service_name)
                
    if 'alias' in request.session:
        log('alias in request session')
        alias = request.session['alias']
        if alias and not alias.billing_allowed():
            request.messages['notices'].append(_('Only a team admin can visit the payment page when logged in.') )
            log('redirecting to /')
            return HttpResponseRedirectAgent(request, '/')

        button_html = '<br/><br/><input type="button" name="go" value="'+ _("<< Choose a different service plan") + '" onclick="window.location=\'/@/register/Free\'"/>' 
        #if alias and alias.billing_allowed() and alias.account.status == ACCOUNT_STATUS_ACTIVE:
        #    request.messages['notices'].append(_('You are logged in.  Payments made here will change your account service plan and #expiration automatically.') + button_html)
        if alias and alias.billing_allowed() and alias.account.status == ACCOUNT_STATUS_INACTIVE:
            request.messages['notices'].append(_('Your account has expired and we have not received a payment from your payment gateway. Please make a payment below.') + button_html)                
        elif alias and alias.billing_allowed() and alias.account.status == ACCOUNT_STATUS_INACTIVE_MAX_USERS_EXCEEDED: 
            request.messages['notices'].append(_('Your account has been temporarily suspended because you have more users than your service allows. Please modify your subscription below or select a different service, or <a href="/dash/%s/admin">remove at least %d user(s)</a> to restore service.') % (alias.account.base_url, alias.account.num_active_aliases() - alias.account.max_users))                
        
        # ACCOUNT_STATUS_DELETE_PENDING and ACCOUNT_STATUS_DELETE_IN_PROGRESS shouldn't even be able to get here
        else:
            alias = None
    else:
        alias = None
    account = alias and alias.account
    
    # Find the service
    try:
        service = Service.objects.get(name__iexact=service_name)    # iexact is IMPORTANT here! Leave it in.
    except:
        try:
            service = Service.objects.get(pk=1)
        except:
            request.messages['notices'].append(_('Failed finding the service you requested: %s' % service_name) )
            log('Could not find the service!!')
            return HttpResponseRedirectAgent(request, '/')
    
    # Find the pre-existing promo (if any)
    promo = None                # pre-filled-in promo
    
    # Allow promo via the URL for logged in users [eg 2/26]
    if 'promo' in request.GET:
        try:
            p = Promo.objects.get(name__iexact=request.GET['promo'])
            if p.service and p.service != service:
                request.messages['notices'].append(_('Promo %s can only be applied to the service <a href="/info/go/%s/?promo=%s">%s</a>.') % (request.GET['promo'], p.service.name.lower(), request.GET['promo'].lower(), p.service.name))
            elif p.expiration and (now() > p.expiration):
                request.messages['notices'].append(_('Promo %s has already expired.') % request.GET['promo'])
            else:
                promo = p           # fill it in automatically
        except:
            pass
                
    disable_trial = bool(alias) or (promo and promo.disable_service_trial)     # No free trial if logged in
    
    t = loader.get_template('main_info_register.html')
        
    c = RequestContext(request, {
        'email': request.GET.get('email', None),
        'team_url': request.GET.get('team_url', None),
        'messages': request.messages,
        'service': service,
        'service_features': service.get_features_for_display(),
        'is_free': service.is_free(promo=promo),
        'promo': promo,                                         # Pre-filled-in promo, if any
        'show_promo': (promo is None) and (alias is None),      # If pre-set promo, then don't show it on the form
        'terms_string': service.get_fancy_terms_string(promo=promo, disable_trial=disable_trial, account=account),
        'alias': alias,
        'payment_required': service.is_payment_required(upgrade=bool(alias), promo=promo),
    })
    
    log('return httpresponse object')
    return HttpResponse(t.render(c))
go = global_def(go, allow_expired=True)

def billing(request):
    """A page allowing user to enter or update their credit card info, 
    to upgrade/downgrade or to go to Paypal"""
    
    log("views_info.billing")

    try:
        alias = request.session.get('alias')
        if not alias:
            return HttpResponseRedirectAgent(request, '/')
        
        # Billing page can only be visited by an admin, or by any user if the 
        #  account has never had a payment
        account = alias.account

        if not alias.billing_allowed():
            request.messages['notices'].append(_('Only a team admin can visit the billing page when logged in.') )
            return HttpResponseRedirectAgent(request, '/')
        
        # Which service to select by default
        if 'service' in request.GET:
            try:
                service = Service.objects.get(name__iexact=request.GET['service'])
            except:
                service = account.service
        else:
            service = account.service
                    
        promo = account.promo
        
        n = now()
        t = loader.get_template('main_info_billing.html')
        
        service_tuples = []
        for s in Service.objects.filter(hidden=False).order_by('name'):
            if not s.is_free():
                service_tuples.append((s, s.get_medium_terms_string(account)))
        
        # If the selected service is hidden and non-free, add it to the select box
        if service and (not service.is_free()) and (service not in [s[0] for s in service_tuples]):
            service_tuples.append((service, service.get_medium_terms_string(account)))
            
        c = RequestContext(request, {
            'account': account,
            'subscription': account.get_subscription_info(),
            'months': [('%02d' % i) for i in range(1, 13)],
            'years': [('%d' % i) for i in range(n.year, n.year+10)],
            'terms_string': service.get_fancy_terms_string(promo=promo, disable_trial=True, account=account),
            'paypal_fee': service.percent_paypal_fee or 0,
            'services': service_tuples,       # List of all services
            'service': service          # Which service is selected by default in the drop-down box          
        })
        
        log('return httpresponse object')
        return HttpResponse(t.render(c))
    except:
        log("views_info.billing fail")
        log(traceback.format_exc())
        return HttpResponseRedirectAgent(request, '/dash/')
     
billing = global_def(billing, allow_expired=True)



@csrf_exempt 
def payment_cancel(request):
    """View called when user clicks Cancel within the order form on Paypal.
    This does NOT mean they have cancelled an existing subscription, just that they've abandoned their Paypal order."""
    
    request.messages['notices'].append(_('You have canceled your order on Paypal.'))
    return HttpResponseRedirectAgent(request, '/@/register/Free/')
payment_cancel = global_def(payment_cancel, allow_expired=True)

@csrf_exempt
@global_def
def payment_complete(request):
    
    try:                
        # No payment complete page for the agents if they're already logged in
        #agent = get_agent(request)
        #if agent and request.session.get('alias'):
        #    return HttpResponseRedirectAgent(request, '/dash/')
        
        alias = request.session.get('alias', None)
        
        # "cancel" means that this particular payment was cancelled on Paypal, 
        # not that the whole account is cancelled (payment_cancel above)!
        cancel = bool(int(request.GET.get('cancel', '0')))
        if cancel:
            log ('Payment cancelled')
            request.messages['notices'].append(_('Your payment has been cancelled.'))
    
            t = loader.get_template('main_info_payment_complete.html')
            c = RequestContext(request, dict={
                'success': False,
                'site': get_site(request),
                'messages': request.messages,
                'alias': alias
            })
            log('return httpresponse object')
            c.update(global_context(request))
            return HttpResponse(t.render(c))            
            
        
        custom = request.GET.get('custom', '')
        log("custom_field:"+custom)
        
        try:
            alias_slug, service_slug, promo_code = custom.split('_')
            log("alias_slug:", alias_slug)
            log("service slug:", service_slug)
            log("promo code:", promo_code)
            alias = Alias.objects.active().get(slug=alias_slug)
            
            # Log them in automatically if it's their first time
            # For security, don't log them in if there's already a pw set
            #  (This doesn't affect upgrade/downgrade because they will already be logged in)
            if not alias.password_hash: 
                log("payment_complete: no password hash, logging in user")
                log("alias.account_user: ", alias.account_user)
                request.session['alias'] = alias
                user = auth.authenticate(user=alias.user)        # dummy authentication, just fill in the 'backend' attribute
                auth.login(request, user)
                pass
            else:
                log("payment_complete: password hash exists")
            account = alias.account
            service = Service.objects.get(slug=service_slug)
            if promo_code:
                promo = Promo.objects.get(code__iexact=promo_code)
            else:
                promo = None
        except:
            alias, service, promo = None, None, None            # payment-complete without logging them in
                                                                #  (they may already be logged in)

        try:
            sales_account = Account.objects.get(base_url=settings.SALES_ACCOUNT)
        except:
            sales_account = None
            
        t = loader.get_template('main_info_payment_complete.html')
        c = RequestContext(request, dict={
            'success': True,
            'site': get_site(request),
            'messages': request.messages,
            'alias': alias,
            'alias_email': alias and alias.email or request.GET.get('email'),
            'is_free': service and service.is_free(promo=promo),
            'session_referer': request.session.get('referer', ''),
            'sales_account': sales_account,
            'modify': bool(int(request.GET.get('modify', '0')))
        })
        
        if 'amt' in request.GET and 'st' in request.GET and 'cc' in request.GET:
            log('appending details')
            c.update({
                "details": {
                    "amt": request.GET['amt'],
                    "cc": request.GET['cc'],
                    "st": request.GET['st'] 
                } 
            })
        log('return httpresponse object')
        c.update(global_context(request))
        return HttpResponse(t.render(c))
    
    except:
        log ('Failed to get a complete payment... POST obj:', request.POST)
        request.messages['notices'].append(_('Something went wrong with your payment.  Please email founders@%s with your payment receipt from Paypal and we will look into it right away. ') % get_site(request).domain)

        t = loader.get_template('main_info_payment_complete.html')
        c = RequestContext(request, dict={
            'success': False,
            'site': get_site(request),
            'messages': request.messages,
        })
        log('return httpresponse object')
        c.update(global_context(request))
        return HttpResponse(t.render(c))
        

    

def install_view(request, subdomain=None, msg=''):
    ''' 
    This is the install firefox chrome view of the site '''
        
    log("install_view: init")
    
    try:

        c = RequestContext(request, dict={
            
        })
                
        t = loader.get_template('main_info_install.html')
        
        return HttpResponse(t.render(c))    
        
    except:
        log(traceback.format_exc())
        return HttpResponseRedirectAgent(request, '/')
    