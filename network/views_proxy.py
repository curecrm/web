#from publisher.models import *
from django.http import HttpResponse
import sys, os, urllib, urllib2
from utils import *

MAX_IMAGE_SIZE = 50000

# 1x1 spacer image
SPACER_GIF_DATA = 'GIF89a\x01\x00\x01\x00\x80\xff\x00\xc0\xc0\xc0\x00\x00\x00!\xf9\x04\x01\x00\x00\x00\x00,\x00\x00\x00\x00\x01\x00\x01\x00\x00\x01\x012\x00;'

@global_def
def proxy_view(request):
    """
    Load an image from some other site (URL encrypted and encoded in the parameters) and return it
    Used for proxying http images (icons or social-data profile pictures) from remote sites over HTTPS, to prevent
    trouble with Outlook, etc.
    """

    try:
        url = CRYPTER.Decrypt(request.GET['url'])
        request = urllib2.Request(url)
        handle = urllib2.urlopen(request)
        
        data = handle.read()
        mimetype = handle.headers.gettype()
        
        if len(data) > MAX_IMAGE_SIZE:
            raise Exception()
        
        if mimetype.lower() not in ['image/jpeg', 'image/pjpeg', 'image/gif', 'image/png', 'image/x-icon', 'image/bmp', 'image/x-windows-bmp']:
            log('proxy_view [Exception]: mimetype not valid: %s' % mimetype.lower())
            raise Exception()
        
        return HttpResponse(data, mimetype=mimetype)
    except:
        log(traceback.format_exc())
        return HttpResponse(SPACER_GIF_DATA, mimetype='image/gif')
    
    
def calc_proxy_url(url, request=None):
    """Calculate the proxy URL for the given image
    Used by the social data."""
    
    if not url:
        return url
    
    # We're only proxying HTTP images over HTTPS
    if url.lower().startswith('https://'):
        return url
    
    site = get_site(request) if request else Site.objects.get_current()
    
    return 'http%s://%s/proxy/?%s' % (
        's' if site.domain in settings.SSL_DOMAINS else '',                              
        site.domain, 
        urllib.urlencode({'url': CRYPTER.Encrypt(url)}))

 