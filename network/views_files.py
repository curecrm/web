try:
        import json
        class Encoder(json.JSONEncoder):
            def default(self, obj):
                try:
                    return obj.to_json()
                except AttributeError:
                    return json.JSONEncoder.default(self, obj)
except ImportError:
        pass

#from publisher.models import *
from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.http import require_http_methods
from django.template import Context, loader
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.utils.translation import ugettext as _
from django.db.models import Q
from django.conf import settings
import hashlib
import re
from datetime import datetime, timedelta
import time
import random
import calendar
import sys, os
from utils import *
from django.core import serializers
from assist.models import *
from schedule import views
from assist.constants import *
from schedule.periods import *
from assist import mail
from assist import constants
import traceback
import email.message

def always_error(request):
    raise Exception('always_error')
     

# @global_def
# def retrieve_file(request, file_slug=None):
#     
#     try:
#         log('__retrieve_file__')
#         log('slug:', file_slug)
#         
#         file = File.objects.get(slug=file_slug)
#         log('mimetype:', file.mimetype)
#         
#         contents = file.retrieve()
#         response = HttpResponse(contents, mimetype=file.get_mimetype_using_ext())
#         response['Content-Disposition'] = 'attachment; filename="%s"' % (file.name or 'noted')
#         
#         return response
#         
#     except:
#         log('__retrieve_file__ failed')
#         log(traceback.format_exc())
#         return HttpResponseRedirectAgent(request, '/')


@global_def
def retrieve_file(request, subdomain=None):

    try:        
        alias, viewing_account_user, account = validate_account(request, subdomain=subdomain)
        token_slug = request.GET['token']
        
        log('__retrieve_file__')
        log('slug:', token_slug)
        
        token = FileToken.objects.get(slug=token_slug)
        if token.alias != alias:
            raise Exception("alias doesn't have access to token_slug %s" % token_slug)
        if token.date_start and token.date_end and not (token.date_start <= now() < token.date_end):
            raise Exception("date out of range for token_slug %s" % token_slug)
        
        file = File.objects.get(slug=token.file.slug)
        log('mimetype:', file.mimetype)

        contents = file.retrieve()      # Get the file from IMAP if necessary, storing it in Cloud Files ...
                                        #  or just get it from Cloud Files if it's already there
        
        if contents is None:
            raise Exception("Couldn't retrieve file with slug=%s" % token.file.slug)
            
        response = HttpResponse(contents, mimetype=file.get_mimetype_using_ext())
        response['Content-Disposition'] = 'attachment; %s' % \
            email.message._formatparam('filename', encode_header_unicode(file.name) if file.name else 'noted')
            
        return response
        
    except:
        log('__retrieve_file__ failed')
        log(traceback.format_exc())
        
        msg = _("Failed accessing file. This email may have expired from your account.")
        request.messages['notices'].append(msg)
        
        alias = request.session.get('alias', None)
        if alias and alias.account:
            return HttpResponseRedirectAgent(request, '/dash/'+alias.account.base_url+'/')        
        else:
            return HttpResponseRedirectAgent(request, '/dash/')
            
    

    