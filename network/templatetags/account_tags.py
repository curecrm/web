from django import template
from utils import *
from assist import constants
from network import ajax_feeds
from django.template import Context, loader, Variable
import pdb, traceback, copy, re
from assist import mail
from assist.models import *
from django.template.defaultfilters import escapejs, stringfilter
from django.utils.html import escape
from django import template
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
import hashlib
from jsmin import jsmin

register = template.Library()

# http://www.djangosnippets.org/snippets/953/
class MinifyJs(template.Node):
    def __init__(self, nodelist):
        self.nodelist = nodelist
    def render(self, context):
        return jsmin(self.nodelist.render(context))

def minifyjs(parser, token):
    nodelist = parser.parse(('endminifyjs',))
    parser.delete_first_token()
    return MinifyJs(nodelist)

minifyjs = register.tag(minifyjs)

@register.filter
def eventspan(event, alias):
    user_tz = alias.account_user.get_timezone()
    owner = event.get_owner()
    display_tz=(alias.account_user.timezone != owner.alias.account_user.timezone)
    date_span = format_span(event, display_future_weekdays=True, \
            display_today=True, display_tomorrow=True, display_upper=False,
            display_both=False, display_sometime=True, display_tz=display_tz, tzinfo=user_tz)

    return date_span
    #return format_span(event, tzinfo=alias.account_user.get_timezone())

@register.filter
def md5(s):
    return hashlib.md5(s).hexdigest()

@register.filter
def ago(dt):


    def plural_string(num):
        return 's' if num > 1 else ''

    ago_td = now() - dt

    if ago_td < ONE_MINUTE:
        return _('%s sec%s ago') % (ago_td.seconds, plural_string(ago_td.seconds))
    elif ago_td < ONE_HOUR:
        return _('%s min%s ago') % ((ago_td.seconds // 60), plural_string(ago_td.seconds // 60))
    elif ago_td < ONE_DAY:
        return _('%s hour%s ago') % ((ago_td.seconds // 3600), plural_string(ago_td.seconds // 3600))
    else:
        return _('%s day%s ago') % (ago_td.days, plural_string(ago_td.days))


@register.filter
def truncatechars(s, n):
    if len(s) > n:
        s = s[:n] + '...'
    return s

# For Outlook, add target='_blank' to any links (which are the output of urlize)
@register.filter
def target_blank(html, agent):
    if agent in ['outlook', 'firefox']:
        RE_TAG = re.compile(r'(<a href="[^"]*")([^>]*>)')
        html = RE_TAG.sub('\\1 target="_blank"\\2', html)
    return html

@register.filter
def email_subject(e):
    if not mail.is_subject_reply_or_forward(e.subject_normalized):
        return e.subject_normalized + '\n'
    else:
        return ''

@register.filter
def price(value):
    "Format a float as a decimal with two digits after the ."
    return "%.2f" % value

@register.filter
def email_content(e):
    return mail.get_content_from_email(e, content_mode=CONTENT_MODE_MIN)[0]

@register.filter
def email_username(addr):
    return addr.split('@')[0].strip()

@register.filter
def reply_subject(s):
    if not mail.is_subject_reply(s):
        return 'Re: ' + s
    else:
        return s

@register.filter
def summary(m, enabled=True):
    "Wrapper for get_summary -- can be disabled with a flag to test performance"
    if enabled:
        return m.get_summary()
    else:
        return ''

RE_TAG = re.compile(r'(^|[^\w])(@[A-Za-z]\w+(\.\w+)?)')

@register.filter
def hyperlink_tags_search(text, agent):
    # find all @tag instances, and create a hyperlink to a search query.
    # Not for email (like the digest)!

    text = escape(text)

    if agent == 'windowmob':
        html = RE_TAG.sub("\\1<span style='cursor: pointer;' onclick=''><a href='javascript:search_query(\"\\2\");'>\\2</a></span>", text)
    else:
        html = RE_TAG.sub("\\1<span style='cursor: pointer;' onclick=''><a href='javascript:Page.search(\"\\2\");'>\\2</a></span>", text)
    return html

@register.filter
def bucket_link(bucket, widget_id=None):
    # Not for Windows Mobile or for email

    html = "<span style='cursor: pointer;' onclick='Page.change_bucket(\"%s\", \"%s\", %s); return false;'>%s</span>" % (bucket.slug, bucket.name, str(widget_id) if widget_id else '"all"', bucket.name)
    return html


#@register.tag
#def ajax_expand(parser, token):
#    # Create a hyperlink for an Alias or a Contact
#    args = token.split_contents()
#    item_id = args[1]
#    return AjaxExpandNode(item_id)
#
#
#class AjaxExpandNode(template.Node):
#    def __init__(self, text, item_id):
#        self.item_id = name
#
#    def render(self, context):
#        widget_id = context['widget_id']



@register.filter()
def expand_email_link(text, m):
    # Not for Windows Mobile
    if text.endswith('...'):
        text = re.sub(r"\.\.\.$", "", text)
        text += "<span><a href='#' onclick='Page.expand_email(%d);return false;'>...</a></span>" % m.id
    return text

@register.filter
def search_display(search, alias):
    if search:
        return search.get_search_display(alias)
    else:
        return ''


@register.filter
def password_bullets_aes(password_aes):
    if not password_aes:
        return ''
    password = CRYPTER.Decrypt(password_aes)
    return PASSWORD_BULLET_CHAR * len(password)


@register.tag
def personlink(parser, token):
    # Create a hyperlink for an Alias or a Contact
    args = token.split_contents()
    name = args[1]
    assigned_to = (len(args) >= 3 and args[2] == 'assigned_to')
    return PersonlinkNode(name, assigned_to)

class PersonlinkNode(template.Node):
    def __init__(self, person, assigned_to):
        self.person = template.Variable(person)
        self.assigned_to = assigned_to

    def render(self, context):
        other_person = self.person.resolve(context)
        viewing_alias = context['alias']

        # For the web: Render it as a clickable link, which searches for that person
        # For email (e.g, the morning digest) just make it a mailto: link

        if type(other_person) is Contact:
            is_self = (viewing_alias.contact == other_person)
            extra_style = 'background: #FCF8BC;' if is_self else ''
            text = other_person.get_tag_or_name_for_display(viewing_alias, indicate_self=True)
            if (other_person.status != Contact.STATUS_ACTIVE) and context['agent'] != 'salesforce':
                # Inactive contact -- show with a dotted line; user can click on this contact to activate it
                # (Don't do this for Salesforce -- for Salesforce let them click on any contact to search on it,
                # even inactive ones)
                if context['agent'] == 'windowmob':
                    html = escape(text)
                else:
                    html = "<span onclick='Page.activate_contact(\"%d\", \"%s\"); return true;' style='border-bottom: dashed 1px; border-color: #ccc; cursor: pointer;' title='%s'>%s</span>" % \
                        (other_person.id, \
                         escapejs(other_person.get_display_name(include_email=True)), \
                         escape(_('Click here to create this contact: ') + other_person.get_mouseover_text()), \
                         escape(text))

            else:
                if context['agent'] == 'windowmob':
                    html = "<a href='javascript:search_query(\"%d\", \"%s\")'>%s</a>" % (other_person.id, SEARCH_TYPE_CONTACT, escape(text))
                else:
                    title = other_person.get_mouseover_text()
                    html = "<a style='%s' href='javascript:Page.search(\"%d\", \"%s\")' title='%s'>%s</a>" % (extra_style, other_person.id, SEARCH_TYPE_CONTACT, escape(title), escape(text))
            return html
        else:
            log("personlink: invalid type %s" % type(other_person))
            return ''




@register.tag
def widget(parser, token):
    args = token.split_contents()
    name = args[1]
    return WidgetNode(name)

class WidgetNode(template.Node):
    def __init__(self, name):
        self.name = name

    def render(self, context):
        alias = context['alias']
        request = context['request']
        from network.ajax_feeds import get_widget_manager
        widget = get_widget_manager(request).get_or_create_by_name(self.name)
        return widget.as_outerhtml(request)


@register.filter
def price_main(service):
    "The first line of the price (of a given service), for display on the /@/register/Free page"

    if service.price_per_num_dmy:
        return service.get_simple_terms_string(promo=None)
    #elif service.price_per_synced_email:
    #    return service.get_per_usage_string(promo=None)
    else:
        return _('No monthly fee!')

@register.filter
def price_secondary(service):
    """The second line of the price, for display on the /@/register/Free page
    Returns a string giving the price for each synced email (per-usage price) for this given
    service. Does NOT account for promo codes

    If the service doesn't have a fixed monthly price, return an empty string here
    (the per-usage price is already given in price_main above)"""

    if (service.price_per_num_dmy == 0 and service.price_per_synced_email > 0) or service.price_per_synced_email:
        return "+ " + service.get_per_usage_string()
    else:
        return ''


# From http://www.djangosnippets.org/snippets/861/
# We use this to render flatpages with template directives inside them
class RenderNode(template.Node):

    def __init__(self, content, replace_relative=False):
        self.content = content
        self.replace_relative = replace_relative

    def replace_relative_links(self, html, site):

        def absolute_url(m):
            return m.group('before') + 'http://%s/' % site.domain + m.group('inside').lstrip('/') + m.group('after')

        return re.sub(r'(?i)(?P<before>(href|src)\s*=\s*[\'"])(?P<inside>/[^\'"]*)(?P<after>[\'"])', absolute_url, html)

    def render(self, context):
        try:
            self.content_value = template.resolve_variable(self.content, context)
            result = template.Template(self.content_value).render(template.Context(context, autoescape=False))
            if self.replace_relative:
                result = self.replace_relative_links(result, context['site'])
            return result
        except template.TemplateSyntaxError, e:
            return mark_safe("<strong>Template error: There is an error one of this page's template tags: <code>%s</code></small>" % e.message)


@register.tag(name='render')
def render_django(parser, token):
    " Example: {% render flatpage.content %}"
    content = token.split_contents()[1]
    replace_relative = (token.split_contents()[-1] == 'replace_relative_links')
    return RenderNode(content, replace_relative)
render_django.is_safe = True


@register.tag
def files(parser, token):
    '''
    Take an Email object, as follows: {% files e %}
    Create a "files" variable in the current context which is a list of tuples:
        (file, has_perms) where:
            file is the File object
            has_perms is a Boolean, and is True if current Alias has permissions to view this file immediately
                (if False, Alias must request permission)
    '''

    args = token.split_contents()
    varname = args[1]
    return FilesNode(varname)

class FilesNode(template.Node):
    def __init__(self, varname):
        self.varname = varname

    def render(self, context):
        alias = context['alias']
        self.message = template.resolve_variable(self.varname, context)
        context['files'] = [(f, f.has_download_perms(alias)) for f in self.message.get_files()]
        return ''               # Doesn't output anything, just sets 'files' in the context

@register.tag
def scheduled(parser, token):
    '''
    Take an Email object, as follows: {% scheduled e %}
    Create a "scheduled" variable in the current context which is a list of Events representing scheduled events
    that haven't happened yet
    '''

    args = token.split_contents()
    varname = args[1]
    return ScheduledNode(varname)

class ScheduledNode(template.Node):
    def __init__(self, varname):
        self.varname = varname

    def render(self, context):
        alias = context['alias']
        self.message = template.resolve_variable(self.varname, context)
        context['scheduled'] = self.message.get_scheduled_events()
        return ''               # Doesn't output anything, just sets 'scheduled' in the context
