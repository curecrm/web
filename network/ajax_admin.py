from django.http import HttpResponse, HttpResponseNotFound
from django.template import Context, loader
from assist.models import *
from django.db.models import Q
from django.conf import settings
from os.path import join, exists
from os import stat
import string
import sys, os, htmlentitydefs, hashlib, json, re, sched, time, threading
from utils import *
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.utils.translation import ugettext as _
from django.core.mail import send_mail
from assist import mail, register
from schedule.views import calendar
from assist.constants import *
import traceback

    
def edit_assistant_settings(request):
    try:
        site = get_site(request)
        alias = request.session['alias']
        account = alias.account
        old_addr = account.get_assistant().email
        
        first_name = strip_nonalnumunderscore(request.POST['first_name'])
        last_name = strip_nonalnumunderscore(request.POST['last_name'])
        
        team_name = None        # For now, all new accounts, and all accounts where assistant name gets changed,
                                #  have no team name. [eg 11/11]
                                # When we re-enable this, revisit lines 66-following below.
        
#        if is_network:
#            team_name = strip_nonascii(strip_nonalnumunderscore(request.POST['team_name'])).lower()
#        else:
#            team_name = None
            
        inform_account = bool(int(request.POST['inform_account']))
        assistant_emails_anytime = bool(int(request.POST['assistant_emails_anytime']))
        
        if not first_name:
            raise Exception('Your assistant must have at least a first name.')
        
        if not alias.is_admin:
            raise Exception('Must be an admin to do stuff here.')
            
#        if is_network and (team_name != account.name):
#            # Trying to change team name?
#            if not team_name:
#                d = {
#                    'success':  False,
#                    'msg':      _('Please enter a team name.') % team_name                     
#                }
#            queryset = Account.objects.filter(name=team_name)
#            if queryset.count() > 0:
#                d = {
#                    'success':  False,
#                    'msg':      _('Account name %s is already in use.') % team_name
#                }
#                result = json.dumps(d)
#                return HttpResponse(result, mimetype='application/json')
        
        log("inform_account: ", inform_account)
        
        domain = get_site(request).domain.split(':')[0]
        if team_name:
            full_domain = team_name + '.' + domain
        else:
            full_domain = domain
        addr = register.generate_assistant_email_address(first_name, last_name, full_domain)
        if (not addr) or Assistant.objects.filter(email=addr).exclude(owner_account=account).count() > 0:           
            raise Exception('Could not edit assistant.')
        
        assistant = account.get_assistant()
        assistant.first_name = first_name
        assistant.last_name = last_name
        assistant.email = addr
        assistant.save()
        
        account.assistant_emails_anytime = assistant_emails_anytime
        
        account.name = team_name            # may be None
        if team_name:
            account.base_url = team_name
        account.save()
            
        if inform_account and (addr != old_addr):
            # Send a mail to everybody
            c = Context({
                'assistant':    assistant,
                'site':         get_site(request)
            })
            
            to_aliases = Alias.objects.active().filter(account=account)
            for to_alias in to_aliases:
                mail.send_email('main_assistant_rename.html', c, 
                    _("[Rename] Assistant renamed to ") + assistant.email,
                    (assistant.get_full_name(), assistant.email),
                    (to_alias.get_full_name(), to_alias.email),        
                    noted={'message_type': EMAIL_TYPE_NOTED,\
                        'message_subtype': EMAIL_SUBTYPE_NOTED_ASSISTANT_CHANGED,
                        'owner_account': assistant and assistant.owner_account
                    },
                    alias=alias)
        
        request.session['alias'] = alias
        d = {
            'success': True,
            'msg': _('Assistant saved!')
        }
        result = json.dumps(d)
        return HttpResponse(result, mimetype='application/json')
    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Failed. Could not edit your assistant.')
        }
        result = json.dumps(d)
        return HttpResponse(result, mimetype='application/json')

def display_assistant_email(request):
    try:        
        site = get_site(request)
        alias = request.session['alias']
        account = alias.account

        first_name = strip_nonalnumunderscore(request.POST['first_name'])
        last_name = strip_nonalnumunderscore(request.POST['last_name'])
        
        team_name = None                # see comment above (line 51)
        
#        if is_network:
#            team_name = strip_nonascii(strip_nonalnumunderscore(request.POST['team_name'])).lower()
#        else:
#            team_name = None
#        
#        if team_name and (team_name != alias.account.name):
#            # Trying to change team name?
#            queryset = Account.objects.filter(name=team_name)
#            if queryset.count() > 0:
#                d = {
#                    'success':  False,
#                    'msg':      _('Account name %s is already in use.') % team_name
#                }
#                result = json.dumps(d)
#                return HttpResponse(result, mimetype='application/json')
        
        domain = get_site(request).domain.split(':')[0]
        if team_name:
            full_domain = team_name + '.' + domain
        else:
            full_domain = domain
            
        if not first_name:
            raise NotifyUserException(_('Your assistant must have at least a first name.'))
        addr = register.generate_assistant_email_address(first_name, last_name, full_domain)
        if (not addr) or Assistant.objects.filter(email=addr).exclude(owner_account=account).count() > 0:
            raise NotifyUserException(_("Sorry, you can't use that name for your assistant... please try another."))
            
        d = {
            'success': True,
            'assistant_email': addr,
            'msg': _('Displayed!')
        }
        result = json.dumps(d)
        return HttpResponse(result, mimetype='application/json')
    except NotifyUserException, e:
        d = {
            'success': False,
            'msg': e.message
        }
        log(traceback.format_exc())
        result = json.dumps(d)
        return HttpResponse(result, mimetype='application/json')        
    except:
        d = {
            'success': False,
            'msg': _('Failed. Could not edit your assistant.')
        }
        log(traceback.format_exc())
        result = json.dumps(d)
        return HttpResponse(result, mimetype='application/json')


def edit_privacy_settings(request):
    try:
        from ajax_feeds import get_widget_manager
        
        site = get_site(request)
        alias = request.session['alias']
        account = alias.account
        
        transparent = bool(int(request.POST['transparent']))
        
        if not alias.is_admin:
            raise Exception('Must be an admin to do stuff here.')
        
        account.transparent = transparent
        account.save()
        request.session['alias'] = alias
        get_widget_manager(request).clear_cache_all() # Regenerate the widgets

        d = {
            'success': True,
            'msg': _('Privacy settings saved.')
        }
        result = json.dumps(d)
        return HttpResponse(result, mimetype='application/json')
    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Failed. Could not edit your privacy settings.')
        }
        result = json.dumps(d)
        return HttpResponse(result, mimetype='application/json')
    

def edit_storage_settings(request):
    try:
        log("edit_storage_settings")
        
        site = get_site(request)
        alias = request.session['alias']
        account = alias.account

        if not alias.is_admin:
            raise Exception('Must be an admin to do stuff here.')
         
#        if int(request.POST.get('delete_all', 0)) == 1:
#            account.delete_all_pending = True
#            account.save()
        
        store_email_howlong = int(request.POST['store_email_howlong'])
        if store_email_howlong == -1:
            log("Store emails forever!")
        
        if (store_email_howlong not in [x[0] for x in Account.ACCOUNT_EMAIL_STORAGE_CHOICES]):
            raise Exception('Invalid value')
        
        # Only allow them to set retention settings up to the max their service level allows
        #  (unless they already have a higher value, in which case we let them keep it)
        if (account.service.max_store_email_howlong is not None) and (store_email_howlong != account.store_email_howlong):
            if (store_email_howlong is None) or (store_email_howlong > account.service.max_store_email_howlong):
                raise Exception('Not allowed for this service level')
        
        account.store_email_howlong = store_email_howlong
        account.save()
        request.session['alias'] = alias
        d = {
            'success': True,
            'msg': _('Storage settings saved!')
        }
        result = json.dumps(d)
        return HttpResponse(result, mimetype='application/json')
    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Failed. Could not edit your storage settings.')
        }
        result = json.dumps(d)
        return HttpResponse(result, mimetype='application/json')
    

def edit_account_users(request):
    try:         
        site = get_site(request)
        alias = request.session['alias']
        if not alias.is_admin:
            raise Exception('Must be an admin to do stuff here.')
        reactivate_msg = None
        
        if 'oper' in request.POST:
            oper = request.POST['oper']
            id = request.POST.get('id')
            if oper == 'del':
                # delete by id. do not delete own alias
                alias_to_delete = Alias.objects.filter(id=id, account=alias.account).exclude(id=alias.id).get()
                if register.delete_alias(alias_to_delete, alias):
                    reactivate_msg = _('User %s has been deleted from this account.') % alias_to_delete.email
                    request.session['alias'].account = Account.objects.get(pk=alias.account.pk)    
            elif oper == 'edit':
                alias_to_edit = Alias.objects.filter(id=id, account=alias.account).exclude(id=alias.id).get()
                cmd = request.POST['cmd']
                if cmd == 'admin':
                    is_admin = bool(int(request.POST['is_admin']))
                    alias_to_edit.is_admin = is_admin
                    alias_to_edit.save()
            elif oper == 'add':
                # add a new user
                email = request.POST['email']
                register.add_new_aliases([email], alias.account, timezone=settings.TIME_ZONE, admin_alias=alias)
                request.messages['notices'].append(_('User %s has been added to this account and will receive an user activation link via email.') % email)
                request.messages_persist = True  # Needed to get request.messages to persist across the next page reload
            else:
                raise Exception('Missing a valid operator request.POST["oper"]: '+oper)
        
        from network.ajax_feeds import get_widget_manager
        get_widget_manager(request).refresh_manager()
        
        d = {
            'success': True
        }
        if reactivate_msg:
            d['reactivate_msg'] = reactivate_msg
            
        result = json.dumps(d)
        return HttpResponse(result, mimetype='application/json')
    except NotifyUserException, e:
        d = {
            'success': False,
            'msg': e.message
        }
        result = json.dumps(d)
        return HttpResponse(result, mimetype='application/json') 
            
    except:
        log('__edit_account_users__: error',traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Could not process your request.')
        }
        result = json.dumps(d)
        return HttpResponse(result, mimetype='application/json')
     
    
# Helper function ... create a fake quickbox entry (that looks like from the tracker)
def admin_ping(contact, content, categories=None):
    from crawl.procmail import procmail_quickbox
    
    try:
        log("admin_ping [1]", contact, categories)
        
        admin_account = Account.objects.get(base_url=settings.SALES_ACCOUNT)
        admin_alias = Alias.objects.get(email=settings.SALES_EMAIL, account=admin_account)
        if categories is None:  categories = []
        
        # Make the fake quickbox entry (that looks like from the tracker)
        
        # Create a contact for that person in the cc account
        admin_contact, created = Contact.objects.get_or_create_smart(\
            first_name = contact.first_name,
            last_name = contact.last_name,
            email = contact.email,
            categories = categories,
            viewing_alias = admin_alias,
            twitter_screen_name = contact.twitter_screen_name
        )
             
        log("admin_ping [2]", contact, categories)

        bucket, created = Bucket.objects.get_or_create_for_alias(admin_alias, 'payments')  

        log("admin_ping [3]", contact, categories)

        mode = procmail_quickbox(alias=admin_alias, content=content, \
            ref_contact_id=admin_contact.id)
        
        log("admin_ping [4]", contact, categories)

        for category in categories:
            bucket_custom, created = Bucket.objects.get_or_create_for_alias(admin_alias, category)
            if bucket_custom and ('email' in mode):
                admin_contact.contact_buckets.add(bucket_custom)
                        
    except:
        log("admin_ping: couldn't create customer contact")    
        
# Helper function ... send a contact to the followup server under the admin account
def admin_followup(contact, campaign, source=None, cc=None, extra=None):
    
    if settings.SERVER != 'live':
        log("admin_followup: skipping because not live server")
        return
    
    log("admin_followup: ", contact, campaign, source, cc)
    admin_account = Account.objects.get(base_url=settings.SALES_ACCOUNT)
    admin_alias = Alias.objects.get(email=settings.SALES_EMAIL, account=admin_account)
    
    try:
        from remote.models import RemoteUser
        ru = RemoteUser.objects.filter(type=RemoteUser.TYPE_FOLLOWUP, account=admin_account)[0]
        api = ru.get_api()
        
        params = {
            'email':        contact.email,
            'first_name':   contact.first_name,
            'last_name':    contact.last_name,
            'company':      contact.company,
            'source':       source or contact.source_text or '',
            'cc_email':     cc or ''          
        }
        if extra:
            params.update(extra)
        api.followup([params], campaign=campaign)
        
    except:
        log("admin_followup: failed")
        log(traceback.format_exc())