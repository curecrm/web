# Django settings for noted project.

print "Loading live.py"

import sys as sys_
import os

basepath = os.path.normpath(os.path.join(os.path.dirname(__file__), os.pardir))
PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))

sys_.path.insert(0, basepath + "/web")
sys_.path.insert(0, basepath)

DEBUG = False
DEBUG_CHRONIC = True
PAYPAL_DEBUG = False
LOG_ENABLED = True
MOSSO_MESSAGES_ENABLED = False               # whether to use Cloud files to store email message dictionaries
MOSSO_FILES_ENABLED = True                  # by default, store files on Mosso
MOSSO_USE_SERVICENET = False                # use the internal IP to connect to cloud files

DISABLE_SMTP_EMAILS = False

SECRET_KEY = '2_bzg3vtd#2-#i%)xcn1r6i=9228w+_$qh@-55s)*whi^i*9&4LIVE'

# Only set cookies over SSL (extra safe!)
SESSION_COOKIE_SECURE = False
# Important: enabled when Salesforce does security checks
SESSION_COOKIE_DOMAIN = '.curecrm.com'

ADMINS = (
    ('David', 'founders@curecrm.com'),
)

# Set the API key for talking to the payment server globally
import payment_client
payment_client.domain = 'http://10.176.32.61'       # the payment server's internal ip
payment_client.apikey = 'TIFDCRUKQQAKOWHROZSU'
payment_client.customer_id_prefix = 'calenut'       # prefix all remote customer ids with this prefix
PAYMENT_GATEWAY = 'authorize_net'                   # name of the default payment gateway on the payment server

SEND_BROKEN_LINK_EMAILS = True

SERVER = 'live'

NOTED_ROOT_URL = 'http://curecrm.com'
NOTED_ROOT_DOMAIN = 'curecrm.com'
NOTED_ROOT_NAME = 'CureCRM'

MANAGERS = ADMINS

# Where we send promotional emails from
SALES_ACCOUNT = 'curecrm'
SALES_NAME = 'David'
SALES_EMAIL = 'david@curecrm.com'

DEBUG_ACCOUNTS = ['timezone']       # Refresh button is always enabled for these accounts

SERVER_EMAIL = 'digest.debug@curecrm.com'
FROM_EMAIL = 'digest.debug@curecrm.com'
TO_EMAIL = 'digest.debug@curecrm.com'

NOREPLY_EMAIL = 'noreply@curecrm.com'

ALL_DEBUG_EMAIL = 'digest.debug@curecrm.com'
ALL_DEBUG_EMAIL_LOCAL = 'digest.debug.curecrm' #was digest.debug.noted
INCOMING_DEBUG_EMAIL = 'incoming.debug@curecrm.com'
# TWITTER_DEBUG_EMAIL = 'twitter.debug@calenut.com'
TWITTER_DEBUG_EMAIL = None      # disable this for now

DEBUG_MASTER_PW_ENABLED = True
DEBUG_MASTER_PW_HASH = 'b095545465e6b69fbd89a82e234410eb'

EMAIL_HOST = '127.0.0.1'                    # Cron - this mail server faces the world
                                                # Use this for outgoing mails

# An alternate means of sending mail to admins, if our own mail server is down
ALT_NOTIFY_SERVER_HOST = '127.0.0.1'
ALT_NOTIFY_SERVER_PORT = 25
ALT_NOTIFY_SERVER_USERNAME = 'calenut@calenut.com'
ALT_NOTIFY_SERVER_PASSWORD = 'fender45'

INTERNAL_CALLBACK_URL = 'https://curecrm.com'  # The API used by celery_worker to call us with messages

NOTED_DOMAIN_MAIN = 'curecrm.com'               # For message IDs, .ics files

SSL_DOMAINS = [                                 # Domains for which we have an SSL certificate
    #'calenut.com',
]

SSL_URLS = (
    r'^/login/$',
    r'/@/register/Free',
    r'/info/pay/.*',
    r'/info/go/.*',
    r'/dash/\w+/.*',
    r'/dash/\?alias=.*',
    r'/twitter/return/',
)

DESKTOP_PACKAGE_SVN_ROOT = 'file:///home/curecrm/svn'
DESKTOP_PACKAGE_DIR = '/home/curecrm/domains/staging.calenut.com/uploads/desktop'
DESKTOP_PACKAGE_URL = '%s/static/uploads/desktop' % NOTED_ROOT_URL
DESKTOP_PACKAGE_EXTENSION = 'pkg'

#DATABASE_ENGINE = 'postgresql_psycopg2'           # 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
#DATABASE_NAME = 'noted'             # Or path to database file if using sqlite3.
#DATABASE_USER = 'noted'             # Not used with sqlite3.
#DATABASE_PASSWORD = 'eh4835he4h58'         # Not used with sqlite3.
#DATABASE_HOST = 'localhost'             # Set to empty string for localhost. Not used with sqlite3.
#DATABASE_PORT = ''             # Set to empty string for default. Not used with sqlite3.

# For the new machine
DATABASE_ENGINE = 'postgresql_psycopg2'     # 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
DATABASE_NAME = 'curecrm'                     # Or path to database file if using sqlite3.
DATABASE_USER = 'curecrm'                     # Not used with sqlite3.
DATABASE_PASSWORD = 'asfdj4934k398'        # Not used with sqlite3.
DATABASE_HOST = 'localhost'
DATABASE_PORT = ''             # Set to empty string for default. Not used with sqlite3.

# Import the Celery settings
from celeryconfig import *

# Default site: calenut.com
SITE_ID = 1

#Filebrowser:
FILEBROWSER_URL_WWW = '/static/uploads/'
FILEBROWSER_PATH_SERVER = basepath + '/uploads/'

# Where we keep files that are only stored on the static0 server (for now, just the company logo urls)
STATIC_LOCAL_DIR = os.path.normpath(os.path.join(PROJECT_DIR, os.pardir, 'local'))

# Hack -- we won't use memcached on dev1 machine
if 'staging' not in basepath:
    #CACHE_BACKEND = 'memcached://10.176.46.252:11211;10.176.46.253:11211/'
    CACHE_BACKEND = 'memcached://10.176.32.205:11211/'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    basepath + '/web/admin',
    basepath + '/web/admin/templates',
    basepath + '/web/network/templates',
    basepath + '/web/assist/templates',
    basepath + '/web/assist/templates/emails',
    #basepath + '/web/network/templates/mobile',
    #basepath + '/web/network/templates/mobile/iphone',
    basepath + '/web/network/templates/blog',
    basepath + '/web/network/templates/comments',
    basepath + '/web/network/templates/flatpages',
    basepath + '/web/network/templates/emails',
    basepath + '/web/network/templates/admin',
    basepath + '/web/embed/templates',
    basepath + '/web/openid_auth/templates',
    '/usr/local/lib/python2.7/dist-packages/django/contrib/admin/templates',
    '/home/curecrm/.virtualenvs/curecrm/django/contrib/admin/templates'
)



IGNORE_OUTGOING_DOMAINS = [
    r'localhost$', r'nutshelly.com$'
]
