"""
Tasks related to crawling

These tasks actually run on the crawl.tasks server and the code is in the crawl.tasks repo.
The tasks in this file are placeholders with the same names, so that code in the Noted repo can
call Celery tasks defined in another repo.
"""

from celeryconfig import get_celery


def test_task(*args, **kwargs):
    with get_celery() as celery:
        return celery.send_task(
            'crawl.tasks.test_task',
            args=args,
            kwargs=kwargs
        )


def test_wait_task(*args, **kwargs):
     with get_celery() as celery:
        return celery.send_task(
            'crawl.tasks.test_wait_task',
            args=args,
            kwargs=kwargs
        )


def mail_server_auth_task(mail_settings, **kwargs):
    with get_celery() as celery:
        return celery.send_task(
            'crawl.tasks.mail_server_auth_task',
            args=(mail_settings,),
            kwargs=kwargs
        )


def get_captcha_task(mail_settings, **kwargs):
    with get_celery() as celery:
        return celery.send_task(
            'crawl.tasks.get_captcha_task',
            args=(mail_settings,),
            kwargs=kwargs
        )


def crawl_task(*args, **kwargs):
    with get_celery() as celery:
        return celery.send_task(
            'crawl.tasks.crawl_task',
            args=args,
            kwargs=kwargs)


def retrieve_and_store_file_attachment_task(mail_settings, folder, path,
                                            mimepath, container_name, filename):
    args = (
        mail_settings, folder,
        path, mimepath,
        container_name, filename
    )

    with get_celery() as celery:
        return celery.send_task(
            'crawl.tasks.retrieve_and_store_file_attachment_task',
            args=args)
