'''Convenient wrappers for the crawl task which runs on celery_worker'''

import sys, os
from django.utils.translation import ugettext as _

from assist.models import *
from models import *
from assist import register
from assist import reminder
from utils import *

from django.template import Context, loader
from django.views.decorators.http import require_http_methods
from django.template import Context, loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.core import serializers
import hashlib
import re, copy, tempfile, shutil
from datetime import *
import random
import json
import traceback, pdb
from collections import defaultdict
from assist import parsedate
from assist import mail, mailthread
from email.Utils import formatdate, parsedate_tz, mktime_tz


def start_crawl_alias(alias, recrawl_from=None, join_existing=True):
    """
    start_crawl_alias: crawl an alias 'in the background' (by launching a Celery task on celery_worker)
    recrawl_from: If specified, crawl all messages back from the given date (without regard to the current EmailFolders)
    join_existing: If True, and there is an existing crawl in progress for this alias, don't start a new one
    """

    print('Calling start_crawl_alias ------------------------>')
    if join_existing:
        queryset = NotedTaskMeta.objects.filter(type=NotedTaskMeta.TYPE_CRAWL, status=NotedTaskMeta.STATUS_STARTED, alias=alias)
        try:
            existing = queryset[0]

            log("start_crawl_alias: return existing NotedTaskMeta %d" % existing.pk)
            print("start_crawl_alias: return existing NotedTaskMeta %d" % existing.pk)
            return None, existing
        except:
            # No ongoing crawl for this user, just start a new one
            pass

    noted_meta = NotedTaskMeta()
    noted_meta.type = NotedTaskMeta.TYPE_CRAWL
    noted_meta.status = NotedTaskMeta.STATUS_STARTED
    noted_meta.alias = alias
    noted_meta.message = _('Connecting to your inbox...')

    args = {
        'recrawl_from':     recrawl_from and recrawl_from.astimezone(pytz.UTC).strftime('%Y-%m-%d %H:%M:%S'),
    }

    noted_meta.args_json = json.dumps(args)
    noted_meta.save()

    m = noted_meta.delay()
    return m, noted_meta

def crawl_account(account, recrawl_from=None, silent=None):

    log("crawler.crawl_account...")

    if not (account.service and account.service.allow_crawl):
        return

    queryset = Alias.objects.active().filter(account=account, server_enabled=True)
    alias_list = list(queryset)
    for alias in alias_list:
        # TODO: Make this a low-priority crawl
        start_crawl_alias(alias, recrawl_from=recrawl_from)

def crawl_slice(current_slice, num_slices):
    log ("crawl_slice (%d/%d)..." % (current_slice, num_slices))
    for account in Account.objects.active().all():
        if not (account.service and account.service.allow_crawl):
            continue

        queryset = Alias.objects.active().filter(account=account, server_enabled=True)
        alias_list = list(queryset)
        log ("crawl_slice - looking for aliases in slice %d" % current_slice)
        for alias in alias_list:
            if alias.id % num_slices == current_slice:
                log(" -- crawl_slice: crawling alias %s" % alias.email)
                # TODO: Make this a low-priority crawl
                start_crawl_alias(alias)



