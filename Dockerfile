FROM python:2.7.12-alpine

RUN apk update && apk add git build-base gcc

COPY . /app
WORKDIR /app

RUN pip install -r requirements.txt
RUN pip install -e git://github.com/nathanborror/django-basic-apps#egg=django-basic-apps

