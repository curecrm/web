"Wrapper for the cloudfiles library"

from django.conf import settings

class CloudFilesManager(object):
    """Our own wrapper for the cloudfiles library
    Uses our mosso:container/filename path syntax
    Keeps connection and container objects persistent
    """

    def __init__(self):
        self.connection = None
        self.containers = {}
        
    def get_connection(self):
        "Get a Connection object to Cloud Files and cache it"
        import cloudfiles
        if not self.connection:
            self.connection = cloudfiles.get_connection(settings.MOSSO_API_USERNAME, settings.MOSSO_API_KEY, servicenet=settings.MOSSO_USE_SERVICENET)
        return self.connection
        
    def get_container(self, container_name, create=False):
        """
        Get a Container object from Cloud Files and cache it
        If create=True, create the Container if it doesn't exist
        """
        conn = self.get_connection()
        if container_name not in self.containers:
            try:
                if create:        
                    self.containers[container_name] = conn.create_container(container_name)
                else:
                    self.containers[container_name] = conn.get_container(container_name)
            except:
                pass
        return self.containers[container_name]
          
    def get_file(self, path):
        """
        Retrieve one file from Mosso Cloud Files, where the file path is specified like this:
        mosso:containername/filename
        
        Returns None if the container or file doesn't exist.
        Raises an exception if the container or file doesn't exist.
        """
                
        try:
            # Get one file from Mosso
            
            if not path.startswith('mosso:'):
                return None
            
            container_name, file_name = path.split(':')[1].split('/')
            container = self.get_container(container_name)
            obj = container.get_object(file_name)
            return(obj.read())
                
        except:
            return None
        
    def delete_file(self, path):
        """
        Delete a file from Mosso Cloud Files, where the file path is specified like this:
        mosso:containername/filename
        
        Returns None if the container or file doesn't exist.
        Raises an exception if the container or file doesn't exist.
        """
                
        try:
            if not path.startswith('mosso:'):
                return None
            
            container_name, file_name = path.split(':')[1].split('/')
            container = self.get_container(container_name)
            obj = container.delete_object(file_name)
            return True
        except:
            return False
        
    def save_file(self, content, container_name, file_name):
        """
        Save one file to Mosso Cloud files, creating the container if necessary.
        Containers usually have names like Calenvy_live_ABCDE.
        """
        
        try:
            container = self.get_container(container_name, create=True)
            obj = container.create_object(file_name)
            obj.write(content)
            
            return 'mosso:%s/%s' % (container_name, file_name)
        except:
            return None
                    